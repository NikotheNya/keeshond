+++
title = "The Importance of IDs"
weight = 2
+++

Numeric IDs are a simple concept, so why devote a whole page to them? Because part of writing idiomatic Keeshond code is using these IDs the way they're intended.

Many parts of Keeshond will return some form of numeric ID. Adding data sources returns a `SourceId`, Looking up items in the resource system will return a `DataId`, and ECS `Entity` objects are essentially IDs themselves. But why IDs? Any game engine supporting some form of modularity needs to be able to lookup items by name. This allows creation and use of resources that don't necessarily have to be hardcoded into the game. Looking them up by name however is slow. In the best case a hash lookup is performed on the name string. In the worst case the game may need to read from the hard drive which will incur heavy latency. On the other hand, looking up an already-loaded object by numeric index is fast and easy. CPUs have dedicated one-liners for this operation.

New users may be tempted to write code similar to the following:

```rust
fn runs_every_frame()
{
    // ...
    
    let id = sheets.get_id("doggymark", "doggy.png").unwrap(); // Don't do this repeatedly!

    use_id(id);
}
```

Not only will this be slow, but the resource must be checked for existence multiple times every time it is accessed - the ID may suddenly change, or the function may suddenly throw an error during gameplay.

Instead, look up the ID once during initialization and store it somewhere so you can reuse it later:

```rust
fn runs_once_at_start()
{
    // ...
    
    my_struct.id = sheets.get_id("doggymark", "doggy.png").unwrap(); // But remember to handle the error gracefully!
}

fn runs_every_frame()
{
    use_id(my_struct.id);
}
```

Unless the documentation says otherwise, you'll want to keep IDs around for as long as you can. For `DataId` and `Entity` objects, you will be informed when the IDs no longer point to something that is loaded in memory. They're not raw pointers or references, meaning they are much safer to work with, and are closer to idiomatic Rust.

There's an additional benefit to using IDs: it helps satisfy Rust's borrow checker by not holding onto references to the objects that the IDs point to. Instead, you grab a temporary reference to the object by providing the ID, and relinquish the reference when you're done, but hold onto the ID. This ensures you can keep track of which resource or object you need while still allowing everyone to take turns borrowing the object itself.

+++
title = "The Coordinate System"
weight = 2
+++

When drawing graphics, you need to decide where you would like to draw them. One important piece of information you'll need is the coordinate system. Keeshond follows the conventions of classic 2D graphics frameworks by having the origin be in the top-left corner of the screen. This represents the coordinates (0, 0). By increasing the X coordinate, we move to the right, and by increasing the Y coordinate, we move down the screen.

The GameInfo object you fill out when initializing your game includes a base resolution. This is the size the game's window defaults to, and it is used as a reference for how to scale all your graphics. For example, the default base resolution of 1280x720 means that the coordinate system runs from (0, 0) top-left to (1279, 719) bottom-right. One unit in this coordinate system is equal to a single pixel.

It is also possible for the user to resize the window, or switch to/from fullscreen. Keeshond will adjust the coordinate system automatically so that the game display scales up and down, preserving aspect ratio by adding black bars on the sides. If the window in this example is made twice as big at 2560x1440, every unit is two pixels in size instead of one.

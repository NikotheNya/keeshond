+++
title = "Multi-Component Systems"
weight = 5
+++

So how do we use multiple component stores at once? We can write code like this:

```rust
// Code to retrieve the player object goes here
let mut movable_store = components.component_store_mut::<Movable>();
let mut enemy_store = components.component_store_mut::<Enemy>();

for (entity, movable) in movable_store.iter_mut()
{
    if player.collides_with(&movable)
    {
        if let Some(enemy) = enemy_store.get_entity_mut(&entity)
        {
            player.damage(enemy.contact_damage);
        }
    }
}
```

At a high level, this code will check if the player is touching a `Movable`, and if it happens to also be an `Enemy`, deal contact damage to the player. But let's go through everything step by step.

We start by retrieving the two component stores. After that is the for loop. This is similar to the example we saw earlier in that we are iterating through all the `Movable` Components. But this time we are also getting the Entity associated with each of these Components.

Now we check if the player collides with the `Movable`. We want to check collisions with all `Movables` in this example, but react to them differently based on their Components.

Now we take this Entity and use it to look up a `Enemy` Component. This lets us check and see if our `Movable` Entity is also an `Enemy`. Because `get_entity_mut()` returns an `Option`, we use the `if let` syntax here to simplify our work by handling the `Option` for us. The following code will run, but only if the `Enemy` Component actually exists.

Finally, we use the `Enemy` Component to get the amount of damage that should be dealt to the player.
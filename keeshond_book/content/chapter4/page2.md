+++
title = "Entities"
weight = 2
+++

An Entity is simply an identifier that can be used to refer to a game object and the Components it has. We could just leave it at that, but it's worth explaining the how and the why.

You can create an Entity manually during setup with the following:

```rust
// let scene = Scene<FooType>::new();

let entity = scene.add_entity();
```

But most likely you will be doing it within a System:

```rust
let entity = scene_control.add_entity_later();
```

Now what's inside the Entity object? It's an identifier, yes, but it's actually made of two numbers: the `id` and the `generation`. The `id` is the addressable part. Internally, it lists where inside the internal data structures everything should be stored. So let's say we've created our first Entity. It will have an `id` of 0. We now create another Entity. This one has an `id` of 1. We create a few more, resulting in `id` values of 2, 3, and 4, in order.

What happens when we decide to delete one of these Entities? Let's delete the ones with `id` of 2 and 3. We are now left with Entities that have `id` of 0, 1, and 4. We don't touch the `id` values of the existing Entities so that code can continue to reference the one with `id` 4 and still access the same object. Meanwhile, `id` values of 2 and 3 are placed into a pool of Entities to be reused.

Now we create a new Entity again. This has an `id` of 2, rather than 5, but something has changed. Despite reusing the same `id`, we want to treat this as a completely new object. This is where the `generation` value comes in.

Every `id` used for the first time is paired with a `generation` value of 0. Each time an `id` is reused, we increase the `generation` value by 1. So this new Entity has an `id` of 2, and a `generation` of 1. Effectively, it is the 2nd Entity with `id` 2. This allows it to be distinguished from the first Entity with `id` 2.

When you perform operations involving Entities, such as removing Entities, or adding or removing Components, both the `id` and `generation` need to match with the Entity you are passing in. It's not enough to simply match the `id`. You could be hanging onto a stale identifier, pointing to an object that has since been deleted and replaced with another. If this stale identifier were accepted, it would give unexpected results. Suddenly you'll be making changes to objects you didn't intend!

But then, why not just have the Entity be a single number instead of two, and avoid reusing `id` values? To put it simply, the `id` specifies where data is stored in memory. If we never reused a single `id` and kept creating more and more, given enough time we would run out of places to put the data. But we can keep increasing the `generation`, because doing so does not require extra memory.

And that is why each Entity has both an `id` and a `generation`.

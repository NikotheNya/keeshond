+++
title = "The Entity Component System"
weight = 4
sort_by = "weight"
+++

The Entity Component System (ECS) Paradigm is at the heart of what makes a Keeshond game tick. ECS has slowly been gaining popularity among game engines, and for good reason. It allows constructing objects and behaviors out of multiple pieces, enabling modularity while being more flexible than the conventional inheritance-based object-oriented approach.

Keeshond provides its very own ECS that ties in with the rest of the engine in a convenient way. Once you understand the basics, you might find it quite enjoyable to work with. Query information about objects. Assign or remove data or behaviors on the fly. Reuse code easily from other projects. All this is possible with ECS.

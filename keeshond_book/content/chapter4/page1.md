+++
title = "ECS Overview"
weight = 1
+++

So what exactly is Entity Component System? Once you understand the name, you'll understand what it's about. There are three parts to an ECS:

- Entity - Represents a living object in your game. By itself it is only an identifier. An Entity is what you make of it.
- Component - A piece that can be attached to an Entity. Components often take the form of data, but they can also be used to "tag" an Entity for specific behaviors.
- System - Provides behavioral code, usually applied to specific Components or configurations of.

In Keeshond there is also the Scene, which holds the above and represents a game state. You can add Entities and Systems to a Scene, and Components to the Entities within the Scene. When you switch Scenes, all associated ECS objects will vanish with the Scene, so it's easy to start again with a clean slate.

While a Scene is running, there are two objects you generally use to interact with the ECS: `ComponentControl` and `SceneControl`. The `ComponentControl` is used to access Components that exist in the Scene, while the `SceneControl` lets you perform modifications such as creating or deleting Entities, and adding or removing Components. You will learn more about these later in the chapter.

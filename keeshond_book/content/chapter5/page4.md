+++
title = "Trust Model"
weight = 4
+++

Keeshond provides a simple trust model to prevent automatic loading of potentially dangerous resources, such as shaders. You may have noticed earlier in this chapter that the `FilesystemSource` was assigned a trust level:

```rust
let source = FilesystemSource::new("data", TrustLevel::TrustedSource);
```

Because this is a folder on disk, created by us, we can mark it "trusted" because we are the authors of the content. If this were data received over the network, however, we would use `TrustLevel::UntrustedSource` instead. Specific `DataObject` implementations must specify whether they need a trusted source by implementing `trust_policy()` and returning `TrustPolicy::TrustRequired` or `TrustPolicy::UntrustedOk`.

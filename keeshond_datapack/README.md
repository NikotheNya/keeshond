# 🐶 KEESHOND Datapack 🐶

## About

Keeshond Datapack lets you easily load resources for your game and cache them
in memory, mapped via pathname and accessible by handle objects that allow
for automatic unloading and can be stored within structs without worrying
about lifetimes. Define how they load with the `DataObject` trait, by
implementing a function that takes a `Read + Seek` object. Define where they
load from by instantiating a `Source`.

## Features

- Load entire packages at once either automatically or preloaded during initiailization
- Unloading and reloading of individual packages during runtime. Memory optimization and hot reloading!
- DataHandle system means objects can reference data without having to borrow it. Serialize handles as pathnames and resolve them at runtime.
- File loading from the filesystem or .zip/.pk3 archives
- File saving - make your own data editor!
- Separate "PreparedStore" system for synchronizing loaded data with backends, such as loading textures into VRAM

Datapack is used by Keeshond but can work with any engine.

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.


## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

extern crate keeshond;

pub mod objects;
pub mod scene;

use keeshond::gameloop::{FrameInterpolationMode, GameInfo, Gameloop};
use keeshond::renderer::{ViewportMode, WindowScaleMode};

use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use crate::scene::GameScene;


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "level",
        friendly_name : "Keeshond Treats Level Example",
        base_width : 512,
        base_height : 288,
        texture_filtering : false,
        viewport_mode : ViewportMode::Pixel,
        default_window_scale : WindowScaleMode::PixelHighestMultiplier,
        cursor_visible : false,
        frame_interpolation : FrameInterpolationMode::Automatic,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    let input = gameloop.control_mut().input_mut();

    input.add_bind("up", "up");
    input.add_bind("up", "gamepad up");
    input.add_bind("up", "gamepad lstick up");
    input.add_bind("up", "gamepad rstick up");
    input.add_bind("down", "down");
    input.add_bind("down", "gamepad down");
    input.add_bind("down", "gamepad lstick down");
    input.add_bind("down", "gamepad rstick down");
    input.add_bind("left", "left");
    input.add_bind("left", "gamepad left");
    input.add_bind("left", "gamepad lstick left");
    input.add_bind("left", "gamepad rstick left");
    input.add_bind("left", "gamepad ltrigger");
    input.add_bind("right", "right");
    input.add_bind("right", "gamepad right");
    input.add_bind("right", "gamepad lstick right");
    input.add_bind("right", "gamepad rstick right");
    input.add_bind("right", "gamepad rtrigger");
    
    let source_manager = gameloop.control_mut().source_manager();

    for path in &["examples_data", "keeshond_treats/examples_data", "keeshond/keeshond_treats/examples_data"]
    {
        let source = FilesystemSource::new(path, TrustLevel::TrustedSource);
        source_manager.borrow_mut().add_source(Box::new(source));
    }
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}

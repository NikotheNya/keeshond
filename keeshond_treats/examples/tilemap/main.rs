
#[macro_use] extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{GameControl, GameInfo, Gameloop};
use keeshond::scene::{DynArgList, SceneConfig, SceneType, SpawnControl, ThinkerArgs, ThinkerSystem};
use keeshond::renderer::{ViewportMode, WindowScaleMode};
use keeshond::util::{BakedRect, Rect, SimpleTransform};

use keeshond::datapack::DataHandle;
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_treats::{DefaultSystemConfig, DefaultSystems, LoadErrorMode};
use keeshond_treats::visual::{Drawable, PixelSnap};
use keeshond_treats::world::Camera;
use keeshond_treats::tilemap::{Tilemap, TilemapTile};
use keeshond_treats::spawnable::SpawnDefaultHelper;


////////////////////////////////////////


const CAMERA_MOVE_SPEED : f64 = 4.0;


keeshond::spawnables!
{
    SpawnId;
    Camera,
    Background,
    Tilemap
}


fn generate_tilemap() -> Tilemap
{
    let mut tilemap = Tilemap::new(100, 100, DataHandle::with_path("tile_example", "tileset.json"));

    for y in 0..1000
    {
        for x in 0..1000
        {
            tilemap.grid_mut().set_tile(x, y, TilemapTile { index : (x as u32 + y as u32), flags : 0 });
        }
    }

    tilemap
}


struct GameScene
{
    spawn_defaults : SpawnDefaultHelper<Self>
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;

    fn new() -> Self where Self : Sized
    {
        GameScene
        {
            spawn_defaults : SpawnDefaultHelper::empty()
        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.default_systems(DefaultSystemConfig
        {
            load_error : LoadErrorMode::Fatal,
            pixel_snap : PixelSnap::SpriteAndCameraSnap,
            auto_create_camera : false,
            collision_cel_size : 64
        })
            .thinker(ExampleThinker {});

        self.spawn_defaults = SpawnDefaultHelper::from_spawnables_lookup("common", "objects.json", game);

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        match spawnable_id
        {
            SpawnId::Camera =>
            {
                let (base_width, base_height) = game.renderer().base_size();
                let mut camera = Camera::new(transform.x, transform.y, Rect
                {
                    x : 0.0,
                    y : 0.0,
                    w : base_width,
                    h : base_height
                });

                camera.scroll_stop = Some(BakedRect
                {
                    x1: 0.0,
                    y1: 0.0,
                    x2: 1600.0,
                    y2: 1600.0
                });

                spawn.with(camera);
            }
            SpawnId::Background =>
            {
                spawn.with(Drawable::with_background_color(0.14, 0.65, 1.0));
            }
            SpawnId::Tilemap =>
            {
                let scroll_rate = args.get_float(0, 1.0) as f32;
                let alpha = args.get_float(1, 1.0) as f32;

                let mut drawable = Drawable::new();
                drawable.scroll_rate_x = scroll_rate;
                drawable.scroll_rate_y = scroll_rate;

                drawable.item.modify_multi_tile_sprite(|tilemap|
                {
                    tilemap.alpha = alpha;
                });

                spawn.with(drawable);
                spawn.with(generate_tilemap());
            }
        }

        self.spawn_defaults.spawn_defaults(spawn, spawnable_id, game, transform, args, LoadErrorMode::Fatal);
    }
}


////////////////////////////////////////

struct ExampleThinker {}

impl ThinkerSystem<GameScene> for ExampleThinker
{
    fn start(&mut self, args : ThinkerArgs<GameScene>)
    {
        args.scene.spawn_later(SpawnId::Camera, args.game, &SimpleTransform::with_xy(0.0, 0.0));
        args.scene.spawn_later(SpawnId::Background, args.game, &SimpleTransform::with_xy(0.0, 0.0));
        args.scene.spawn_later_with(SpawnId::Tilemap, args.game, &SimpleTransform
        {
            x : -400.0,
            y : 0.0,
            z : -1024.0,
            angle : -0.4,
            scale_x : 1.0,
            scale_y : 1.0
        }, &darg![0.05, 0.5]);
        args.scene.spawn_later_with(SpawnId::Tilemap, args.game, &SimpleTransform::with_xy(0.0, 0.0), &darg![1.0, 1.0]);
    }
    
    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut camera_store = args.components.store_mut::<Camera>();

        if let Some((_, camera)) = camera_store.first_mut()
        {
            camera.x += args.game.input().paired_axis("left", "right") * CAMERA_MOVE_SPEED;
            camera.y += args.game.input().paired_axis("up", "down") * CAMERA_MOVE_SPEED;
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "tilemap",
        friendly_name : "Keeshond Treats Tilemap Example",
        base_width : 480,
        base_height : 270,
        texture_filtering : false,
        viewport_mode : ViewportMode::Pixel,
        default_window_scale : WindowScaleMode::PixelMultiplierDpiScaled(2),
        cursor_visible : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    let input = gameloop.control_mut().input_mut();

    input.add_bind("up", "up");
    input.add_bind("up", "gamepad up");
    input.add_bind("up", "gamepad lstick up");
    input.add_bind("up", "gamepad rstick up");
    input.add_bind("down", "down");
    input.add_bind("down", "gamepad down");
    input.add_bind("down", "gamepad lstick down");
    input.add_bind("down", "gamepad rstick down");
    input.add_bind("left", "left");
    input.add_bind("left", "gamepad left");
    input.add_bind("left", "gamepad lstick left");
    input.add_bind("left", "gamepad rstick left");
    input.add_bind("left", "gamepad ltrigger");
    input.add_bind("right", "right");
    input.add_bind("right", "gamepad right");
    input.add_bind("right", "gamepad lstick right");
    input.add_bind("right", "gamepad rstick right");
    input.add_bind("right", "gamepad rtrigger");
    
    let source_manager = gameloop.control_mut().source_manager();

    for path in &["examples_data", "keeshond_treats/examples_data", "keeshond/keeshond_treats/examples_data"]
    {
        let source = FilesystemSource::new(path, TrustLevel::TrustedSource);
        source_manager.borrow_mut().add_source(Box::new(source));
    }
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}

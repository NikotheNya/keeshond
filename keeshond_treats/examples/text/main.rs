
#[macro_use] extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{GameControl, GameInfo, Gameloop};
use keeshond::scene::{DynArgList, SceneConfig, SceneType, SpawnableConfig, SpawnControl, ThinkerArgs, ThinkerSystem};
use keeshond::renderer::{ViewportMode, WindowScaleMode};
use keeshond::util::SimpleTransform;

use keeshond::datapack::DataHandle;
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

use keeshond_treats::{DefaultSystemConfig, DefaultSystems, LoadErrorMode};
use keeshond_treats::visual::{Drawable, PixelSnap, Text};
use keeshond_treats::spawnable::SpawnDefaultHelper;
use keeshond_treats::world::Transform;


////////////////////////////////////////


keeshond::spawnables!
{
    SpawnId;
    Background,
    Text,
    BlobRed : Blob,
    BlobCyan : Blob
}

#[derive(Component)]
struct Blob
{
    origin_x : f64,
    origin_y : f64,
    tics : u64
}

impl Blob
{
    fn new(origin_x : f64, origin_y : f64, tics : u64) -> Blob
    {
        Blob
        {
            origin_x,
            origin_y,
            tics
        }
    }
}

impl SpawnableConfig for Blob
{
    fn spawn(spawn : &mut SpawnControl, _game : &mut GameControl,
             transform : &SimpleTransform, args : &DynArgList)
    {
        spawn.with(Blob::new(transform.x, transform.y, args.get_int(0, 0) as u64 * 15));
    }
}

struct GameScene
{
    spawn_defaults : SpawnDefaultHelper<Self>
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;

    fn new() -> Self where Self : Sized
    {
        GameScene
        {
            spawn_defaults : SpawnDefaultHelper::empty()
        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.default_systems(DefaultSystemConfig
        {
            load_error : LoadErrorMode::Fatal,
            pixel_snap : PixelSnap::SpriteAndCameraSnap,
            auto_create_camera : true,
            collision_cel_size : 64
        })
            .thinker(GameThinker::new());

        self.spawn_defaults = SpawnDefaultHelper::from_spawnables_lookup("common", "objects.json", game);

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        match spawnable_id
        {
            SpawnId::Background =>
            {
                spawn.with(Drawable::with_background_color(0.42, 0.48, 0.52));
            }
            SpawnId::Text =>
            {
                let mut text = Text::new("hello world\nthis is multi-line text", DataHandle::with_path("common", "font.png"), 32.0);

                text.set_alignment(0.5);

                spawn.with(Drawable::new());
                spawn.with(text);
            }
            _ => {}
        }

        self.spawn_defaults.spawn_defaults(spawn, spawnable_id, game, transform, args, LoadErrorMode::Fatal);
    }
}

struct GameThinker
{
    tics : u64
}

impl GameThinker
{
    fn new() -> GameThinker
    {
        GameThinker
        {
            tics : 0
        }
    }
}

impl ThinkerSystem<GameScene> for GameThinker
{
    fn start(&mut self, args : ThinkerArgs<GameScene>)
    {
        let (base_width, base_height) = args.game.renderer().base_size();
        let x = base_width as f64 / 2.0;
        let y = base_height as f64 / 2.0;

        args.scene.spawn_later(SpawnId::Background, args.game, &SimpleTransform::default());
        args.scene.spawn_later(SpawnId::Text, args.game, &SimpleTransform::with_xy(x, y));

        for i in 0..32
        {
            args.scene.spawn_later_with(SpawnId::BlobCyan, args.game, &SimpleTransform::with_xy(x, y), &darg![i*2]);
            args.scene.spawn_later_with(SpawnId::BlobRed, args.game, &SimpleTransform::with_xy(x, y), &darg![i*2 + 1]);
        }
    }

    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut text_store = args.components.store_mut::<Text>();
        let mut blob_store = args.components.store_mut::<Blob>();
        let mut transform_store = args.components.store_mut::<Transform>();
        let mut drawable_store = args.components.store_mut::<Drawable>();

        for (entity, text) in text_store.iter_mut()
        {
            text.set_alignment((self.tics as f32 * 0.02).sin() * 0.25 + 0.5);

            if let Some(transform) = transform_store.get_entity_mut(&entity)
            {
                transform.set_angle((self.tics as f64 * 0.012).sin() * 0.2);
                transform.set_scale((self.tics as f64 * 0.025).sin() * 0.1 + 1.0,
                                    (self.tics as f64 * 0.03).sin() * 0.1 + 1.0);
            }

            if let Some(drawable) = drawable_store.get_entity_mut(&entity)
            {
                drawable.item.modify_multi_slice_sprite(|multi_slice|
                {
                    multi_slice.alpha = (self.tics as f32 * 0.03).sin() * 0.2 + 0.8;
                });
            }
        }

        for (entity, blob) in blob_store.iter_mut()
        {
            if let Some(transform) = transform_store.get_entity_mut(&entity)
            {
                blob.tics += 1;

                let sin = (blob.tics as f64 / std::f64::consts::PI * 360.0 * 0.0001).sin();
                let cos = (blob.tics as f64 / std::f64::consts::PI * 360.0 * 0.0001).cos();
                let radius = ((blob.tics as f64 / std::f64::consts::PI * 360.0 * 0.00003).sin() + 2.0) * 48.0;

                // TODO: Have depth range be user-specified
                transform.set_pos(blob.origin_x + sin * radius - cos * (radius * 0.5), blob.origin_y + cos * radius, sin * radius);

                if let Some(drawable) = drawable_store.get_entity_mut(&entity)
                {
                    drawable.item.modify_sprite(|sprite|
                    {
                        sprite.r = 0.6 + sin as f32 * 0.6;
                        sprite.g = 1.0;
                        sprite.b = 1.0 + cos as f32;
                        sprite.alpha = 0.85;
                    });
                }
            }
        }

        self.tics += 1;
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "text",
        friendly_name : "Keeshond Treats Text Example",
        base_width : 512,
        base_height : 288,
        texture_filtering : false,
        viewport_mode : ViewportMode::Pixel,
        default_window_scale : WindowScaleMode::PixelMultiplierDpiScaled(2),
        cursor_visible : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    gameloop.control_mut().renderer_mut().set_window_title("Keeshond Treats Text");
    
    let source_manager = gameloop.control_mut().source_manager();

    for path in &["examples_data", "keeshond_treats/examples_data", "keeshond/keeshond_treats/examples_data"]
    {
        let source = FilesystemSource::new(path, TrustLevel::TrustedSource);
        source_manager.borrow_mut().add_source(Box::new(source));
    }
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}

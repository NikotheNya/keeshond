
extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{GameControl, GameInfo, Gameloop};
use keeshond::scene::{DynArgList, SceneConfig, SceneType, SpawnControl, ThinkerArgs, ThinkerSystem};
use keeshond::renderer::{ViewportMode, WindowScaleMode};
use keeshond::util::SimpleTransform;

use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_treats::{DefaultSystemConfig, DefaultSystems, LoadErrorMode};
use keeshond_treats::visual::PixelSnap;
use keeshond_treats::spawnable::SpawnDefaultHelper;


////////////////////////////////////////


keeshond::spawnables!
{
    SpawnId;
    BlobRed
}

struct GameScene
{
    spawn_defaults : SpawnDefaultHelper<Self>
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;

    fn new() -> Self where Self : Sized
    {
        GameScene
        {
            spawn_defaults : SpawnDefaultHelper::empty()
        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.default_systems(DefaultSystemConfig
        {
            load_error : LoadErrorMode::Fatal,
            pixel_snap : PixelSnap::SpriteAndCameraSnap,
            auto_create_camera : true,
            collision_cel_size : 64
        })
            .thinker(ExampleThinker {});

        self.spawn_defaults = SpawnDefaultHelper::from_spawnables_lookup("common", "objects.json", game);

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        self.spawn_defaults.spawn_defaults(spawn, spawnable_id, game, transform, args, LoadErrorMode::Fatal);
    }
}


////////////////////////////////////////

struct ExampleThinker {}

impl ThinkerSystem<GameScene> for ExampleThinker
{
    fn start(&mut self, args : ThinkerArgs<GameScene>)
    {
        args.scene.spawn_later(SpawnId::BlobRed, args.game, &SimpleTransform::with_xy(32.0, 32.0));
    }
    
    fn think(&mut self, _args : ThinkerArgs<GameScene>)
    {

    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "sprite",
        friendly_name : "Keeshond Treats Sprite Example",
        base_width : 384,
        base_height : 224,
        texture_filtering : false,
        viewport_mode : ViewportMode::Pixel,
        default_window_scale : WindowScaleMode::PixelMultiplierDpiScaled(2),
        cursor_visible : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    let source_manager = gameloop.control_mut().source_manager();

    for path in &["examples_data", "keeshond_treats/examples_data", "keeshond/keeshond_treats/examples_data"]
    {
        let source = FilesystemSource::new(path, TrustLevel::TrustedSource);
        source_manager.borrow_mut().add_source(Box::new(source));
    }
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}

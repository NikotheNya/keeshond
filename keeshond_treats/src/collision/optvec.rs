use std::iter::Enumerate;

pub(crate) struct OptVecIter<'a, T>
{
    data_iter : Enumerate<std::slice::Iter<'a, Option<T>>>,
}

impl<'a, T> OptVecIter<'a, T>
{
    fn new(opt_vec : &OptVec<T>) -> OptVecIter<T>
    {
        OptVecIter::<T> { data_iter : opt_vec.list.iter().enumerate() }
    }
}

impl<'a, T> Iterator for OptVecIter<'a, T>
{
    type Item = (usize, &'a T);

    fn next(&mut self) -> Option<(usize, &'a T)>
    {
        loop
        {
            if let Some((id, opt_item)) = self.data_iter.next()
            {
                if let Some(item) = opt_item
                {
                    return Some((id, item));
                }
            }
            else
            {
                return None
            }
        }
    }
}

pub(crate) struct OptVecIterMut<'a, T>
{
    data_iter : Enumerate<std::slice::IterMut<'a, Option<T>>>,
}

impl<'a, T> OptVecIterMut<'a, T>
{
    #[allow(dead_code)]
    fn new(opt_vec : &mut OptVec<T>) -> OptVecIterMut<T>
    {
        OptVecIterMut::<T> { data_iter : opt_vec.list.iter_mut().enumerate() }
    }
}

impl<'a, T> Iterator for OptVecIterMut<'a, T>
{
    type Item = (usize, &'a mut T);

    fn next(&mut self) -> Option<(usize, &'a mut T)>
    {
        loop
        {
            if let Some((id, opt_item)) = self.data_iter.next()
            {
                if let Some(item) = opt_item
                {
                    return Some((id, item));
                }
            }
            else
            {
                return None
            }
        }
    }
}

pub(crate) struct OptVec<T>
{
    list : Vec<Option<T>>,
    recycled : Vec<usize>,
    count : usize
}

impl<T> OptVec<T>
{
    pub fn new() -> OptVec<T>
    {
        OptVec
        {
            list : Vec::new(),
            recycled : Vec::new(),
            count : 0
        }
    }

    pub fn add(&mut self, item : T) -> usize
    {
        let id;

        if self.recycled.is_empty()
        {
            id = self.list.len();
        }
        else
        {
            id = self.recycled.pop().unwrap();
        }

        while self.list.len() <= id
        {
            self.list.push(None);
        }

        self.list[id] = Some(item);
        self.count += 1;

        id
    }

    pub fn remove(&mut self, id : usize) -> Option<T>
    {
        if id < self.list.len() && self.list[id].is_some()
        {
            self.recycled.push(id);

            let mut item = None;
            std::mem::swap(&mut item, &mut self.list[id]);

            self.count -= 1;

            return item;
        }

        None
    }

    pub fn get(&self, id : usize) -> Option<&T>
    {
        if let Some(opt_item) = self.list.get(id)
        {
            return opt_item.as_ref();
        }

        None
    }

    pub fn get_mut(&mut self, id : usize) -> Option<&mut T>
    {
        if let Some(opt_item) = self.list.get_mut(id)
        {
            return opt_item.as_mut();
        }

        None
    }

    pub fn iter(&self) -> OptVecIter<T>
    {
        OptVecIter::<T>::new(self)
    }

    #[allow(dead_code)]
    pub fn iter_mut(&mut self) -> OptVecIterMut<T>
    {
        OptVecIterMut::<T>::new(self)
    }

    pub fn shrink(&mut self)
    {
        self.recycled.sort_unstable_by(|a, b| b.cmp(a));

        for i in (0..self.list.len()).rev()
        {
            if self.list[i].is_none()
            {
                self.list.pop();
            }
            else
            {
                break;
            }
        }
    }

    pub fn count(&self) -> usize
    {
        self.count
    }

    #[allow(dead_code)]
    pub fn len(&self) -> usize
    {
        self.list.len()
    }
}
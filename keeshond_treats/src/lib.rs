
extern crate keeshond;
extern crate keeshond_derive;

#[macro_use] extern crate log;
extern crate smallvec;
extern crate bit_set;
extern crate rustc_hash;
extern crate serde;
extern crate serde_json;
extern crate bincode;
extern crate block_grid;

use collision::CollidableTransformSyncThinker;
use crate::visual::{ActorThinker, DrawableDrawer, PixelSnap, PropThinker, TextThinker};
use crate::world::{CameraThinker, TransformCollidableSyncThinker};
use crate::tilemap::TilemapThinker;
use crate::collision::{CollisionControl, CollisionMaintenanceThinker};

use keeshond::scene::{SceneConfig, SceneType};
pub use keeshond::datapack::LoadErrorMode;

pub mod spawnable;
pub mod level;
pub mod world;
pub mod visual;
pub mod tilemap;
pub mod collision;
mod util;

#[cfg(test)] mod tests;

pub const SYSTEM_PRIORITY_PRE_FRAME : i32 = -16777216;
pub const SYSTEM_PRIORITY_PRE_PHYSICS : i32 = -8388608;
pub const SYSTEM_PRIORITY_POST_PHYSICS : i32 = 8388608;
pub const SYSTEM_PRIORITY_POST_FRAME : i32 = 16777216;
pub const COMPONENT_PRIORITY_PHYSICAL : i32 = -65535;

pub struct DefaultSystemConfig
{
    pub load_error : LoadErrorMode,
    pub pixel_snap : PixelSnap,
    pub auto_create_camera : bool,
    pub collision_cel_size : u16
}

pub trait DefaultSystems
{
    fn default_systems(&mut self, config : DefaultSystemConfig) -> &mut Self;
}

impl<T : SceneType + 'static> DefaultSystems for SceneConfig<T>
{
    fn default_systems(&mut self, config : DefaultSystemConfig) -> &mut Self
    {
        self.thinker(TransformCollidableSyncThinker::new())
        .thinker(CollidableTransformSyncThinker::new())
        .thinker(CameraThinker::new(config.auto_create_camera))
        .thinker(PropThinker::new())
        .thinker(ActorThinker::new(config.load_error))
        .thinker(TextThinker::new(config.load_error))
        .thinker(TilemapThinker::new(config.load_error))
        .thinker(CollisionMaintenanceThinker::new())
        .drawer(DrawableDrawer::new(config.pixel_snap))
        .singleton(CollisionControl::new(config.collision_cel_size))
    }
}

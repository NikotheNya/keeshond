use crate::LoadErrorMode;
use crate::spawnable::{SpawnableSet};
use crate::tilemap::{Tilemap, Tileset};
use crate::visual::{Actor, Costume, Drawable, Prop};

use keeshond::crate_reexport::strum_macros::Display;
use keeshond::datapack::{DataError, DataHandle, DataObject, TrustPolicy};
use keeshond::datapack::source::Source;
use keeshond::renderer::Sheet;
use keeshond::scene::{DynArg, DynArgList, SceneType, ThinkerArgs};
use keeshond::util::{BakedRect, NamedItemStore, SimpleTransform};

use keeshond_derive::Component;

use std::collections::{HashMap, HashSet};
use std::str::FromStr;
use bincode::Options;
use serde::{Deserialize, Deserializer, Serialize};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct BackgroundInfo
{
    pub r : f32,
    pub g : f32,
    pub b : f32
}

impl BackgroundInfo
{
    pub fn black() -> BackgroundInfo
    {
        BackgroundInfo
        {
            r : 0.0,
            g : 0.0,
            b : 0.0
        }
    }
}

#[derive(Component)]
pub struct LevelBackground {}

#[derive(Clone, Serialize, Deserialize, Debug, Display)]
pub enum LevelRepresentation
{
    None,
    Actor
    {
        custom_costume : Option<DataHandle<Costume>>
    },
    Tilemap
    {
        tiles : Tilemap
    },
    Prop
    {
        custom_sheet : Option<DataHandle<Sheet>>,
        custom_slice : Option<String>
    }
}

impl LevelRepresentation
{
    pub fn is_none(&self) -> bool
    {
        match self
        {
            LevelRepresentation::None => { true }
            _ => { false }
        }
    }

    pub fn index(&self) -> usize
    {
        match self
        {
            LevelRepresentation::None => { 0 }
            LevelRepresentation::Actor { .. } => { 1 }
            LevelRepresentation::Tilemap { .. } => { 2 }
            LevelRepresentation::Prop { .. } => { 3 }
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LevelSpawnable
{
    #[serde(rename = "ty")]
    #[serde(alias = "spawn_name_id")]
    pub spawn_name_id : u32,
    pub transform : SimpleTransform,
    pub args : Option<DynArgList>,
    #[serde(rename = "re")]
    #[serde(alias = "representation")]
    pub representation : LevelRepresentation
}

impl LevelSpawnable
{
    pub fn modify_args<F : FnMut(&mut DynArgList)>(&mut self, mut func : F)
    {
        if self.args.is_none()
        {
            self.args = Some(DynArgList::new());
        }

        if let Some(args) = &mut self.args
        {
            func(args);
        }
    }

    pub fn args_empty(&self) -> bool
    {
        match &self.args
        {
            None => { true }
            Some(arg_list) =>
            {
                arg_list.is_empty()
            }
        }
    }
}

#[derive(Clone, Default, Serialize, Debug)]
struct SpawnableNameIdMap
{
    #[serde(skip_serializing)]
    names : HashMap<String, u32>,
    #[serde(serialize_with = "crate::util::sort_map")]
    #[serde(rename = "i")]
    #[serde(alias = "ids")]
    ids : HashMap<u32, String>
}

#[derive(Deserialize, Debug)]
struct NameIdMapRaw
{
    #[serde(rename = "i")]
    #[serde(alias = "ids")]
    ids : HashMap<u32, String>
}

impl<'de> Deserialize<'de> for SpawnableNameIdMap
{
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
        where D : Deserializer<'de>
    {
        let map_raw = NameIdMapRaw::deserialize(deserializer)?;

        let mut names = HashMap::new();

        for (id, name) in &map_raw.ids
        {
            names.insert(name.clone(), *id);
        }

        Ok(SpawnableNameIdMap
        {
            names,
            ids : map_raw.ids
        })
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LevelLayer
{
    #[serde(default)]
    spawnables : Vec<LevelSpawnable>,
    #[serde(default)]
    z_offset : f64,
    #[serde(default)]
    scroll_rate : Option<(f32, f32)>,
    #[serde(default)]
    visible : bool,
    #[serde(default)]
    locked : bool
}

impl Default for LevelLayer
{
    fn default() -> LevelLayer
    {
        LevelLayer
        {
            spawnables : Vec::new(),
            z_offset : 0.0,
            scroll_rate : None,
            visible : true,
            locked : false
        }
    }
}

impl LevelLayer
{
    pub fn spawnable(&self, id : usize) -> Option<&LevelSpawnable>
    {
        self.spawnables.get(id)
    }

    pub fn spawnable_mut(&mut self, id : usize) -> Option<&mut LevelSpawnable>
    {
        self.spawnables.get_mut(id)
    }

    pub fn insert_spawnable(&mut self, pos : usize, spawnable : LevelSpawnable) -> bool
    {
        if pos <= self.spawnables.len()
        {
            self.spawnables.insert(pos, spawnable);

            return true;
        }

        false
    }

    pub fn remove_spawnable(&mut self, id : usize) -> Option<LevelSpawnable>
    {
        if id >= self.spawnables.len()
        {
            return None;
        }

        Some(self.spawnables.remove(id))
    }

    pub fn spawnable_count(&self) -> usize
    {
        self.spawnables.len()
    }

    pub fn z_offset(&self) -> f64
    {
        self.z_offset
    }

    pub fn set_z_offset(&mut self, z_offset : f64)
    {
        self.z_offset = z_offset;
    }

    pub fn scroll_rate(&self) -> Option<(f32, f32)>
    {
        self.scroll_rate
    }

    pub fn set_scroll_rate(&mut self, scroll_rate : Option<(f32, f32)>)
    {
        self.scroll_rate = scroll_rate;
    }

    pub fn visible(&self) -> bool
    {
        self.visible
    }

    pub fn set_visible(&mut self, visible : bool)
    {
        self.visible = visible;
    }

    pub fn locked(&self) -> bool
    {
        self.locked
    }

    pub fn set_locked(&mut self, locked : bool)
    {
        self.locked = locked;
    }
}

#[derive(Clone, Debug)]
pub struct LevelApplyOptions
{
    pub global_properties : bool
}

impl Default for LevelApplyOptions
{
    fn default() -> Self
    {
        LevelApplyOptions
        {
            global_properties : true
        }
    }
}

#[derive(Component)]
pub struct LevelInfo
{
    pub bounds : BakedRect<f64>
}

impl Default for LevelInfo
{
    fn default() -> Self
    {
        LevelInfo
        {
            bounds : Default::default()
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Level
{
    header : String,
    #[serde(rename = "bb")]
    bounds : BakedRect<f64>,
    bg : Option<BackgroundInfo>,
    #[serde(default)]
    spawnable_set : DataHandle<SpawnableSet>,
    spawnable_names : SpawnableNameIdMap,
    #[serde(default)]
    layers : NamedItemStore<LevelLayer>,
    #[serde(skip)]
    generation : u64
}

impl Level
{
    pub fn new() -> Level
    {
        Level
        {
            header : String::from("Keeshond Level Format r1"),
            bounds : BakedRect::default(),
            bg : None,
            spawnable_set : DataHandle::null(),
            spawnable_names : SpawnableNameIdMap::default(),
            layers : NamedItemStore::new(),
            generation : 0
        }
    }

    fn next_generation(&mut self)
    {
        self.generation += 1;

        if self.generation == 0
        {
            warn!("Generation overflow!");
            self.generation = 1;
        }
    }

    pub fn apply_from_path<T : SceneType + 'static>(package : &str, path : &str, args : &mut ThinkerArgs<T>, apply_options : LevelApplyOptions) -> Level
    {
        let source_id = args.game.res().package_source_id(package);
        let level_result = args.game.res().store_mut::<Level>().load_direct(package, path, source_id);

        match level_result
        {
            Ok(mut level) =>
            {
                level.apply(args, apply_options);

                level
            }
            Err(error) =>
            {
                panic!("Failed to load \"{}/{}/{}\": {}",
                       package, Level::folder_name(), path, error.to_string());
            }
        }
    }

    pub fn apply<T : SceneType + 'static>(&mut self, args : &mut ThinkerArgs<T>, apply_options : LevelApplyOptions)
    {
        if apply_options.global_properties
        {
            let mut level_info_store = args.components.store_mut::<LevelInfo>();
            let mut level_info = level_info_store.singleton_mut();

            level_info.bounds = self.bounds;

            if let Some(level_bg) = &self.bg
            {
                let bg_store = args.components.store_mut::<LevelBackground>();

                if bg_store.count() == 0
                {
                    let bg_entity = args.scene.add_entity_later();
                    args.scene.add_component_later(&bg_entity, LevelBackground {});
                    args.scene.add_component_later(&bg_entity, Drawable::with_background_color(level_bg.r, level_bg.g, level_bg.b));
                }
            }
        }

        let mut spawnable_map : HashMap<u32, T::SpawnableIdType> = HashMap::new();

        for (id, name) in &self.spawnable_names.ids
        {
            if let Ok(spawnable_type) = T::SpawnableIdType::from_str(name)
            {
                spawnable_map.insert(*id, spawnable_type);
            }
            else
            {
                warn!("Unknown Spawnable name \"{}\"", name);
            }
        }

        for layer in &mut self.layers
        {
            for spawnable in &mut layer.spawnables
            {
                if let Some(id) = spawnable_map.get(&spawnable.spawn_name_id)
                {
                    let mut new_args;

                    if let Some(spawnable_args) = &spawnable.args
                    {
                        new_args = spawnable_args.clone();
                    }
                    else
                    {
                        new_args = DynArgList::new();
                    }

                    if let Some((scroll_rate_x, scroll_rate_y)) = layer.scroll_rate
                    {
                        if new_args.get_named("scroll_rate_x").is_none() && new_args.get_named("scroll_rate_y").is_none()
                        {
                            new_args.push_named(String::from("scroll_rate_x"), DynArg::Float(scroll_rate_x as f64));
                            new_args.push_named(String::from("scroll_rate_y"), DynArg::Float(scroll_rate_y as f64));
                        }
                    }

                    let mut transform = spawnable.transform.clone();

                    transform.z += layer.z_offset;

                    args.scene.spawn_later_with_components(*id, args.game, &transform, &new_args, |game, spawn|
                    {
                        match &mut spawnable.representation
                        {
                            LevelRepresentation::None => {}
                            LevelRepresentation::Actor { custom_costume } =>
                            {
                                spawn.with(crate::spawnable::drawable_from_args(&new_args));

                                if let Some(costume) = custom_costume
                                {
                                    let mut actor = Actor::new();
                                    let mut costume_store = game.res().store_mut();

                                    costume.resolve(&mut costume_store, LoadErrorMode::Fatal);

                                    actor.set_costume(costume.clone_without_path());

                                    spawn.with(actor);
                                }
                            }
                            LevelRepresentation::Tilemap { tiles } =>
                            {
                                let mut tileset_store = game.res().store_mut::<Tileset>();

                                tiles.tileset().resolve(&mut tileset_store, LoadErrorMode::Fatal);
                                spawn.with(Tilemap::with_grid(tiles.grid().clone(), tiles.tileset().clone_without_path()));
                            }
                            LevelRepresentation::Prop { custom_sheet, custom_slice } =>
                            {
                                spawn.with(crate::spawnable::drawable_from_args(&new_args));

                                if let Some(sheet) = custom_sheet
                                {
                                    let mut prop = Prop::new();
                                    let mut sheet_store = game.res().store_mut();

                                    sheet.resolve(&mut sheet_store, LoadErrorMode::Fatal);

                                    prop.set_sheet(sheet.clone_without_path());
                                    if let Some(slice) = &custom_slice
                                    {
                                        prop.set_slice_name(slice, &mut sheet_store);
                                    }

                                    spawn.with(prop);
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    pub fn bounds(&self) -> &BakedRect<f64>
    {
        &self.bounds
    }

    pub fn set_bounds(&mut self, bounds : BakedRect<f64>)
    {
        self.bounds = bounds;

        self.next_generation();
    }

    pub fn background(&self) -> Option<&BackgroundInfo>
    {
        self.bg.as_ref()
    }

    pub fn set_background(&mut self, bg : Option<BackgroundInfo>)
    {
        self.bg = bg;

        self.next_generation();
    }

    pub fn spawnable_set(&self) -> &DataHandle<SpawnableSet>
    {
        &self.spawnable_set
    }

    pub fn set_spawnable_set(&mut self, spawnable_set : DataHandle<SpawnableSet>)
    {
        self.spawnable_set = spawnable_set;

        self.next_generation();
    }

    fn next_name_id(&mut self) -> u32
    {
        let mut next_id = 1;

        while self.spawnable_names.ids.contains_key(&next_id) // TODO optimize
        {
            next_id += 1;
        }

        next_id
    }

    pub fn spawnable_name_id(&self, name : &str) -> Option<u32>
    {
        self.spawnable_names.names.get(name).cloned()
    }

    pub fn spawnable_name_id_mut(&mut self, name : &str) -> u32
    {
        match self.spawnable_names.names.get(name).cloned()
        {
            None =>
            {
                let id = self.next_name_id();
                self.spawnable_names.names.insert(String::from(name), id);
                self.spawnable_names.ids.insert(id, String::from(name));

                self.next_generation();

                return id;
            }
            Some(id) => { return id; }
        }
    }

    pub fn spawnable_name_for_id(&self, id : u32) -> Option<&String>
    {
        self.spawnable_names.ids.get(&id)
    }

    pub fn clean_spawnable_names(&mut self)
    {
        let mut defined_set = HashSet::new();
        let mut used_set = HashSet::new();

        for id in self.spawnable_names.ids.keys()
        {
            defined_set.insert(*id);
        }

        for i in 0..self.layers.len()
        {
            if let Some(layer) = self.layers.get(i)
            {
                for spawnable in &layer.spawnables
                {
                    used_set.insert(spawnable.spawn_name_id);
                }
            }
        }

        for id in &defined_set
        {
            if !used_set.contains(id)
            {
                if let Some(name) = self.spawnable_names.ids.remove(id)
                {
                    self.spawnable_names.names.remove(&name);
                }
            }
        }
    }

    pub fn layer(&self, id : usize) -> Option<&LevelLayer>
    {
        self.layers.get(id)
    }

    pub fn set_layer(&mut self, id : usize, layer : LevelLayer) -> bool
    {
        let success = self.layers.set(id, layer);

        if success
        {
            self.next_generation();
        }

        success
    }

    pub fn modify_layer<F : FnMut(&mut LevelLayer)>(&mut self, id : usize, mut func : F) -> bool
    {
        if let Some(layer) = self.layers.get_mut(id)
        {
            func(layer);
            self.next_generation();

            return true;
        }

        false
    }

    pub fn insert_layer(&mut self, pos : usize, layer : LevelLayer) -> bool
    {
        let success = self.layers.insert_item(pos, layer);

        if success
        {
            self.next_generation();
        }

        success
    }

    pub fn remove_layer(&mut self, id : usize) -> Option<LevelLayer>
    {
        let item = self.layers.remove_item(id);

        if item.is_some()
        {
            self.next_generation();
        }

        item
    }

    pub fn rename_layer(&mut self, id : usize, new_name : &str) -> bool
    {
        let success = self.layers.rename_item(id, new_name);

        if success
        {
            self.next_generation();
        }

        success
    }

    pub fn layer_name(&self, id : usize) -> Option<String>
    {
        self.layers.get_name(id)
    }

    pub fn layer_id(&self, name : &str) -> Option<usize>
    {
        self.layers.get_id(name)
    }

    pub fn layer_count(&self) -> usize
    {
        self.layers.len()
    }

    pub fn layer_spawnable_count(&self, id : usize) -> usize
    {
        if let Some(layer) = self.layers.get(id)
        {
            return layer.spawnable_count();
        }

        0
    }

    pub fn spawnable(&self, layer_id : usize, spawnable_id : usize) -> Option<&LevelSpawnable>
    {
        if let Some(layer) = self.layers.get(layer_id)
        {
            return layer.spawnable(spawnable_id);
        }

        None
    }

    pub fn set_spawnable(&mut self, layer_id : usize, spawnable_id : usize, spawnable : LevelSpawnable) -> bool
    {
        if let Some(layer) = self.layers.get_mut(layer_id)
        {
            if let Some(old_spawnable) = layer.spawnable_mut(spawnable_id)
            {
                *old_spawnable = spawnable;
                self.next_generation();

                return true;
            }
        }

        false
    }

    pub fn modify_spawnable<F : FnMut(&mut LevelSpawnable)>(&mut self, layer_id : usize, spawnable_id : usize, mut func : F) -> bool
    {
        if let Some(layer) = self.layers.get_mut(layer_id)
        {
            if let Some(spawnable) = layer.spawnable_mut(spawnable_id)
            {
                func(spawnable);
                self.next_generation();

                return true;
            }
        }

        false
    }

    pub fn insert_spawnable(&mut self, layer_id : usize, spawnable_id : usize, spawnable : LevelSpawnable) -> bool
    {
        if let Some(layer) = self.layers.get_mut(layer_id)
        {
            let success = layer.insert_spawnable(spawnable_id, spawnable);

            if success
            {
                self.next_generation();
            }

            return success;
        }

        false
    }

    pub fn remove_spawnable(&mut self, layer_id : usize, spawnable_id : usize) -> Option<LevelSpawnable>
    {
        if let Some(layer) = self.layers.get_mut(layer_id)
        {
            let item = layer.remove_spawnable(spawnable_id);

            if item.is_some()
            {
                self.next_generation();
            }

            return item;
        }

        None
    }
}

impl DataObject for Level
{
    fn folder_name() -> &'static str where Self : Sized { "levels" }
    fn trust_policy() -> TrustPolicy { TrustPolicy::UntrustedOk }
    fn want_file(pathname : &str) -> bool where Self : Sized { pathname.ends_with(".keelvl") }

    fn from_package_source(source : &mut Box<dyn Source>, package_name : &str, pathname : &str) -> Result<Self, DataError> where Self : Sized
    {
        let reader = source.read_file(package_name, pathname)?;
        let options = bincode::DefaultOptions::new().with_varint_encoding();

        match options.deserialize_from(reader)
        {
            Ok(loaded_data) =>
            {
                return Ok(loaded_data);
            },
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            }
        }
    }

    fn write(&mut self, package_name : &str, pathname : &str, source : &mut Box<dyn Source>) -> Result<(), DataError>
    {
        self.clean_spawnable_names();

        let writer = source.write_file(package_name, pathname).map_err(
            |error| DataError::PackageSourceError(error))?;

        let options = bincode::DefaultOptions::new().with_varint_encoding();

        match options.serialize_into(writer, &self)
        {
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            },
            Ok(_) =>
            {
                info!("Wrote level to {}/{}", package_name, pathname);

                return Ok(());
            }
        }
    }

    fn generation(&self) -> u64
    {
        self.generation
    }

    fn set_generation(&mut self, generation : u64)
    {
        self.generation = generation;
    }
}

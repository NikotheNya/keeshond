use keeshond::scene::{DrawerArgs, DrawerSystem, FreezeFlags, SceneType, ThinkerArgs, ThinkerSystem};
use keeshond::renderer::{DrawBackgroundColorInfo, DrawControl, DrawOptions, DrawOrdering, DrawSpriteInfo, DrawSpriteRawInfo, DrawTransform, Shader, Sheet};
use keeshond::util::NamedItemStore;

use keeshond::datapack::{DataError, DataHandle, DataId, DataObject, DataStore, TrustPolicy};
use keeshond::datapack::source::Source;

use keeshond_derive::Component;

use std::marker::PhantomData;
use std::ops::{Add, Mul, Sub};

use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::Error;
use toodee::{TooDee, TooDeeOps};

use crate::world::{Camera, Transform};
use crate::LoadErrorMode;

pub const DEPTH_RANGE_MULTIPLIER : f32 = 4096.0;
pub const BACKGROUND_COLOR_DEPTH : f32 = -DEPTH_RANGE_MULTIPLIER;

struct ExactStructDefaultIter<T : Default + 'static>
{
    count : usize,
    _phantom : PhantomData<T>
}

impl<T : Default + 'static> ExactStructDefaultIter<T>
{
    pub fn new(count : usize) -> ExactStructDefaultIter<T>
    {
        ExactStructDefaultIter
        {
            count,
            _phantom : PhantomData
        }
    }
}

impl<T : Default + 'static> Iterator for ExactStructDefaultIter<T>
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item>
    {
        if self.count > 0
        {
            self.count -= 1;
            return Some(T::default());
        }

        None
    }
}

impl<T : Default + 'static> ExactSizeIterator for ExactStructDefaultIter<T>
{
    fn len(&self) -> usize
    {
        self.count
    }
}

impl<T : Default + 'static> DoubleEndedIterator for ExactStructDefaultIter<T>
{
    fn next_back(&mut self) -> Option<Self::Item>
    {
        if self.count > 0
        {
            self.count -= 1;
            return Some(T::default());
        }

        None
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum PixelSnap
{
    NoSnap,
    SpriteSnap,
    CameraSnap,
    SubSpriteSnap,
    SpriteAndCameraSnap,
    SubSpriteAndCameraSnap
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub struct Lerpable<T : Copy + Add<Output = T> + Sub<Output = T> + Mul<Output = T>>
{
    pub value : T,
    pub old : T
}

impl<T : Copy + Add<Output = T> + Sub<Output = T> + Mul<Output = T>> Lerpable<T>
{
    pub fn new(value : T) -> Lerpable<T>
    {
        Lerpable::<T>
        {
            value,
            old : value
        }
    }

    #[inline(always)]
    pub fn lerped(&self, interpolation : T) -> T
    {
        self.value + (self.value - self.old) * interpolation
    }

    pub fn new_frame_set(&mut self, value : T)
    {
        self.old = self.value;
        self.value = value;
    }

    pub fn teleport(&mut self, value : T)
    {
        self.value = value;
        self.old = value;
    }

    pub fn new_frame(&mut self)
    {
        self.old = self.value;
    }
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub struct LerpedTransform<T : Copy + Add<Output = T> + Sub<Output = T> + Mul<Output = T>>
{
    pub x : Lerpable<T>,
    pub y : Lerpable<T>,
    pub angle : Lerpable<T>,
    pub scale_x : Lerpable<T>,
    pub scale_y : Lerpable<T>
}

#[derive(PartialEq, Clone, Debug)]
pub enum DrawableItem
{
    None,
    BackgroundColor(BackgroundColor),
    Sprite(Sprite),
    TileSprite(TileSprite),
    MultiSliceSprite(MultiSliceSprite),
    MultiTileSprite(MultiTileSprite)
}

impl DrawableItem
{
    pub fn is_some(&self) -> bool
    {
        *self != DrawableItem::None
    }

    pub fn modify_background_color<F : FnMut(&mut BackgroundColor)>(&mut self, mut func : F)
    {
        match self
        {
            DrawableItem::BackgroundColor(_) => (),
            _ => *self = DrawableItem::BackgroundColor(BackgroundColor::default())
        }

        if let DrawableItem::BackgroundColor(inner) = self
        {
            func(inner);
        }
    }

    pub fn modify_sprite<F : FnMut(&mut Sprite)>(&mut self, mut func : F)
    {
        match self
        {
            DrawableItem::Sprite(_) => (),
            _ => *self = DrawableItem::Sprite(Sprite::new())
        }

        if let DrawableItem::Sprite(inner) = self
        {
            func(inner);
        }
    }

    pub fn modify_tile<F : FnMut(&mut TileSprite)>(&mut self, mut func : F)
    {
        match self
        {
            DrawableItem::TileSprite(_) => (),
            _ => *self = DrawableItem::TileSprite(TileSprite::new())
        }

        if let DrawableItem::TileSprite(inner) = self
        {
            func(inner);
        }
    }

    pub fn modify_multi_slice_sprite<F : FnMut(&mut MultiSliceSprite)>(&mut self, mut func : F)
    {
        match self
        {
            DrawableItem::MultiSliceSprite(_) => (),
            _ => *self = DrawableItem::MultiSliceSprite(MultiSliceSprite::new())
        }

        if let DrawableItem::MultiSliceSprite(inner) = self
        {
            func(inner);
        }
    }

    pub fn modify_multi_tile_sprite<F : FnMut(&mut MultiTileSprite)>(&mut self, mut func : F)
    {
        match self
        {
            DrawableItem::MultiTileSprite(_) => (),
            _ => *self = DrawableItem::MultiTileSprite(MultiTileSprite::new())
        }

        if let DrawableItem::MultiTileSprite(inner) = self
        {
            func(inner);
        }
    }
}

/// Data for drawing a single object. This component is not intended to be used on its own, but
///  managed by another component, such as [Actor], [Prop], or [Text]. If you want to make your own
///  component for this purpose, you need to make sure that you hold onto any [DataHandle] objects
///  pointing to resources used in the [Drawable]. Additionally, you will need to make sure that
///  any [Lerpable] values are either set from a [Transform] component at the end of each frame, or
///  that [Lerpable::new_frame()] is called at the start of each frame.
#[derive(Component, Debug, Clone)]
pub struct Drawable
{
    pub item : DrawableItem,
    pub transform : LerpedTransform<f32>,
    pub scroll_rate_x : f32,
    pub scroll_rate_y : f32,
    pub depth : f32,
    pub visible : bool,
    pub lerp_enabled : bool
}

impl Drawable
{
    pub fn new() -> Drawable
    {
        Drawable::with_item(DrawableItem::None)
    }

    pub fn with_item(item : DrawableItem) -> Drawable
    {
        Drawable
        {
            item,
            transform : LerpedTransform
            {
                x : Lerpable::new(0.0),
                y : Lerpable::new(0.0),
                angle : Lerpable::new(0.0),
                scale_x : Lerpable::new(1.0),
                scale_y : Lerpable::new(1.0)
            },
            scroll_rate_x: 1.0,
            scroll_rate_y: 1.0,
            depth : 0.0,
            visible : true,
            lerp_enabled : false
        }
    }
    
    pub fn with_background_color(r : f32, g : f32, b : f32) -> Drawable
    {
        Drawable
        {
            item : DrawableItem::BackgroundColor(BackgroundColor
            {
                r,
                g,
                b,
                alpha : 1.0,
                shader_id : DataId::new(0)
            }),
            transform : LerpedTransform
            {
                x : Lerpable::new(0.0),
                y : Lerpable::new(0.0),
                angle : Lerpable::new(0.0),
                scale_x : Lerpable::new(1.0),
                scale_y : Lerpable::new(1.0)
            },
            scroll_rate_x : 1.0,
            scroll_rate_y : 1.0,
            depth : BACKGROUND_COLOR_DEPTH,
            visible : true,
            lerp_enabled : false
        }
    }

    pub fn update_from_transform(&mut self, transform : &mut Transform)
    {
        if !transform.lerp_suspended && self.lerp_enabled
        {
            self.transform.x.new_frame_set(transform.x() as f32);
            self.transform.y.new_frame_set(transform.y() as f32);
            self.transform.angle.new_frame_set(transform.angle() as f32);
            self.transform.scale_x.new_frame_set(transform.scale_x() as f32);
            self.transform.scale_y.new_frame_set(transform.scale_y() as f32);
        }
        else
        {
            self.transform.x.teleport(transform.x() as f32);
            self.transform.y.teleport(transform.y() as f32);
            self.transform.angle.teleport(transform.angle() as f32);
            self.transform.scale_x.teleport(transform.scale_x() as f32);
            self.transform.scale_y.teleport(transform.scale_y() as f32);
            self.lerp_enabled = true;
            transform.lerp_suspended = false;
        }

        self.depth = transform.z() as f32;
    }
}

#[derive(PartialEq, Clone, Debug)]
pub struct BackgroundColor
{
    pub shader_id : DataId<Shader>,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
}

impl BackgroundColor
{
    pub fn new() -> BackgroundColor
    {
        BackgroundColor
        {
            shader_id : DataId::new(0),
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0
        }
    }
}

impl Default for BackgroundColor
{
    fn default() -> Self { BackgroundColor::new() }
}

#[derive(PartialEq, Clone, Debug)]
pub struct Sprite
{
    pub sheet_id : DataId<Sheet>,
    pub slice_id : usize,
    pub shader_id : DataId<Shader>,
    pub offset_x : f32,
    pub offset_y : f32,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
}

impl Sprite
{
    pub fn new() -> Sprite
    {
        Sprite
        {
            sheet_id : DataId::new(0),
            slice_id : 0,
            shader_id : DataId::new(0),
            offset_x : 0.0,
            offset_y : 0.0,
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0
        }
    }
}

impl Default for Sprite
{
    fn default() -> Self { Sprite::new() }
}

#[derive(PartialEq, Clone, Debug)]
pub struct TileSprite
{
    pub texture_x : f32,
    pub texture_y : f32,
    pub texture_w : f32,
    pub texture_h : f32,
    pub tile_w : f32,
    pub tile_h : f32,
    pub offset_x : f32,
    pub offset_y : f32,
    pub sheet_id : DataId<Sheet>,
    pub shader_id : DataId<Shader>,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
}

impl TileSprite
{
    pub fn new() -> TileSprite
    {
        TileSprite
        {
            texture_x : 0.0,
            texture_y : 0.0,
            texture_w : 1.0,
            texture_h : 1.0,
            tile_w : 32.0,
            tile_h : 32.0,
            offset_x : 0.0,
            offset_y : 0.0,
            sheet_id : DataId::new(0),
            shader_id : DataId::new(0),
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0
        }
    }
}

impl Default for TileSprite
{
    fn default() -> Self { TileSprite::new() }
}

#[derive(PartialEq, Clone, Debug)]
pub struct PositionedSlice
{
    pub slice_id : usize,
    pub x : f32,
    pub y : f32
}

#[derive(PartialEq, Clone, Debug)]
pub struct MultiSliceSprite
{
    pub sheet_id : DataId<Sheet>,
    pub slices : Vec<PositionedSlice>,
    pub shader_id : DataId<Shader>,
    pub offset_x : f32,
    pub offset_y : f32,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
}

impl MultiSliceSprite
{
    pub fn new() -> MultiSliceSprite
    {
        MultiSliceSprite
        {
            sheet_id : DataId::new(0),
            slices : Vec::new(),
            shader_id : DataId::new(0),
            offset_x : 0.0,
            offset_y : 0.0,
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0
        }
    }
}

impl Default for MultiSliceSprite
{
    fn default() -> Self { MultiSliceSprite::new() }
}

#[derive(PartialEq, Clone, Debug)]
pub struct PositionedTile
{
    pub x : f32,
    pub y : f32,
    pub texture_x : f32,
    pub texture_y : f32,
    pub texture_w : f32,
    pub texture_h : f32
}

#[derive(PartialEq, Clone, Debug)]
pub struct MultiTileSprite
{
    pub tile_w : f32,
    pub tile_h : f32,
    pub offset_x : f32,
    pub offset_y : f32,
    pub sheet_id : DataId<Sheet>,
    pub tiles : Vec<PositionedTile>,
    pub shader_id : DataId<Shader>,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
}

impl MultiTileSprite
{
    pub fn new() -> MultiTileSprite
    {
        MultiTileSprite
        {
            tile_w : 32.0,
            tile_h : 32.0,
            offset_x : 0.0,
            offset_y : 0.0,
            sheet_id : DataId::new(0),
            tiles : Vec::new(),
            shader_id : DataId::new(0),
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0
        }
    }
}

impl Default for MultiTileSprite
{
    fn default() -> Self { MultiTileSprite::new() }
}

struct SimpleDrawTransform
{
    x : f32,
    y : f32,
    angle : f32,
    scale_x : f32,
    scale_y : f32,
    depth : f32
}

pub struct DrawableDrawer
{
    sprite_pixel_snap : bool,
    subsprite_pixel_snap : bool,
    camera_pixel_snap : bool
}

impl DrawableDrawer
{
    pub fn new(pixel_snap : PixelSnap) -> DrawableDrawer
    {
        let mut sprite_pixel_snap = false;
        let mut subsprite_pixel_snap = false;
        let mut camera_pixel_snap = false;

        match pixel_snap
        {
            PixelSnap::NoSnap => {}
            PixelSnap::SpriteSnap =>
            {
                sprite_pixel_snap = true;
            }
            PixelSnap::CameraSnap =>
            {
                camera_pixel_snap = true;
            }
            PixelSnap::SubSpriteSnap =>
            {
                sprite_pixel_snap = true;
                subsprite_pixel_snap = true;
            }
            PixelSnap::SpriteAndCameraSnap =>
            {
                sprite_pixel_snap = true;
                camera_pixel_snap = true;
            }
            PixelSnap::SubSpriteAndCameraSnap =>
            {
                sprite_pixel_snap = true;
                subsprite_pixel_snap = true;
                camera_pixel_snap = true;
            }
        }

        DrawableDrawer
        {
            sprite_pixel_snap,
            subsprite_pixel_snap,
            camera_pixel_snap
        }
    }

    pub fn draw_drawable(&self, drawing : &mut Box<dyn DrawControl>, interpolation : f32, camera_transform : &DrawTransform, drawable : &Drawable)
    {
        let transform = SimpleDrawTransform
        {
            x : drawable.transform.x.lerped(interpolation),
            y : drawable.transform.y.lerped(interpolation),
            angle : drawable.transform.angle.lerped(interpolation),
            scale_x : drawable.transform.scale_x.lerped(interpolation),
            scale_y : drawable.transform.scale_y.lerped(interpolation),
            depth : drawable.depth / DEPTH_RANGE_MULTIPLIER
        };
        let ordering = DrawOrdering::OrderedAutomatic;

        match &drawable.item
        {
            DrawableItem::None => {}
            DrawableItem::BackgroundColor(background_color) =>
            {
                drawing.draw_background_color(&DrawBackgroundColorInfo
                {
                    r : background_color.r,
                    g : background_color.g,
                    b : background_color.b,
                    alpha : background_color.alpha,
                    depth : drawable.depth / DEPTH_RANGE_MULTIPLIER,
                    shader_id : background_color.shader_id
                },
                &DrawOptions
                {
                    ordering
                });
            }
            DrawableItem::Sprite(sprite) =>
            {
                self.draw_item_sprite(sprite, drawing, transform, camera_transform, ordering);
            }
            DrawableItem::TileSprite(sprite) =>
            {
                self.draw_item_tile(sprite, drawing, transform, camera_transform, ordering);
            }
            DrawableItem::MultiSliceSprite(multi_slice) =>
            {
                self.draw_item_multi_slice(multi_slice, drawing, transform, camera_transform, ordering);
            }
            DrawableItem::MultiTileSprite(multi_tile) =>
            {
                self.draw_item_multi_tile(multi_tile, drawing, transform, camera_transform, ordering);
            }
        }
    }

    fn draw_item_sprite(&self, item : &Sprite, drawing : &mut Box<dyn DrawControl>, mut transform : SimpleDrawTransform,
                        camera_transform : &DrawTransform, ordering : DrawOrdering)
    {
        transform.x += item.offset_x;
        transform.y += item.offset_y;

        if self.sprite_pixel_snap
        {
            transform.x = (transform.x + 0.5).floor();
            transform.y = (transform.y + 0.5).floor();
        }

        let info = DrawSpriteInfo
        {
            sheet_id : item.sheet_id,
            shader_id : item.shader_id,
            x : transform.x,
            y : transform.y,
            angle : transform.angle,
            scale_x : transform.scale_x,
            scale_y : transform.scale_y,
            slice : item.slice_id,
            transform : camera_transform.clone(),
            r : item.r,
            g : item.g,
            b : item.b,
            alpha : item.alpha,
            depth : transform.depth
        };

        drawing.draw_sprite(&info, &DrawOptions
        {
            ordering
        });
    }

    fn draw_item_tile(&self, item : &TileSprite, drawing : &mut Box<dyn DrawControl>, mut transform : SimpleDrawTransform,
                      camera_transform : &DrawTransform, ordering : DrawOrdering)
    {
        if self.sprite_pixel_snap
        {
            transform.x = (transform.x + 0.5).floor();
            transform.y = (transform.y + 0.5).floor();
        }

        let mut root_transform = camera_transform.clone();
        root_transform.translate(transform.x, transform.y);
        root_transform.rotate(transform.angle);
        root_transform.scale(transform.scale_x * item.tile_w, transform.scale_y * item.tile_h);

        let info = DrawSpriteRawInfo
        {
            sheet_id : item.sheet_id,
            shader_id: item.shader_id,
            transform : root_transform,
            transform_offset_x : item.offset_x,
            transform_offset_y : item.offset_y,
            texture_x : item.texture_x,
            texture_y : item.texture_y,
            texture_w : item.texture_w,
            texture_h : item.texture_h,
            r : item.r,
            g : item.g,
            b : item.b,
            alpha : item.alpha,
            depth : transform.depth
        };

        drawing.draw_sprite_raw(&info, &DrawOptions
        {
            ordering
        });
    }

    fn draw_item_multi_slice(&self, item : &MultiSliceSprite, drawing : &mut Box<dyn DrawControl>, mut transform : SimpleDrawTransform,
                             camera_transform : &DrawTransform, ordering : DrawOrdering)
    {
        // TODO: More useful offset
        transform.x += item.offset_x;
        transform.y += item.offset_y;

        if self.sprite_pixel_snap
        {
            transform.x = (transform.x + 0.5).floor();
            transform.y = (transform.y + 0.5).floor();
        }

        let mut root_transform = camera_transform.clone();

        root_transform.translate(transform.x, transform.y);
        root_transform.rotate(transform.angle);
        root_transform.scale(transform.scale_x, transform.scale_y);

        let mut info = DrawSpriteInfo
        {
            sheet_id : item.sheet_id,
            shader_id : item.shader_id,
            x : 0.0,
            y : 0.0,
            angle : 0.0,
            scale_x : 1.0,
            scale_y : 1.0,
            slice : 0,
            transform : root_transform,
            r : item.r,
            g : item.g,
            b : item.b,
            alpha : item.alpha,
            depth : transform.depth
        };

        let options = DrawOptions
        {
            ordering
        };

        for slice in &item.slices
        {
            info.x = slice.x;
            info.y = slice.y;

            if self.subsprite_pixel_snap
            {
                info.x = (info.x + 0.5).floor();
                info.y = (info.y + 0.5).floor();
            }

            info.slice = slice.slice_id;

            drawing.draw_sprite(&info, &options);
        }
    }

    fn draw_item_multi_tile(&self, item : &MultiTileSprite, drawing : &mut Box<dyn DrawControl>, mut transform : SimpleDrawTransform,
                            camera_transform : &DrawTransform, ordering : DrawOrdering)
    {
        if self.sprite_pixel_snap
        {
            transform.x = (transform.x + 0.5).floor();
            transform.y = (transform.y + 0.5).floor();
        }

        let mut root_transform = camera_transform.clone();

        root_transform.translate(transform.x, transform.y);
        root_transform.rotate(transform.angle);
        root_transform.scale(transform.scale_x * item.tile_w, transform.scale_y * item.tile_h);
        root_transform.translate(item.offset_x, item.offset_y);

        let mut info = DrawSpriteRawInfo
        {
            sheet_id : item.sheet_id,
            shader_id : item.shader_id,
            transform : root_transform.clone(),
            transform_offset_x : 0.0,
            transform_offset_y : 0.0,
            texture_x : 0.0,
            texture_y : 0.0,
            texture_w : 0.0,
            texture_h : 0.0,
            r : item.r,
            g : item.g,
            b : item.b,
            alpha : item.alpha,
            depth : transform.depth
        };

        let options = DrawOptions
        {
            ordering
        };

        for tile in &item.tiles
        {
            info.transform = root_transform.clone();

            info.transform.translate(tile.x, tile.y);

            info.texture_x = tile.texture_x;
            info.texture_y = tile.texture_y;
            info.texture_w = tile.texture_w;
            info.texture_h = tile.texture_h;

            drawing.draw_sprite_raw(&info, &options);
        }
    }
}

impl DrawerSystem for DrawableDrawer
{
    fn draw(&self, args : DrawerArgs)
    {
        let drawable_store = args.components.store::<Drawable>();
        let camera_store = args.components.store::<Camera>();

        let mut first_camera = true;

        for (_, camera) in camera_store.iter()
        {
            let mut camera_transform = args.transform.clone();
            let mut scroll_transform = camera_transform.clone();

            let mut cam_interpolate_x;
            let mut cam_interpolate_y;
            let mut last_scroll_rate_x = 0.0;
            let mut last_scroll_rate_y = 0.0;

            let mut screen_area = camera.screen_area.clone();

            cam_interpolate_x = camera.x as f32 + (camera.x - camera.older_x) as f32 * args.interpolation;
            cam_interpolate_y = camera.y as f32 + (camera.y - camera.older_y) as f32 * args.interpolation;

            if self.camera_pixel_snap
            {
                screen_area.x = (screen_area.x + 0.5).floor();
                screen_area.y = (screen_area.y + 0.5).floor();
                screen_area.w = (screen_area.w + 0.5).floor();
                screen_area.h = (screen_area.h + 0.5).floor();
                cam_interpolate_x = (cam_interpolate_x + 0.5).floor();
                cam_interpolate_y = (cam_interpolate_y + 0.5).floor();
            }

            camera_transform.translate(screen_area.x, screen_area.y);
            args.drawing.set_scissor_area(Some(screen_area));

            if !first_camera
            {
                // Clear Z-buffer in this area
                args.drawing.draw_background_color(&DrawBackgroundColorInfo
                {
                    r : 0.0,
                    g : 0.0,
                    b : 0.0,
                    alpha : 0.0,
                    depth : -1.0,
                    shader_id : DataId::new(0)
                }, &DrawOptions
                {
                    ordering : DrawOrdering::PainterZWrite
                });
            }

            first_camera = false;

            for (_, drawable, _) in drawable_store.iter_filtered(FreezeFlags::REASON_CULLED | FreezeFlags::REASON_HIDDEN)
            {
                if !drawable.visible
                {
                    continue;
                }

                if drawable.scroll_rate_x != last_scroll_rate_x || drawable.scroll_rate_y != last_scroll_rate_y
                {
                    scroll_transform = camera_transform.clone();
                    let mut scrolled_x = -cam_interpolate_x * drawable.scroll_rate_x;
                    let mut scrolled_y = -cam_interpolate_y * drawable.scroll_rate_y;

                    if self.camera_pixel_snap
                    {
                        scrolled_x = (scrolled_x + 0.5).floor();
                        scrolled_y = (scrolled_y + 0.5).floor();
                    }

                    scroll_transform.translate(scrolled_x, scrolled_y);

                    last_scroll_rate_x = drawable.scroll_rate_x;
                    last_scroll_rate_y = drawable.scroll_rate_y;
                }

                self.draw_drawable(args.drawing, args.interpolation, &mut scroll_transform, &drawable);
            }
        }
    }
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct SliceHandle
{
    #[serde(rename = "sli")]
    #[serde(alias = "slice")]
    slice : String,
    #[serde(skip)]
    slice_id : usize,
    #[serde(skip)]
    generation : u64
}

impl SliceHandle
{
    pub fn new(slice : &str) -> SliceHandle
    {
        SliceHandle
        {
            slice : slice.to_string(),
            slice_id : usize::MAX,
            generation : 0
        }
    }

    pub fn null() -> SliceHandle
    {
        SliceHandle
        {
            slice : String::new(),
            slice_id : usize::MAX,
            generation : 0
        }
    }

    pub fn slice(&self) -> &String
    {
        &self.slice
    }

    #[inline(always)]
    pub fn updated_slice_id(&mut self, sheet : &Sheet) -> usize
    {
        if self.generation == sheet.generation()
        {
            return self.slice_id;
        }

        self.slice_id = sheet.slice_id(&self.slice).unwrap_or(std::usize::MAX);

        self.generation = sheet.generation();

        self.slice_id
    }

    pub fn invalidate(&mut self)
    {
        self.generation = 0;
        self.slice_id = usize::MAX;
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum SliceIndexHandleEnum
{
    #[serde(rename = "i")]
    Index(usize),
    #[serde(rename = "n")]
    Named(SliceHandle)
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SliceIndexHandle
{
    #[serde(flatten)]
    handle : SliceIndexHandleEnum
}

impl SliceIndexHandle
{
    pub fn with_index(index : usize) -> SliceIndexHandle
    {
        SliceIndexHandle
        {
            handle : SliceIndexHandleEnum::Index(index)
        }
    }

    pub fn with_name(name : &str) -> SliceIndexHandle
    {
        SliceIndexHandle
        {
            handle : SliceIndexHandleEnum::Named(SliceHandle::new(name))
        }
    }

    pub fn name(&self) -> Option<&String>
    {
        match &self.handle
        {
            SliceIndexHandleEnum::Index(_) =>
            {
                None
            }
            SliceIndexHandleEnum::Named(named) =>
            {
                Some(&named.slice)
            }
        }
    }

    #[inline(always)]
    pub fn updated_slice_id(&mut self, sheet : &Sheet) -> usize
    {
        match &mut self.handle
        {
            SliceIndexHandleEnum::Index(index) =>
            {
                *index
            }
            SliceIndexHandleEnum::Named(named) =>
            {
                if named.generation == sheet.generation()
                {
                    return named.slice_id;
                }

                named.slice_id = sheet.slice_id(&named.slice).unwrap_or(std::usize::MAX);
                named.generation = sheet.generation();

                named.slice_id
            }
        }
    }

    pub fn invalidate(&mut self)
    {
        if let SliceIndexHandleEnum::Named(named) = &mut self.handle
        {
            named.generation = 0;
            named.slice_id = usize::MAX;
        }
    }
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct AnimationCel
{
    #[serde(rename = "she")]
    #[serde(alias = "sheet")]
    pub sheet : DataHandle<Sheet>,
    #[serde(flatten)]
    pub slice : SliceHandle,
    pub x : f32,
    pub y : f32
}

impl AnimationCel
{
    pub fn new() -> AnimationCel
    {
        AnimationCel
        {
            sheet : DataHandle::null(),
            slice : SliceHandle::null(),
            x : 0.0,
            y : 0.0
        }
    }
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct AnimationFrame
{
    #[serde(rename = "dly")]
    #[serde(alias = "delay")]
    pub delay : f64
}

impl AnimationFrame
{
    pub fn new() -> AnimationFrame
    {
        AnimationFrame
        {
            delay : 4.0
        }
    }
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct AnimationTrack
{
    #[serde(rename = "id")]
    #[serde(alias = "track_id")]
    pub track_id : u16
}

impl AnimationTrack
{
    pub fn new(track_id : u16) -> AnimationTrack
    {
        AnimationTrack
        {
            track_id : track_id
        }
    }
}

#[derive(Clone, Debug)]
pub struct Animation
{
    cels : TooDee<AnimationCel>,
    frames : Vec<AnimationFrame>,
    tracks : Vec<AnimationTrack>,
    track_id_to_row : Vec<Option<usize>>,
    looping : bool
}

impl Animation
{
    pub fn new() -> Animation
    {
        Animation
        {
            cels : TooDee::new(0, 0),
            frames : Vec::new(),
            tracks : Vec::new(),
            track_id_to_row : Vec::new(),
            looping : false
        }
    }

    pub fn cel(&self, frame : usize, track_row : usize) -> Option<&AnimationCel>
    {
        if frame < self.cels.num_cols() && track_row < self.cels.num_rows()
        {
            return Some(&self.cels[(frame, track_row)])
        }

        None
    }

    fn cel_mut(&mut self, frame : usize, track_row : usize) -> Option<&mut AnimationCel>
    {
        if frame < self.cels.num_cols() && track_row < self.cels.num_rows()
        {
            return Some(&mut self.cels[(frame, track_row)])
        }

        None
    }

    pub fn set_cel(&mut self, frame : usize, track_row : usize, cel : AnimationCel) -> bool
    {
        if frame < self.cels.num_cols() && track_row < self.cels.num_rows()
        {
            self.cels[(frame, track_row)] = cel;
            return true;
        }

        false
    }

    pub fn updated_slice_id(&mut self, frame : usize, track_row : usize,
                            sheet_res : &mut DataStore<Sheet>, load_error : LoadErrorMode) -> usize
    {
        if frame < self.cels.num_cols() && track_row < self.cels.num_rows()
        {
            let cel = &mut self.cels[(frame, track_row)];
            let sheet_id = cel.sheet.resolve(sheet_res, load_error);

            if let Some(sheet) = sheet_res.get(sheet_id)
            {
                return cel.slice.updated_slice_id(sheet);
            }
        }

        0
    }

    pub fn add_frame(&mut self, frame : AnimationFrame) -> usize
    {
        let pos = self.frames.len();
        self.insert_frame(pos, frame);

        pos
    }

    pub fn insert_frame(&mut self, pos : usize, frame : AnimationFrame) -> bool
    {
        if pos > self.frames.len()
        {
            return false;
        }

        self.frames.insert(pos, frame);

        if self.cels.is_empty()
        {
            if self.tracks.len() > 0
            {
                self.cels = TooDee::from_vec(1, self.tracks.len(), vec![AnimationCel::default(); self.tracks.len()]);
            }
        }
        else
        {
            let num_rows = self.cels.num_rows();
            self.cels.insert_col(pos, ExactStructDefaultIter::new(num_rows));
        }

        true
    }

    pub fn set_frame(&mut self, pos : usize, frame : AnimationFrame) -> bool
    {
        if pos >= self.frames.len()
        {
            return false;
        }

        self.frames[pos] = frame;

        true
    }

    pub fn remove_frame(&mut self, pos : usize) -> bool
    {
        if pos >= self.frames.len()
        {
            return false;
        }

        self.frames.remove(pos);

        if self.cels.num_cols() > 0
        {
            self.cels.remove_col(pos);
        }

        true
    }

    pub fn move_frame(&mut self, old_pos : usize, new_pos : usize) -> bool
    {
        if old_pos >= self.frames.len() || new_pos > self.frames.len().saturating_sub(1)
        {
            return false;
        }

        let moved_frame = self.frames.remove(old_pos);
        self.frames.insert(new_pos, moved_frame);

        if self.cels.num_cols() > 0
        {
            let moved_col : Vec<AnimationCel> = self.cels.remove_col(old_pos).collect();
            self.cels.insert_col(new_pos, moved_col);
        }

        true
    }

    pub fn add_track(&mut self, track : AnimationTrack) -> usize
    {
        let row = self.tracks.len();
        self.insert_track(row, track);

        row
    }

    pub fn insert_track(&mut self, row : usize, track : AnimationTrack) -> bool
    {
        if row > self.tracks.len()
        {
            return false;
        }

        self.tracks.insert(row, track);

        if self.cels.is_empty()
        {
            if self.frames.len() > 0
            {
                self.cels = TooDee::from_vec(self.frames.len(), 1, vec![AnimationCel::default(); self.frames.len()]);
            }
        }
        else
        {
            let num_cols = self.cels.num_cols();
            self.cels.insert_row(row, ExactStructDefaultIter::new(num_cols));
        }

        self.update_track_id_to_row();

        true
    }

    pub fn remove_track(&mut self, row : usize) -> bool
    {
        if row >= self.tracks.len()
        {
            return false;
        }

        self.tracks.remove(row);

        if self.cels.num_rows() > 0
        {
            self.cels.remove_row(row);
        }

        self.update_track_id_to_row();

        true
    }

    pub fn move_track(&mut self, old_row : usize, new_row : usize) -> bool
    {
        if old_row >= self.tracks.len() || new_row > self.tracks.len().saturating_sub(1)
        {
            return false;
        }

        let moved_track = self.tracks.remove(old_row);
        self.tracks.insert(new_row, moved_track);

        if self.cels.num_rows() > 0
        {
            let moved_row : Vec<AnimationCel> = self.cels.remove_row(old_row).collect();
            self.cels.insert_row(new_row, moved_row);
        }

        self.update_track_id_to_row();

        true
    }

    fn update_track_id_to_row(&mut self)
    {
        self.track_id_to_row.clear();

        for row in 0..self.tracks.len()
        {
            let track_id = self.tracks[row].track_id as usize;

            while self.track_id_to_row.len() <= track_id
            {
                self.track_id_to_row.push(None);
            }

            self.track_id_to_row[track_id] = Some(row);
        }
    }

    pub fn costume_track_to_row(&self, costume_track_id : u16) -> Option<usize>
    {
        if (costume_track_id as usize) < self.track_id_to_row.len()
        {
            return self.track_id_to_row[costume_track_id as usize]
        }

        None
    }

    pub fn frame(&self, index : usize) -> Option<&AnimationFrame>
    {
        self.frames.get(index)
    }

    pub fn track(&self, track_row : usize) -> Option<&AnimationTrack>
    {
        self.tracks.get(track_row)
    }

    pub fn frame_count(&self) -> usize
    {
        self.frames.len()
    }

    pub fn track_count(&self) -> usize
    {
        self.tracks.len()
    }

    pub fn looping(&self) -> bool
    {
        self.looping
    }

    pub fn set_looping(&mut self, looping : bool)
    {
        self.looping = looping
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct AnimationRaw
{
    cels : Vec<AnimationCel>,
    #[serde(rename = "fra")]
    #[serde(alias = "frames")]
    frames : Vec<AnimationFrame>,
    #[serde(rename = "tra")]
    #[serde(alias = "tracks")]
    tracks : Vec<AnimationTrack>,
    #[serde(rename = "loop")]
    #[serde(alias = "looping")]
    looping : bool
}

impl Serialize for Animation
{
    fn serialize<S>(&self, serializer : S) -> Result<S::Ok, S::Error> where S : Serializer
    {
        let anim_raw = AnimationRaw
        {
            cels : self.cels.data().to_vec(),
            frames : self.frames.clone(),
            tracks : self.tracks.clone(),
            looping : self.looping
        };

        anim_raw.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Animation
{
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
        where D : Deserializer<'de>
    {
        let mut anim_raw = AnimationRaw::deserialize(deserializer)?;

        if anim_raw.frames.is_empty() || anim_raw.tracks.is_empty()
        {
            if !anim_raw.cels.is_empty()
            {
                warn!("Animation cels found with invalid frames/tracks dimensions, cels will be removed.");
            }

            anim_raw.frames.clear();
            anim_raw.tracks.clear();
            anim_raw.cels.clear();
        }
        
        if anim_raw.cels.len() != anim_raw.frames.len() * anim_raw.tracks.len()
        {
            return Err(D::Error::custom(format!("Data length {} doesn't match frame count {} and track count {}",
                                                anim_raw.cels.len(), anim_raw.frames.len(), anim_raw.tracks.len())));
        }
        
        let mut anim = Animation
        {
            cels : TooDee::from_vec(anim_raw.frames.len(), anim_raw.tracks.len(), anim_raw.cels),
            frames : anim_raw.frames,
            tracks: anim_raw.tracks,
            track_id_to_row: Vec::new(),
            looping : anim_raw.looping
        };

        anim.update_track_id_to_row();

        Ok(anim)
    }
}

pub type AnimationId = usize;


#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Costume
{
    #[serde(rename = "anim")]
    #[serde(alias = "animations")]
    animations : NamedItemStore<Animation>,
    #[serde(rename = "trna")]
    #[serde(alias = "track_names")]
    track_names : NamedItemStore<()>
}

impl Costume
{
    pub fn new() -> Costume
    {
        Costume
        {
            animations : NamedItemStore::new(),
            track_names : NamedItemStore::new()
        }
    }

    pub fn animation(&self, id : usize) -> Option<&Animation>
    {
        self.animations.get(id)
    }

    fn animation_mut(&mut self, id : usize) -> Option<&mut Animation>
    {
        self.animations.get_mut(id)
    }

    pub fn set_animation(&mut self, id : usize, animation : Animation) -> bool
    {
        self.animations.set(id, animation)
    }

    pub fn animation_name(&self, id : usize) -> Option<String>
    {
        self.animations.get_name(id)
    }

    pub fn animation_id(&self, name : &str) -> Option<usize>
    {
        self.animations.get_id(name)
    }

    pub fn add_animation(&mut self, animation : Animation) -> usize
    {
        let pos = self.animations.len();
        self.insert_animation(pos, animation);

        pos
    }

    pub fn insert_animation(&mut self, pos : usize, animation : Animation) -> bool
    {
        self.animations.insert_item(pos, animation)
    }

    pub fn remove_animation(&mut self, id : usize) -> Option<Animation>
    {
        self.animations.remove_item(id)
    }

    pub fn rename_animation(&mut self, id : usize, new_name : &str) -> bool
    {
        self.animations.rename_item(id, new_name)
    }

    pub fn animation_count(&self) -> usize
    {
        self.animations.len()
    }

    pub fn add_track(&mut self, name : &str) -> usize
    {
        let pos = self.track_names.len();
        self.insert_track(self.track_names.len(), name);

        pos
    }

    pub fn insert_track(&mut self, pos : usize, name : &str) -> bool
    {
        if self.track_names.insert_item(pos, ())
        {
            self.track_names.rename_item(pos, name);

            return true;
        }

        false
    }

    pub fn remove_track(&mut self, pos : usize) -> bool
    {
        self.track_names.remove_item(pos).is_some()
    }

    pub fn rename_track(&mut self, id : usize, new_name : &str) -> bool
    {
        self.track_names.rename_item(id, new_name)
    }

    pub fn track_name(&self, id : usize) -> Option<String>
    {
        self.track_names.get_name(id)
    }

    pub fn track_id(&self, name : &str) -> Option<usize>
    {
        self.track_names.get_id(name)
    }

    pub fn track_count(&self) -> usize
    {
        self.track_names.len()
    }
}

impl DataObject for Costume
{
    fn folder_name() -> &'static str where Self : Sized
    {
        "costumes"
    }
    fn trust_policy() -> TrustPolicy { TrustPolicy::UntrustedOk }
    fn want_file(pathname: &str) -> bool where Self : Sized
    {
        pathname.ends_with(".json")
    }
    fn from_package_source(source : &mut Box<dyn Source>, package_name : &str, pathname : &str) -> Result<Self, DataError> where Self : Sized
    {
        let mut reader = source.read_file(package_name, pathname)?;
        let mut json_text = String::new();
        let data_result = reader.read_to_string(&mut json_text);

        match data_result
        {
            Err(error) =>
            {
                return Err(DataError::IoError(error));
            },
            _ => {}
        }

        match serde_json::from_str(&json_text)
        {
            Ok(loaded_data) =>
            {
                return Ok(loaded_data);
            },
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            }
        }
    }

    fn write(&mut self, package_name : &str, pathname : &str, source : &mut Box<dyn Source>) -> Result<(), DataError>
    {
        let mut writer = source.write_file(package_name, pathname).map_err(
                |error| DataError::PackageSourceError(error))?;

        match serde_json::to_string(&self)
        {
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            },
            Ok(json_text) =>
            {
                match writer.write_all(json_text.as_bytes())
                {
                    Err(error) =>
                    {
                        return Err(DataError::IoError(error));
                    },
                    _ => {}
                }
            }
        }

        info!("Wrote costume to {}/{}", package_name, pathname);

        Ok(())
    }
}

#[derive(Component)]
pub struct Prop
{
    sheet : DataHandle<Sheet>,
    slice_id : usize,
    dirty : bool
}

impl Prop
{
    pub fn new() -> Prop
    {
        Prop
        {
            sheet : DataHandle::default(),
            slice_id : 0,
            dirty : true
        }
    }

    pub fn with_sheet(sheet : DataHandle<Sheet>, slice_name : &str, store : &mut DataStore<Sheet>, load_error : LoadErrorMode) -> Prop
    {
        let mut slice_id = 0;

        if let Some(sheet) = sheet.get(store, load_error)
        {
            slice_id = sheet.slice_id(slice_name).unwrap_or(0);
        }

        let prop = Prop
        {
            sheet,
            slice_id,
            dirty : true
        };

        prop
    }

    pub fn sheet(&self) -> &DataHandle<Sheet>
    {
        &self.sheet
    }
    pub fn set_sheet(&mut self, sheet : DataHandle<Sheet>)
    {
        self.sheet = sheet;
        self.dirty = true;
    }

    pub fn slice_id(&self) -> usize
    {
        self.slice_id
    }
    pub fn set_slice_id(&mut self, slice : usize)
    {
        self.slice_id = slice;
        self.dirty = true;
    }
    pub fn set_slice_name(&mut self, slice_name : &str, store : &mut DataStore<Sheet>)
    {
        if let Some(sheet) = self.sheet.get_if_resolved(store)
        {
            self.slice_id = sheet.slice_id(slice_name).unwrap_or(0);
            self.dirty = true;
        }
    }

    pub fn set_dirty(&mut self)
    {
        self.dirty = true;
    }

    pub fn update_sprite(&mut self, sprite : &mut Sprite)
    {
        sprite.sheet_id = self.sheet.id();
        sprite.slice_id = self.slice_id;
    }
}

pub struct PropThinker<T : SceneType + 'static>
{
    phantom : std::marker::PhantomData::<T>
}

impl<T : SceneType + 'static> PropThinker<T>
{
    pub fn new() -> PropThinker::<T>
    {
        PropThinker::<T>
        {
            phantom : std::marker::PhantomData
        }
    }
}

impl<T : SceneType + 'static> ThinkerSystem<T> for PropThinker<T>
{
    fn think(&mut self, args : ThinkerArgs<T>)
    {
        let mut prop_store = args.components.store_mut::<Prop>();
        let mut drawable_store = args.components.store_mut::<Drawable>();
        let mut transform_store = args.components.store_mut::<Transform>();

        for (entity, prop, freeze_flags) in prop_store.iter_filtered_mut(FreezeFlags::REASON_HIDDEN | FreezeFlags::REASON_CULLED)
        {
            if let Some(drawable) = drawable_store.get_entity_mut(&entity)
            {
                if let Some(transform) = transform_store.get_entity_mut(&entity)
                {
                    drawable.update_from_transform(transform);
                }

                if freeze_flags.is_empty()
                {
                    if prop.dirty
                    {
                        match &mut drawable.item
                        {
                            DrawableItem::Sprite(_) => (),
                            _ => drawable.item = DrawableItem::Sprite(Sprite::new())
                        }

                        match &mut drawable.item
                        {
                            DrawableItem::Sprite(sprite) =>
                            {
                                prop.update_sprite(sprite);
                            },
                            _ => ()
                        }

                        prop.dirty = false;
                    }
                }
            }
        }
    }

    fn priority(&self) -> i32 { crate::SYSTEM_PRIORITY_POST_FRAME }
}


#[derive(Component)]
pub struct Actor
{
    costume : DataHandle<Costume>,
    index : AnimationId,
    frame : usize,
    track : u16,
    track_fallbacks : Vec<u16>,
    timer : f64,
    speed : f64,
    next_index : AnimationId,
    next_frame: usize,
    finished : bool,
    offset_x : f32,
    offset_y : f32
}

impl Actor
{
    pub fn new() -> Actor
    {
        Actor
        {
            costume : DataHandle::default(),
            index : 0,
            frame : 0,
            track : 0,
            track_fallbacks : Vec::new(),
            timer : 0.0,
            speed : 1.0,
            next_index : 0,
            next_frame : 0,
            finished : false,
            offset_x : 0.0,
            offset_y : 0.0
        }
    }

    pub fn with_costume(costume : DataHandle<Costume>, store : &mut DataStore<Costume>, load_error : LoadErrorMode) -> Actor
    {
        let mut actor = Actor::new();

        costume.resolve(store, load_error);
        actor.costume = costume;

        actor
    }

    pub fn costume(&self) -> &DataHandle<Costume>
    {
        &self.costume
    }
    pub fn set_costume(&mut self, costume : DataHandle<Costume>)
    {
        self.costume = costume;
    }

    pub fn base_offset(&self) -> (f32, f32)
    {
        (self.offset_x, self.offset_y)
    }
    pub fn set_base_offset(&mut self, x : f32, y : f32)
    {
        self.offset_x = x;
        self.offset_y = y;
    }

    pub fn animation(&self) -> AnimationId
    {
        self.index
    }
    pub fn frame(&self) -> usize
    {
        self.frame
    }
    pub fn set_frame(&mut self, frame : usize, reset_timer : bool)
    {
        self.frame = frame;
        self.next_frame = frame;
        self.finished = false;

        if reset_timer
        {
            self.timer = 0.0;
        }
    }
    pub fn track(&self) -> u16
    {
        self.track
    }
    pub fn set_track(&mut self, track : u16, fallbacks : &[u16])
    {
        self.track = track;
        self.track_fallbacks.clear();
        self.track_fallbacks.extend_from_slice(fallbacks);
    }
    pub fn set_track_by_name(&mut self, track : &str, fallbacks : &[&str], store : &DataStore<Costume>) -> bool
    {
        if let Some(costume) = self.costume.get_if_resolved(store)
        {
            self.track = costume.track_names.get_id(track).unwrap_or(0) as u16;

            self.track_fallbacks.clear();

            for fallback in fallbacks
            {
                self.track_fallbacks.push(costume.track_names.get_id(fallback).unwrap_or(0) as u16);
            }

            return true;
        }

        false
    }
    pub fn is_playing(&self, id : AnimationId) -> bool
    {
        self.index == id
    }
    pub fn is_playing_name(&self, name : &str, store : &DataStore<Costume>) -> bool
    {
        if let Some(costume) = self.costume.get_if_resolved(store)
        {
            if let Some(id) = costume.animation_id(name)
            {
                return self.is_playing(id);
            }
        }

        false
    }
    pub fn is_finished(&self) -> bool
    {
        self.finished
    }
    pub fn is_animation_finished(&self, id : AnimationId) -> bool
    {
        self.finished && self.is_playing(id)
    }
    pub fn is_animation_name_finished(&self, name : &str, store : &DataStore<Costume>) -> bool
    {
        self.finished && self.is_playing_name(name, store)
    }

    /// Plays the animation with the given id, starting playback from the first frame.
    ///  If restart_if_same is false, the animation will not be restarted if it is
    ///  already playing. Otherwise, the animation is always restarted.
    ///  Returns true if the animation was changed.
    pub fn play_animation(&mut self, id : AnimationId, restart_if_same : bool) -> bool
    {
        if self.index != id || restart_if_same
        {
            self.next_index = id;
            self.index = id;

            self.next_frame = 0;
            self.frame = 0;

            self.timer = 0.0;
            self.finished = false;

            return true;
        }

        false
    }
    /// Plays the animation with the given name, starting playback from the first frame.
    ///  If restart_if_same is false, the animation will not be restarted if it is
    ///  already playing. Otherwise, the animation is always restarted.
    ///  Returns true if the animation was changed.
    pub fn play_animation_name(&mut self, name : &str, restart_if_same : bool, store : &DataStore<Costume>) -> bool
    {
        if let Some(costume) = self.costume.get_if_resolved(store)
        {
            if let Some(id) = costume.animation_id(name)
            {
                return self.play_animation(id, restart_if_same);
            }
        }

        false
    }

    /// Changes the currently playing animation without restarting playback. This can be useful in
    ///  situations where you want a smooth transition between two continuous animations. Normally
    ///  you'll probably want to use play_animation() instead.
    pub fn swap_animation(&mut self, id : AnimationId)
    {
        self.next_index = id;
        self.index = id;
        self.finished = false;
    }
    /// Changes the currently playing animation without restarting playback. This can be useful in
    ///  situations where you want a smooth transition between two continuous animations. Normally
    ///  you'll probably want to use swap_animation_name() instead.
    ///  Returns true if the animation was changed.
    pub fn swap_animation_name(&mut self, name : &str, store : &DataStore<Costume>) -> bool
    {
        if let Some(costume) = self.costume.get_if_resolved(store)
        {
            if let Some(id) = costume.animation_id(name)
            {
                self.swap_animation(id);
                return true;
            }
        }

        false
    }

    pub fn animation_speed(&self) -> f64
    {
        self.speed
    }
    pub fn set_animation_speed(&mut self, speed : f64)
    {
        self.speed = speed;
    }

    pub fn act(&mut self, store : &mut DataStore<Costume>)
    {
        if let Some(costume) = self.costume.get_mut_if_resolved(store)
        {
            self.index = self.next_index;
            self.frame = self.next_frame;

            if let Some(animation) = costume.animation(self.index)
            {
                if let Some(frame) = animation.frames.get(self.frame)
                {
                    self.timer += self.speed;

                    if self.timer >= frame.delay
                    {
                        self.timer -= frame.delay;
                        self.next_frame += 1;
                    }
                }

                if self.next_frame >= animation.frames.len()
                {
                    if animation.looping
                    {
                        self.next_frame = 0;
                    }
                    else
                    {
                        self.finished = true;
                    }
                }
            }
        }
    }

    pub fn update_sprite(&mut self, sprite : &mut Sprite,
                         costume_res : &mut DataStore<Costume>, sheet_res : &mut DataStore<Sheet>,
                         load_error : LoadErrorMode)
    {
        sprite.offset_x = self.offset_x;
        sprite.offset_y = self.offset_y;
        sprite.sheet_id = DataId::new(0);
        sprite.slice_id = 0;

        if let Some(costume) = self.costume.get_mut_if_resolved(costume_res)
        {
            if let Some(animation) = costume.animation_mut(self.animation())
            {
                let frame_max = animation.frames.len().max(1) - 1;
                let frame = self.frame().min(frame_max);
                let track = self.track;
                let mut track_row_opt = animation.costume_track_to_row(track);

                if track_row_opt.is_none()
                {
                    for fallback in &self.track_fallbacks
                    {
                        track_row_opt = animation.costume_track_to_row(*fallback);

                        if track_row_opt.is_some()
                        {
                            break;
                        }
                    }
                }

                if let Some(cel) = animation.cel_mut(frame, track_row_opt.unwrap_or(0))
                {
                    let sheet_id = cel.sheet.resolve(sheet_res, load_error);

                    if let Some(sheet) = sheet_res.get(sheet_id)
                    {
                        sprite.slice_id = cel.slice.updated_slice_id(sheet);
                        sprite.sheet_id = sheet_id;
                        sprite.offset_x += cel.x;
                        sprite.offset_y += cel.y;
                    }
                }
            }
        }
    }
}


pub struct ActorThinker<T : SceneType + 'static>
{
    load_error : LoadErrorMode,
    phantom : std::marker::PhantomData::<T>
}

impl<T : SceneType + 'static> ActorThinker<T>
{
    pub fn new(load_error : LoadErrorMode) -> ActorThinker::<T>
    {
        ActorThinker::<T>
        {
            load_error,
            phantom : std::marker::PhantomData
        }
    }
}

impl<T : SceneType + 'static> ThinkerSystem<T> for ActorThinker<T>
{
    fn think(&mut self, args : ThinkerArgs<T>)
    {
        let mut actor_store = args.components.store_mut::<Actor>();
        let mut drawable_store = args.components.store_mut::<Drawable>();
        let mut transform_store = args.components.store_mut::<Transform>();
        let mut sheet_res = args.game.res().store_mut::<Sheet>();
        let mut costume_res = args.game.res().store_mut::<Costume>();

        for (entity, actor, freeze_flags) in actor_store.iter_filtered_mut(FreezeFlags::REASON_HIDDEN | FreezeFlags::REASON_CULLED)
        {
            if let Some(drawable) = drawable_store.get_entity_mut(&entity)
            {
                if let Some(transform) = transform_store.get_entity_mut(&entity)
                {
                    drawable.update_from_transform(transform);
                }

                if freeze_flags.is_empty()
                {
                    match &mut drawable.item
                    {
                        DrawableItem::Sprite(_) => (),
                        _ => drawable.item = DrawableItem::Sprite(Sprite::new())
                    }

                    match &mut drawable.item
                    {
                        DrawableItem::Sprite(sprite) =>
                        {
                            if !actor.finished
                            {
                                actor.act(&mut costume_res);
                            }

                            actor.update_sprite(sprite, &mut costume_res, &mut sheet_res, self.load_error);
                        },
                        _ => ()
                    }
                }
            }
        }
    }

    fn priority(&self) -> i32 { crate::SYSTEM_PRIORITY_POST_FRAME }
}


#[derive(PartialEq, Clone, Debug)]
pub struct TextLayout
{
    pub x : Lerpable<f32>,
    pub y : Lerpable<f32>,
    sprites : Vec<TextSprite>,
    sheet_id : DataId<Sheet>
}

impl TextLayout
{
    pub fn new() ->TextLayout
    {
        TextLayout
        {
            x : Lerpable::new(0.0),
            y : Lerpable::new(0.0),
            sprites : Vec::new(),
            sheet_id : DataId::new(0)
        }
    }
}

#[derive(PartialEq, Clone, Debug)]
struct TextSprite
{
    slice_id : usize,
    x : f32,
    y : f32
}

pub struct TextCharInfo
{
    slice_id : usize,
    width : f32
}

#[derive(Component)]
pub struct Text
{
    text : String,
    sheet : DataHandle<Sheet>,
    generation : u64,
    alignment : f32,
    char_spacing : f32,
    char_line_height : f32,
    sprites_dirty : bool
}

impl Text
{
    pub fn new(text : &str, sheet : DataHandle<Sheet>, char_line_height : f32) -> Text
    {
        Text
        {
            text : text.to_string(),
            sheet,
            generation : 0,
            alignment : 0.0,
            char_spacing : 0.0,
            char_line_height,
            sprites_dirty : true
        }
    }

    pub fn text(&self) -> &str
    {
        &self.text
    }

    pub fn set_text(&mut self, text : &str)
    {
        self.text = text.to_string();
        self.sprites_dirty = true;
    }

    pub fn alignment(&self) -> f32
    {
        self.alignment
    }

    pub fn set_alignment(&mut self, alignment : f32)
    {
        self.alignment = alignment;
        self.sprites_dirty = true;
    }

    pub fn char_spacing(&self) -> f32
    {
        self.char_spacing
    }

    pub fn set_char_spacing(&mut self, char_spacing : f32)
    {
        self.char_spacing = char_spacing;
        self.sprites_dirty = true;
    }

    pub fn line_height(&self) -> f32
    {
        self.char_line_height
    }

    pub fn set_line_height(&mut self, line_height : f32)
    {
        self.char_line_height = line_height;
        self.sprites_dirty = true;
    }

    pub fn sheet(&self) -> &DataHandle<Sheet>
    {
        &self.sheet
    }
    pub fn set_sheet(&mut self, sheet : DataHandle<Sheet>)
    {
        self.sheet = sheet;
        self.sprites_dirty = true;
    }

    pub fn char_info(&self, letter : char, sheet_res : &DataStore<Sheet>) -> Option<TextCharInfo>
    {
        if let Some(sheet) = self.sheet.get_if_resolved(sheet_res)
        {
            let mut utf_scratch = [0; 4];

            if let Some(index) = sheet.slice_id(letter.encode_utf8(&mut utf_scratch))
            {
                if let Some(slice) = sheet.slice(index)
                {
                    return Some(TextCharInfo
                    {
                        slice_id: index,
                        width: slice.texture_w
                    });
                }
            }

            // Fallback character is slice 0
            if let Some(slice) = sheet.slice(0)
            {
                return Some(TextCharInfo
                {
                    slice_id: 0,
                    width: slice.texture_w
                });
            }
        }

        None
    }

    pub fn update_layout(&mut self, layout : &mut MultiSliceSprite, sheet_res : &mut DataStore<Sheet>, load_error : LoadErrorMode)
    {
        let mut sheet_gen = 0;

        if let Some(sheet) = self.sheet.get(sheet_res, load_error)
        {
            sheet_gen = sheet.generation();
        }

        if self.generation == sheet_gen && !self.sprites_dirty
        {
            return;
        }

        self.generation = sheet_gen;
        layout.slices.clear();

        let mut new_line = true;
        let mut x = 0.0;
        let mut y = 0.0;

        let letters : Vec<char> = self.text.chars().collect();

        for i in 0..letters.len()
        {
            let letter = &letters[i];

            if new_line && self.alignment != 0.0
            {
                let mut width = 0.0;

                for j in i..letters.len()
                {
                    let letter_center = &letters[j];

                    if *letter_center == '\n'
                    {
                        break;
                    }

                    if let Some(info) = self.char_info(*letter_center, sheet_res)
                    {
                        width += info.width;
                    }
                }

                x = width * -self.alignment;
            }

            new_line = false;

            if *letter == '\n'
            {
                x = 0.0;
                y += self.char_line_height;

                new_line = true;

                continue;
            }

            if let Some(info) = self.char_info(*letter, sheet_res)
            {
                layout.slices.push(PositionedSlice
                {
                    slice_id : info.slice_id,
                    x,
                    y,
                });

                x += info.width;
            }

            x += self.char_spacing;
        }

        layout.sheet_id = self.sheet.id();
        self.sprites_dirty = false;
    }
}

pub struct TextThinker<T : SceneType + 'static>
{
    load_error : LoadErrorMode,
    phantom : std::marker::PhantomData::<T>
}

impl<T : SceneType + 'static> TextThinker<T>
{
    pub fn new(load_error : LoadErrorMode) -> TextThinker::<T>
    {
        TextThinker::<T>
        {
            load_error,
            phantom : std::marker::PhantomData
        }
    }
}

impl<T : SceneType + 'static> ThinkerSystem<T> for TextThinker<T>
{
    fn think(&mut self, args : ThinkerArgs<T>)
    {
        let mut text_store = args.components.store_mut::<Text>();
        let mut drawable_store = args.components.store_mut::<Drawable>();
        let mut transform_store = args.components.store_mut::<Transform>();
        let mut sheet_res = args.game.res().store_mut::<Sheet>();

        for (entity, text, freeze_flags) in text_store.iter_filtered_mut(FreezeFlags::REASON_HIDDEN | FreezeFlags::REASON_CULLED)
        {
            if let Some(drawable) = drawable_store.get_entity_mut(&entity)
            {
                if let Some(transform) = transform_store.get_entity_mut(&entity)
                {
                    drawable.update_from_transform(transform);
                }

                if freeze_flags.is_empty()
                {
                    match &mut drawable.item
                    {
                        DrawableItem::MultiSliceSprite(_) => (),
                        _ => drawable.item = DrawableItem::MultiSliceSprite(MultiSliceSprite::new())
                    }

                    match &mut drawable.item
                    {
                        DrawableItem::MultiSliceSprite(layout) =>
                        {
                            text.update_layout(layout, &mut sheet_res, self.load_error);
                        },
                        _ => ()
                    }
                }
            }
        }
    }

    fn priority(&self) -> i32 { crate::SYSTEM_PRIORITY_POST_FRAME }
}

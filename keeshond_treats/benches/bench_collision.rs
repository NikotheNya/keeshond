#![feature(test)]

extern crate test;
extern crate keeshond_treats;
extern crate rand;

use keeshond::scene::Entity;
use keeshond::util::BakedRect;
use keeshond_treats::collision::broadstore::{CollisionBroadStore};
use keeshond_treats::collision::CollisionBroadItem;

use test::Bencher;
use rand::Rng;

const GRID_CEL_SIZE : u16 = 256;


struct CollisionBenchState
{
    items : Vec<(usize, CollisionBroadItem)>,
    broadstore : CollisionBroadStore
}


fn generate_small(entity : Entity) -> CollisionBroadItem
{
    let mut rng = rand::thread_rng();

    let x1 = rng.gen_range(0.0..10_000.0);
    let y1 = rng.gen_range(0.0..10_000.0);

    let x2 = x1 + rng.gen_range(16.0..64.0);
    let y2 = y1 + rng.gen_range(16.0..64.0);

    CollisionBroadItem
    {
        entity,
        positioned_bound : BakedRect
        {
            x1,
            y1,
            x2,
            y2
        }
    }
}

fn generate_medium(entity : Entity) -> CollisionBroadItem
{
    let mut rng = rand::thread_rng();

    let x1 = rng.gen_range(0.0..40_000.0);
    let y1 = rng.gen_range(0.0..40_000.0);

    let x2 = x1 + rng.gen_range(64.0..256.0);
    let y2 = y1 + rng.gen_range(64.0..256.0);

    CollisionBroadItem
    {
        entity,
        positioned_bound : BakedRect
        {
            x1,
            y1,
            x2,
            y2
        }
    }
}

fn generate_large(entity : Entity) -> CollisionBroadItem
{
    let mut rng = rand::thread_rng();

    let x1 = rng.gen_range(0.0..160_000.0);
    let y1 = rng.gen_range(0.0..160_000.0);

    let x2 = x1 + rng.gen_range(256.0..1024.0);
    let y2 = y1 + rng.gen_range(256.0..1024.0);

    CollisionBroadItem
    {
        entity,
        positioned_bound : BakedRect
        {
            x1,
            y1,
            x2,
            y2
        }
    }
}

fn generate_varied(entity : Entity) -> CollisionBroadItem
{
    let mut rng = rand::thread_rng();

    let x1 = rng.gen_range(0.0..40_000.0);
    let y1 = rng.gen_range(0.0..40_000.0);

    let x2 = x1 + rng.gen_range(16.0..2048.0);
    let y2 = y1 + rng.gen_range(16.0..2048.0);

    CollisionBroadItem
    {
        entity,
        positioned_bound : BakedRect
        {
            x1,
            y1,
            x2,
            y2
        }
    }
}

fn iter_bench_overlaps(bencher : &mut Bencher, state : &mut CollisionBenchState)
{
    bencher.iter(||
    {
        let mut overlaps = Vec::new();

        for (id, _) in &state.items
        {
            state.broadstore.query_overlapping(*id, &mut overlaps);
        }
    });
}

fn iter_bench_tiny_update(bencher : &mut Bencher, state : &mut CollisionBenchState)
{
    bencher.iter(||
    {
        for (id, item) in &state.items
        {
            let new_bound = BakedRect::<f64>
            {
                x1 : item.positioned_bound.x1 + 1.0,
                y1 : item.positioned_bound.y1 + 1.0,
                x2 : item.positioned_bound.x2 + 1.0,
                y2 : item.positioned_bound.y2 + 1.0
            };

            state.broadstore.queue_update(*id, new_bound);
            state.broadstore.update_dirty_bounds();
        }
    });
}

fn iter_bench_update(bencher : &mut Bencher, state : &mut CollisionBenchState)
{
    bencher.iter(||
    {
        for (id, item) in &state.items
        {
            let new_bound = BakedRect::<f64>
            {
                x1 : item.positioned_bound.x1 * 2.0,
                y1 : item.positioned_bound.y1 * 2.0,
                x2 : item.positioned_bound.x2 * 2.0,
                y2 : item.positioned_bound.y2 * 2.0
            };

            state.broadstore.queue_update(*id, new_bound);
            state.broadstore.update_dirty_bounds();
        }
    });
}

#[bench]
fn bench_build_many_small(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut broadstore = CollisionBroadStore::new(GRID_CEL_SIZE);

        for i in 0..10000
        {
            let new_item = generate_small(Entity::new(i, 1));
            broadstore.add(new_item);
        }
    });
}

fn build_many_small() -> CollisionBenchState
{
    let mut broadstore = CollisionBroadStore::new(GRID_CEL_SIZE);
    let mut items : Vec<(usize, CollisionBroadItem)> = Vec::new();

    for i in 0..10000
    {
        let new_item = generate_small(Entity::new(i, 1));
        let id = broadstore.add(new_item.clone());
        items.push((id, new_item));
    }

    CollisionBenchState
    {
        items,
        broadstore
    }
}

#[bench]
fn bench_update_many_small(bencher : &mut Bencher)
{
    let mut state = build_many_small();

    iter_bench_update(bencher, &mut state);
}

#[bench]
fn bench_tiny_update_many_small(bencher : &mut Bencher)
{
    let mut state = build_many_small();

    iter_bench_tiny_update(bencher, &mut state);
}

#[bench]
fn bench_overlap_many_small(bencher : &mut Bencher)
{
    let mut state = build_many_small();

    iter_bench_overlaps(bencher, &mut state);
}

#[bench]
fn bench_build_many_medium(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut broadstore = CollisionBroadStore::new(GRID_CEL_SIZE);

        for i in 0..10000
        {
            let new_item = generate_medium(Entity::new(i, 1));
            broadstore.add(new_item);
        }
    });
}

fn build_many_medium() -> CollisionBenchState
{
    let mut broadstore = CollisionBroadStore::new(GRID_CEL_SIZE);
    let mut items : Vec<(usize, CollisionBroadItem)> = Vec::new();

    for i in 0..10000
    {
        let new_item = generate_medium(Entity::new(i, 1));
        let id = broadstore.add(new_item.clone());
        items.push((id, new_item));
    }

    CollisionBenchState
    {
        items,
        broadstore
    }
}

#[bench]
fn bench_update_many_medium(bencher : &mut Bencher)
{
    let mut state = build_many_medium();

    iter_bench_update(bencher, &mut state);
}

#[bench]
fn bench_tiny_update_many_medium(bencher : &mut Bencher)
{
    let mut state = build_many_medium();

    iter_bench_tiny_update(bencher, &mut state);
}

#[bench]
fn bench_overlap_many_medium(bencher : &mut Bencher)
{
    let mut state = build_many_medium();

    iter_bench_overlaps(bencher, &mut state);
}

#[bench]
fn bench_build_many_large(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut broadstore = CollisionBroadStore::new(GRID_CEL_SIZE);

        for i in 0..10000
        {
            let new_item = generate_large(Entity::new(i, 1));
            broadstore.add(new_item);
        }
    });
}

fn build_many_large() -> CollisionBenchState
{
    let mut broadstore = CollisionBroadStore::new(GRID_CEL_SIZE);
    let mut items : Vec<(usize, CollisionBroadItem)> = Vec::new();

    for i in 0..10000
    {
        let new_item = generate_large(Entity::new(i, 1));
        let id = broadstore.add(new_item.clone());
        items.push((id, new_item));
    }

    CollisionBenchState
    {
        items,
        broadstore
    }
}

#[bench]
fn bench_update_many_large(bencher : &mut Bencher)
{
    let mut state = build_many_large();

    iter_bench_update(bencher, &mut state);
}

#[bench]
fn bench_tiny_update_many_large(bencher : &mut Bencher)
{
    let mut state = build_many_large();

    iter_bench_tiny_update(bencher, &mut state);
}

#[bench]
fn bench_overlap_many_large(bencher : &mut Bencher)
{
    let mut state = build_many_large();

    iter_bench_overlaps(bencher, &mut state);
}

#[bench]
fn bench_build_many_varied(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut broadstore = CollisionBroadStore::new(GRID_CEL_SIZE);

        for i in 0..5000
        {
            let new_item = generate_varied(Entity::new(i, 1));
            broadstore.add(new_item);
        }
    });
}

fn build_many_varied() -> CollisionBenchState
{
    let mut broadstore = CollisionBroadStore::new(GRID_CEL_SIZE);
    let mut items : Vec<(usize, CollisionBroadItem)> = Vec::new();

    for i in 0..5000
    {
        let new_item = generate_varied(Entity::new(i, 1));
        let id = broadstore.add(new_item.clone());
        items.push((id, new_item));
    }

    CollisionBenchState
    {
        items,
        broadstore
    }
}

#[bench]
fn bench_update_many_varied(bencher : &mut Bencher)
{
    let mut state = build_many_varied();

    iter_bench_update(bencher, &mut state);
}

#[bench]
fn bench_tiny_update_many_varied(bencher : &mut Bencher)
{
    let mut state = build_many_varied();

    iter_bench_tiny_update(bencher, &mut state);
}

#[bench]
fn bench_overlap_many_varied(bencher : &mut Bencher)
{
    let mut state = build_many_varied();

    iter_bench_overlaps(bencher, &mut state);
}

use proc_macro2::{Span, TokenStream};
use proc_macro_error::{abort, proc_macro_error};
use quote::quote;
use syn::{Attribute, DeriveInput, GenericParam, Lifetime, LifetimeDef, Lit, Meta, MetaNameValue, NestedMeta, parse_macro_input};

const AVAILABLE_COMPONENT_ATTRIBUTE_NAMES : [&str; 1] = ["removal"];
const AVAILABLE_COMPONENT_REMOVAL_VALUES : [&str; 2] = ["ordered", "fast"];

fn parse_component_removal_attribute(name_value : &MetaNameValue) -> Option<TokenStream>
{
    match &name_value.lit
    {
        Lit::Str(value_string) => match value_string.value().as_ref()
        {
            "ordered" => return Some(quote!
            {
                fn maintain_ordering() -> bool where Self : Sized { true }
            }),
            "fast" => return Some(quote!
            {
                fn maintain_ordering() -> bool where Self : Sized { false }
            }),
            _ => abort!
            {
                name_value,
                format!("Unknown removal value \"{}\"", value_string.value());
                help = format!("Available values for `removal`: {:?}", AVAILABLE_COMPONENT_REMOVAL_VALUES)
            }
        }
        _ => abort!
        {
            name_value,
            "Expected string, got invalid value type";
            help = format!("Available values for `removal`: {:?}", AVAILABLE_COMPONENT_REMOVAL_VALUES)
        }
    }
}

fn parse_component_attribute(name_value : &MetaNameValue) -> Option<TokenStream>
{
    if let Some(ident) = name_value.path.get_ident()
    {
        match ident.to_string().as_ref()
        {
            "removal" =>
            {
                return parse_component_removal_attribute(name_value);
            }
            _ => abort!
            {
                ident,
                format!("Unknown component attribute name \"{}\"", ident);
                help = format!("Available attribute names: {:?}", AVAILABLE_COMPONENT_ATTRIBUTE_NAMES)
            }
        }
    }

    None
}

fn parse_component_attribute_list(attr : &Attribute) -> Option<TokenStream>
{
    if let Ok(Meta::List(list)) = attr.parse_meta()
    {
        if list.path.is_ident("component")
        {
            for nested in &list.nested
            {
                if let NestedMeta::Meta(Meta::NameValue(name_value)) = nested
                {
                    return parse_component_attribute(name_value);
                }
            }
        }
    }

    None
}

#[proc_macro_derive(Component, attributes(component))]
#[proc_macro_error]
pub fn derive_component(input : proc_macro::TokenStream) -> proc_macro::TokenStream
{
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;

    let mut ref_generics = input.generics.clone();
    let (impl_generics, type_generics, where_clause) = input.generics.split_for_impl();

    ref_generics.params.push(GenericParam::Lifetime(LifetimeDef
    {
        attrs : vec![],
        lifetime : Lifetime::new("'component", Span::call_site()),
        colon_token : None,
        bounds : Default::default()
    }));

    let mut body = TokenStream::new();

    let (impl_ref_generics, _, _) = ref_generics.split_for_impl();

    for attr in &input.attrs
    {
        if let Some(tokens) = parse_component_attribute_list(attr)
        {
            body.extend(tokens);
        }
    }

    let expanded = quote!
    {
        impl #impl_generics keeshond::scene::Component for #name #type_generics #where_clause
        {
            #body
        }

        impl #impl_ref_generics keeshond::scene::ComponentRef<'component> for #name #type_generics #where_clause
        {
            type StoreRef = &'component #name #type_generics;

            fn store_get(&'component self) -> Self::StoreRef
            {
                self
            }
        }

        impl #impl_ref_generics keeshond::scene::ComponentRefMut<'component> for #name #type_generics #where_clause
        {
            type StoreRefMut = &'component mut #name #type_generics;

            fn store_get_mut(&'component mut self) -> Self::StoreRefMut
            {
                self
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}

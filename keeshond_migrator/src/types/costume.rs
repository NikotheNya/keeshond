use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::Error;
use toodee::{TooDee, TooDeeOps};
use keeshond::renderer::Sheet;
use keeshond::util::NamedItemStore;
use keeshond::datapack::DataHandle;
use crate::migrator::{Migratable, MigrationError};
use crate::util::ExactStructDefaultIter;

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct SliceHandle
{
    #[serde(rename = "sli")]
    #[serde(alias = "slice")]
    slice : String,
    #[serde(skip)]
    #[allow(dead_code)]
    slice_id : usize,
    #[serde(skip)]
    #[allow(dead_code)]
    generation : u64
}


#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct AnimationCelV1
{
    #[serde(rename = "she")]
    #[serde(alias = "sheet")]
    pub sheet : DataHandle<Sheet>,
    #[serde(flatten)]
    pub slice : SliceHandle,
    #[serde(rename = "dly")]
    #[serde(alias = "delay")]
    pub delay : f64,
    pub x : f32,
    pub y : f32
}


#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct AnimationV1
{
    cels : Vec<AnimationCelV1>,
    #[serde(rename = "loop")]
    #[serde(alias = "looping")]
    looping : bool
}


#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct CostumeV1
{
    #[serde(rename = "anim")]
    #[serde(alias = "animations")]
    animations : NamedItemStore<AnimationV1>
}

impl Migratable for CostumeV1
{
    fn id() -> &'static str where Self : Sized
    {
        "costume_v1"
    }

    fn describe() -> &'static str where Self : Sized
    {
        "Costume v1"
    }

    fn describe_instance(&self) -> &'static str
    {
        "Costume v1"
    }

    fn read_file(path : &str) -> Result<Self, String> where Self : Sized
    {
        let mut reader = match File::open(path)
        {
            Ok(file) => { BufReader::new(file) }
            Err(error) => { return Err(error.to_string()); }
        };

        let mut json_text = String::new();
        if let Err(error) = reader.read_to_string(&mut json_text)
        {
            return Err(error.to_string());
        }

        match serde_json::from_str(&json_text)
        {
            Ok(loaded_data) =>
            {
                return Ok(loaded_data);
            },
            Err(error) =>
            {
                return Err(error.to_string());
            }
        }
    }

    fn migrate(&self) -> Result<Box<dyn Migratable>, MigrationError>
    {
        let mut costume2 = CostumeV2
        {
            animations : NamedItemStore::new(),
            track_names : NamedItemStore::new()
        };

        costume2.track_names.insert_item(0, ());
        costume2.track_names.rename_item(0, "default");

        for pos in 0..self.animations.len()
        {
            if let Some(old_anim) = self.animations.get(pos)
            {
                let mut new_anim = AnimationV2::new();

                new_anim.add_track(AnimationTrack
                {
                    track_id : 0
                });

                for frame_pos in 0..old_anim.cels.len()
                {
                    let old_cel = &old_anim.cels[frame_pos];

                    new_anim.add_frame(AnimationFrame
                    {
                        delay : old_cel.delay
                    });

                    new_anim.cels[(frame_pos, 0)] = AnimationCelV2
                    {
                        sheet : old_cel.sheet.clone(),
                        slice : old_cel.slice.clone(),
                        x : old_cel.x,
                        y : old_cel.y
                    };
                }

                new_anim.looping = old_anim.looping;

                costume2.animations.insert_item(pos, new_anim);

                if let Some(name) = self.animations.get_name(pos)
                {
                    costume2.animations.rename_item(pos, &name);
                }
            }
        }

        Ok(Box::new(costume2))
    }

    fn write_file(&self, path : &str) -> Result<(), String>
    {
        let mut writer = match File::create(path)
        {
            Ok(file) => { BufWriter::new(file) }
            Err(error) => { return Err(error.to_string()); }
        };

        match serde_json::to_string(&self)
        {
            Ok(json_text) =>
            {
                if let Err(error) = writer.write_all(json_text.as_bytes())
                {
                    return Err(error.to_string());
                }
            }
            Err(error) =>
            {
                return Err(error.to_string());
            }
        }

        Ok(())
    }
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct AnimationCelV2
{
    #[serde(rename = "she")]
    #[serde(alias = "sheet")]
    pub sheet : DataHandle<Sheet>,
    #[serde(flatten)]
    pub slice : SliceHandle,
    pub x : f32,
    pub y : f32
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct AnimationFrame
{
    #[serde(rename = "dly")]
    #[serde(alias = "delay")]
    pub delay : f64
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct AnimationTrack
{
    #[serde(rename = "id")]
    #[serde(alias = "track_id")]
    pub track_id : u16
}

#[derive(Clone, Debug)]
pub struct AnimationV2
{
    cels : TooDee<AnimationCelV2>,
    frames : Vec<AnimationFrame>,
    tracks : Vec<AnimationTrack>,
    track_id_to_row : Vec<Option<usize>>,
    looping : bool
}

impl AnimationV2
{
    pub fn new() -> AnimationV2
    {
        AnimationV2
        {
            cels : TooDee::new(0, 0),
            frames : Vec::new(),
            tracks : Vec::new(),
            track_id_to_row : Vec::new(),
            looping : false
        }
    }

    pub fn add_frame(&mut self, frame : AnimationFrame)
    {
        self.insert_frame(self.frames.len(), frame);
    }

    pub fn insert_frame(&mut self, pos : usize, frame : AnimationFrame) -> bool
    {
        if pos > self.frames.len()
        {
            return false;
        }

        self.frames.insert(pos, frame);

        let num_rows = self.cels.num_rows();
        self.cels.insert_col(pos, ExactStructDefaultIter::new(num_rows));

        true
    }

    pub fn add_track(&mut self, track : AnimationTrack)
    {
        self.insert_track(self.tracks.len(), track);
    }

    pub fn insert_track(&mut self, row : usize, track : AnimationTrack) -> bool
    {
        if row > self.tracks.len()
        {
            return false;
        }

        self.tracks.insert(row, track);

        let num_cols = self.cels.num_cols();
        self.cels.insert_row(row, ExactStructDefaultIter::new(num_cols));

        self.update_track_id_to_row();

        true
    }

    fn update_track_id_to_row(&mut self)
    {
        self.track_id_to_row.clear();

        for row in 0..self.tracks.len()
        {
            let track_id = self.tracks[row].track_id as usize;

            while self.track_id_to_row.len() <= track_id
            {
                self.track_id_to_row.push(None);
            }

            self.track_id_to_row[track_id] = Some(row);
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct AnimationRawV2
{
    cels : Vec<AnimationCelV2>,
    #[serde(rename = "fra")]
    #[serde(alias = "frames")]
    frames : Vec<AnimationFrame>,
    #[serde(rename = "tra")]
    #[serde(alias = "tracks")]
    tracks : Vec<AnimationTrack>,
    #[serde(rename = "loop")]
    #[serde(alias = "looping")]
    looping : bool
}

impl Serialize for AnimationV2
{
    fn serialize<S>(&self, serializer : S) -> Result<S::Ok, S::Error> where S : Serializer
    {
        let anim_raw = AnimationRawV2
        {
            cels : self.cels.data().to_vec(),
            frames : self.frames.clone(),
            tracks : self.tracks.clone(),
            looping : self.looping
        };

        anim_raw.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for AnimationV2
{
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
        where D : Deserializer<'de>
    {
        let mut anim_raw = AnimationRawV2::deserialize(deserializer)?;

        if anim_raw.frames.is_empty() || anim_raw.tracks.is_empty()
        {
            if !anim_raw.cels.is_empty()
            {
                eprintln!("Animation cels found with invalid frames/tracks dimensions, cels will be removed.");
            }

            anim_raw.frames.clear();
            anim_raw.tracks.clear();
            anim_raw.cels.clear();
        }

        if anim_raw.cels.len() != anim_raw.frames.len() * anim_raw.tracks.len()
        {
            return Err(D::Error::custom(format!("Data length {} doesn't match frame count {} and track count {}",
                                                anim_raw.cels.len(), anim_raw.frames.len(), anim_raw.tracks.len())));
        }

        let mut anim = AnimationV2
        {
            cels : TooDee::from_vec(anim_raw.frames.len(), anim_raw.tracks.len(), anim_raw.cels),
            frames : anim_raw.frames,
            tracks : anim_raw.tracks,
            track_id_to_row : Vec::new(),
            looping : anim_raw.looping
        };

        anim.update_track_id_to_row();

        Ok(anim)
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct CostumeV2
{
    #[serde(rename = "anim")]
    #[serde(alias = "animations")]
    animations : NamedItemStore<AnimationV2>,
    #[serde(rename = "trna")]
    #[serde(alias = "track_names")]
    track_names : NamedItemStore<()>
}

impl Migratable for CostumeV2
{
    fn id() -> &'static str where Self : Sized
    {
        "costume_v2"
    }

    fn describe() -> &'static str where Self : Sized
    {
        "Costume v2"
    }

    fn describe_instance(&self) -> &'static str
    {
        "Costume v2"
    }

    fn read_file(path : &str) -> Result<Self, String> where Self : Sized
    {
        let mut reader = match File::open(path)
        {
            Ok(file) => { BufReader::new(file) }
            Err(error) => { return Err(error.to_string()); }
        };

        let mut json_text = String::new();
        if let Err(error) = reader.read_to_string(&mut json_text)
        {
            return Err(error.to_string());
        }

        match serde_json::from_str(&json_text)
        {
            Ok(loaded_data) =>
            {
                return Ok(loaded_data);
            },
            Err(error) =>
            {
                return Err(error.to_string());
            }
        }
    }

    fn migrate(&self) -> Result<Box<dyn Migratable>, MigrationError>
    {
        Err(MigrationError::AlreadyLatest)
    }

    fn write_file(&self, path : &str) -> Result<(), String>
    {
        let mut writer = match File::create(path)
        {
            Ok(file) => { BufWriter::new(file) }
            Err(error) => { return Err(error.to_string()); }
        };

        match serde_json::to_string(&self)
        {
            Ok(json_text) =>
            {
                if let Err(error) = writer.write_all(json_text.as_bytes())
                {
                    return Err(error.to_string());
                }
            }
            Err(error) =>
            {
                return Err(error.to_string());
            }
        }

        Ok(())
    }
}

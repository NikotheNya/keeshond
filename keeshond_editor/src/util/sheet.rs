use image::RgbaImage;
use toodee::TooDeeOps;
use keeshond::renderer::{Sheet, SpriteSlice};
use keeshond::util::BakedRect;

pub fn slice_to_uv(sheet : &Sheet, slice : &SpriteSlice) -> BakedRect<f32>
{
    let base_width = sheet.width() as f32;
    let base_height = sheet.height() as f32;

    BakedRect::<f32>
    {
        x1 : slice.texture_x / base_width,
        y1 : slice.texture_y / base_height,
        x2 : (slice.texture_x + slice.texture_w) / base_width,
        y2 : (slice.texture_y + slice.texture_h) / base_height
    }
}

#[derive(Debug)]
struct FloodFillInfo
{
    min_x : usize,
    max_x : usize,
    y : usize,
    extend_left : bool,
    extend_right : bool
}

// Modified from http://will.thimbleby.net/scanline-flood-fill/
pub fn flood_fill<A, T, S>(target : &mut toodee::TooDee<A>, x : usize, y : usize, diagonal : bool, mut test_pixel : T, mut set_pixel : S)
    where T : FnMut(&mut toodee::TooDee<A>, usize, usize) -> bool, S : FnMut(&mut toodee::TooDee<A>, usize, usize)
{
    let width = target.num_cols();
    let height = target.num_rows();
    let mut ranges = Vec::new();

    if !test_pixel(target, x, y)
    {
        return;
    }

    ranges.push(FloodFillInfo
    {
        min_x : x,
        max_x : x,
        y,
        extend_left : true,
        extend_right : true
    });

    while !ranges.is_empty()
    {
        let mut range = ranges.pop().unwrap();

        range.min_x = usize::min(usize::max(0, range.min_x), width - 1);
        range.max_x = usize::max(usize::min(usize::max(0, range.max_x), width - 1), range.min_x);

        for range_x in range.min_x..range.max_x + 1
        {
            set_pixel(target, range_x, range.y);
        }

        let mut extended_min_x = range.min_x;

        if range.extend_left
        {
            while extended_min_x > 0 && test_pixel(target, extended_min_x - 1, range.y)
            {
                extended_min_x -= 1;
                set_pixel(target, extended_min_x, range.y);
            }
        }

        let mut extended_max_x = range.max_x;

        if range.extend_right
        {
            while extended_max_x < width - 1 && test_pixel(target, extended_max_x + 1, range.y)
            {
                extended_max_x += 1;
                set_pixel(target, extended_max_x, range.y);
            }
        }

        if diagonal
        {
            if extended_min_x > 0 { extended_min_x -= 1; }
            if extended_max_x < width - 1 { extended_max_x += 1; }
        }

        let mut add_range = |new_y|
            {
                let mut range_active = false;
                let mut range_count : usize = 0;
                let mut range_start : usize = 0;

                for x in extended_min_x..extended_max_x + 1
                {
                    if test_pixel(target, x, new_y)
                    {
                        if !range_active
                        {
                            range_start = x;
                            range_active = true;
                        }
                    }
                    else
                    {
                        if range_active
                        {
                            ranges.push(FloodFillInfo
                            {
                                min_x : range_start,
                                max_x : x - 1,
                                y : new_y,
                                extend_left : range_count == 0,
                                extend_right : false
                            });

                            range_active = false;
                            range_count += 1;
                        }
                    }
                }

                if range_active
                {
                    ranges.push(FloodFillInfo
                    {
                        min_x : range_start,
                        max_x : extended_max_x,
                        y : new_y,
                        extend_left : range_count == 0,
                        extend_right : false
                    });

                    range_count += 1;
                }

                if range_count > 0
                {
                    if let Some(range) = ranges.last_mut()
                    {
                        range.extend_right = true;
                    }
                }
            };

        if range.y > 0
        {
            add_range(range.y - 1);
        }
        if range.y < height - 1
        {
            add_range(range.y + 1);
        }
    }
}

struct TransparentPixel
{
    x : u32, y : u32, set : bool
}

pub fn defringe_image(texture : &mut RgbaImage, wrap : bool)
{
    let mut trans_pixels = Vec::new();
    let mut processed_trans = vec![false; (texture.width() * texture.height()) as usize];
    let mut processed_trans_buffer = vec![false; (texture.width() * texture.height()) as usize];

    for y in 0..texture.height()
    {
        for x in 0..texture.width()
        {
            let pixel = texture.get_pixel_mut(x, y);

            if pixel[3] == 0
            {
                trans_pixels.push(TransparentPixel
                {
                    x,
                    y,
                    set: false
                })
            }
        }
    }

    let mut colors = Vec::new();

    while trans_pixels.len() > 0
    {
        let mut processed = false;

        for entry in &mut trans_pixels
        {
            colors.clear();
            let (x, y) = (entry.x, entry.y);

            for (offset_x, offset_y) in &[(0, -1), (-1, 0), (1, 0), (0, 1)]
            {
                let check_x = x as i32 + offset_x;
                let check_y = y as i32 + offset_y;

                if wrap || (check_x >= 0 && check_x < texture.width() as i32
                    && check_y >= 0 && check_y < texture.height() as i32)
                {
                    let check_x_wrapped = check_x as u32 % texture.width();
                    let check_y_wrapped = check_y as u32 % texture.height();
                    let other_pixel = texture.get_pixel(check_x_wrapped, check_y_wrapped);

                    if other_pixel[3] > 0 || processed_trans[(check_y_wrapped * texture.width() + check_x_wrapped) as usize]
                    {
                        colors.push((other_pixel[0], other_pixel[1], other_pixel[2]));
                    }
                }
            }

            if colors.len() > 0
            {
                let mut red = 0.0;
                let mut green = 0.0;
                let mut blue = 0.0;

                for (other_r, other_g, other_b) in &colors
                {
                    red += *other_r as f32;
                    green += *other_g as f32;
                    blue += *other_b as f32;
                }

                red /= colors.len() as f32;
                green /= colors.len() as f32;
                blue /= colors.len() as f32;

                let pixel = texture.get_pixel_mut(x, y);

                pixel[0] = red as u8;
                pixel[1] = green as u8;
                pixel[2] = blue as u8;

                processed = true;
                entry.set = true;
                processed_trans_buffer[(y * texture.width() + x) as usize] = true;
            }
        }

        trans_pixels.retain(|entry| entry.set == false);
        processed_trans.copy_from_slice(&processed_trans_buffer[..]);

        // Don't get stuck if there are no opaque pixels
        if !processed
        {
            break;
        }
    }
}

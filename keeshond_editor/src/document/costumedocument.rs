
use keeshond::gameloop::GameControl;
use keeshond::renderer::Sheet;

use keeshond::datapack::{DataId, DataStore, DataHandle};
use keeshond_treats::visual::{Costume, Animation, AnimationCel, SliceHandle, AnimationFrame, AnimationTrack};

use crate::document::{Document, DocumentState, ActionHistory};
use crate::editorgui::{ResourceType, Packages, EditorConfig, ProjectConfig};
use crate::util::zoom::{EDITOR_ZOOM_LEVELS, ZoomControl};

use std::any::Any;

const CEL_BUTTON_SIZE : f32 = 80.0;

enum CostumeAction
{
    AddAnimation { index : usize, animation : Animation },
    RenameAnimation { index : usize, name : String },
    SetAnimation { index : usize, animation : Animation },
    MoveAnimation { old_index : usize, new_index : usize },
    RemoveAnimation { index : usize },
    AddTrack { index : usize, name : String },
    RenameTrack { index : usize, name : String },
    RemoveTrack { index : usize },
    SetCel { anim_index : usize, frame_index : usize, track_row : usize, cel : AnimationCel },
    MoveCel { anim_index : usize, old_frame_index : usize, new_frame_index : usize, track_row : usize },
    AddFrame { anim_index : usize, frame_index : usize, frame : AnimationFrame },
    SetFrame { anim_index : usize, frame_index : usize, frame : AnimationFrame },
    MoveFrame { anim_index : usize, old_frame_index : usize, new_frame_index : usize },
    RemoveFrame { anim_index : usize, frame_index : usize },
    AddAnimationTrack { anim_index : usize, track_row : usize, track : AnimationTrack },
    MoveAnimationTrack { anim_index : usize, old_track_row : usize, new_track_row : usize },
    RemoveAnimationTrack { anim_index : usize, track_row : usize }
}

pub struct CostumeDocument
{
    action_history : ActionHistory,
    data_id : DataId<Costume>,
    show_origin : bool,
    transparent_color : [f32; 3],
    zoom_control : ZoomControl<f32>,
    dpi_factor : f32,
    current_animation : usize,
    current_frame : usize,
    current_track_row : usize,
    play_animation : bool,
    animation_timer : f64,
    tics : u64,
    focus_animation_name_input : bool,
    scroll_anim_list : bool,
    scroll_cel_list : bool
}

impl CostumeDocument
{
    fn adjusted_zoom(&self) -> f32
    {
        self.zoom_control.zoom() / self.dpi_factor
    }

    fn scaled_round(&self, value : f32) -> f32
    {
        crate::util::scaled_round(value, 1.0 / self.dpi_factor)
    }

    fn add_animation(&mut self, index : usize, costume : &mut Costume, animation : Animation)
    {
        let mut target_animation = 0;

        if costume.animation_count() > 0
        {
            target_animation = index + 1;
        }

        costume.insert_animation(target_animation, animation);

        self.current_animation += 1;
    }

    fn move_animation(&mut self, costume : &mut Costume, old : usize, new : usize) -> bool
    {
        if new >= costume.animation_count()
        {
            return false;
        }

        if let Some(animation) = costume.animation(old).cloned()
        {
            let old_name = costume.animation_name(old);

            costume.remove_animation(old);
            costume.insert_animation(new, animation);

            if let Some(name) = old_name
            {
                costume.rename_animation(new, &name);
            }

            self.current_animation = new;

            return true;
        }

        false
    }

    fn add_frame(&mut self, anim_index : usize, frame_index : usize, costume : &mut Costume, frame : AnimationFrame) -> bool
    {
        if let Some(mut animation) = costume.animation(anim_index).cloned()
        {
            let mut target_frame = 0;

            if animation.frame_count() > 0
            {
                target_frame = frame_index + 1;
            }

            if animation.insert_frame(target_frame, frame)
            {
                self.current_frame += 1;

                return costume.set_animation(anim_index, animation);
            }
        }

        false
    }

    fn add_animation_track(&mut self, anim_index : usize, track_row : usize, costume : &mut Costume, track : AnimationTrack) -> bool
    {
        if let Some(mut animation) = costume.animation(anim_index).cloned()
        {
            let mut target_track = 0;

            if animation.track_count() > 0
            {
                target_track = track_row + 1;
            }

            if animation.insert_track(target_track, track)
            {
                self.current_track_row += 1;

                return costume.set_animation(anim_index, animation);
            }
        }

        false
    }

    fn draw_crosshair(&mut self, draw_list : &imgui::DrawListMut, x : f32, y : f32)
    {
        let pulse = (((self.tics as f64 * 0.1).sin() + 1.0) / 2.0) as f32;

        let cross_size = 4.1 + self.adjusted_zoom().min(8.0);
        let cross_color = [pulse, pulse, pulse, 1.0];

        draw_list.add_line([x - cross_size, y], [x + cross_size, y], imgui::ImColor32::from(cross_color)).thickness(1.0 / self.dpi_factor).build();
        draw_list.add_line([x, y - cross_size], [x, y + cross_size], imgui::ImColor32::from(cross_color)).thickness(1.0 / self.dpi_factor).build();
    }

    fn animation_view(&mut self, ui : &imgui::Ui, costume : &mut Costume)
    {
        let main_style = ui.clone_style();

        self.current_animation = self.current_animation.min(costume.animation_count().max(1) - 1);

        if let Some(animation) = costume.animation(self.current_animation)
        {
            if let Some(cel) = animation.cel(self.current_frame, self.current_track_row)
            {
                let mut update_cel = false;

                ui.text(format!("Editing Cel {}, {}", self.current_frame, self.current_track_row));
                ui.spacing();

                let mut package_name = cel.sheet.package().clone();
                let mut sheet_name = cel.sheet.path().clone();
                let mut slice_name = cel.slice.slice().clone();

                let mut offset = [cel.x, cel.y];

                let style_var = ui.push_style_var(imgui::StyleVar::ItemSpacing([-1.0, -1.0]));

                update_cel |= ui.input_text("Package", &mut package_name).enter_returns_true(true).build();
                update_cel |= ui.input_text("Sheet", &mut sheet_name).enter_returns_true(true).build();

                style_var.pop();

                update_cel |= ui.input_text("Slice", &mut slice_name).enter_returns_true(true).build();
                update_cel |= imgui::Drag::new("Offset").display_format("%.4f").build_array(ui, &mut offset);

                ui.spacing();

                if update_cel
                {
                    let new_cel = AnimationCel
                    {
                        sheet : DataHandle::with_path(&package_name, &sheet_name),
                        slice : SliceHandle::new(&slice_name),
                        x : offset[0],
                        y : offset[1]
                    };

                    self.queue_action(Box::new(CostumeAction::SetCel
                    {
                        anim_index : self.current_animation,
                        frame_index : self.current_frame,
                        track_row : self.current_track_row,
                        cel : new_cel
                    }));
                }
            }

            if let Some(frame) = animation.frame(self.current_frame)
            {
                ui.text(format!("Editing Frame {}", self.current_frame));
                ui.spacing();

                let mut delay = frame.delay;

                if imgui::Drag::new("Delay").display_format("%.4f").build(ui, &mut delay)
                {
                    let new_frame = AnimationFrame
                    {
                        delay
                    };

                    self.queue_action(Box::new(CostumeAction::SetFrame
                    {
                        anim_index : self.current_animation,
                        frame_index : self.current_frame,
                        frame : new_frame
                    }));
                }

                ui.spacing();
            }

            ui.text(format!("Editing Animation {}", self.current_animation));
            ui.spacing();

            let mut update_animation = false;
            let mut animation_name = costume.animation_name(self.current_animation).unwrap_or_default();

            if self.focus_animation_name_input
            {
                ui.set_keyboard_focus_here_with_offset(imgui::FocusedWidget::Offset(0));
                self.focus_animation_name_input = false;
            }

            if ui.input_text("Name", &mut animation_name).enter_returns_true(true).build()
            {
                self.queue_action(Box::new(CostumeAction::RenameAnimation
                {
                    index : self.current_animation,
                    name : animation_name.to_string()
                }));
            }

            let mut looping = animation.looping();

            update_animation |= ui.checkbox("Looping", &mut looping);

            if update_animation
            {
                let mut animation_clone = animation.clone();
                animation_clone.set_looping(looping);

                self.queue_action(Box::new(CostumeAction::SetAnimation
                {
                    index : self.current_animation,
                    animation : animation_clone
                }));
            }
        }

        ui.spacing();

        if ui.button("Add")
        {
            self.queue_action(Box::new(CostumeAction::AddAnimation
            {
                index : self.current_animation,
                animation : Animation::new()
            }));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Up") && self.current_animation > 0
        {
            self.queue_action(Box::new(CostumeAction::MoveAnimation
            {
                old_index : self.current_animation,
                new_index : self.current_animation - 1
            }));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Down")
        {
            self.queue_action(Box::new(CostumeAction::MoveAnimation
            {
                old_index : self.current_animation,
                new_index : self.current_animation + 1
            }));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove")
        {
            self.queue_action(Box::new(CostumeAction::RemoveAnimation
            {
                index : self.current_animation
            }));
        }

        let style_animation_list = ui.push_style_var(imgui::StyleVar::WindowPadding([3.0, 2.0]));
        let colors_animation_list = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);

        imgui::ChildWindow::new("animation_list").border(true).draw_background(true).build(ui, ||
        {
            let num_animations = costume.animation_count();

            for i in 0..num_animations
            {
                let animation_name;

                if let Some(name) = costume.animation_name(i)
                {
                    animation_name = name;
                }
                else
                {
                    animation_name = format!("<animation {}>", i);
                }

                if imgui::Selectable::new(&animation_name).selected(self.current_animation == i).build(ui)
                {
                    self.current_animation = i;
                    self.current_frame = 0;
                    self.current_track_row = 0;
                    self.animation_timer = 0.0;
                }

                if self.current_animation == i && self.scroll_anim_list
                {
                    ui.set_scroll_here_y();
                    self.scroll_anim_list = false;
                }
            }
        });

        colors_animation_list.pop();
        style_animation_list.pop();
    }

    fn track_name_view(&mut self, ui : &imgui::Ui, costume : &mut Costume)
    {
        for i in 0..costume.track_count()
        {
            let mut name = costume.track_name(i).unwrap_or_default();

            if ui.input_text(format!("Track {}", i), &mut name).enter_returns_true(true).build()
            {
                self.queue_action(Box::new(CostumeAction::RenameTrack
                {
                    index : i,
                    name
                }));
            }
        }

        if ui.button("Add")
        {
            self.queue_action(Box::new(CostumeAction::AddTrack
            {
                index : costume.track_count(),
                name : String::new()
            }));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove")
        {
            self.queue_action(Box::new(CostumeAction::RemoveTrack
            {
                index : costume.track_count().saturating_sub(1)
            }));
        }
    }

    fn set_cel_menu(&mut self, packages : &mut Packages, ui : &imgui::Ui, sheet_store : &mut DataStore<Sheet>, animation : &Animation)
    {
        let mut picked_cel = false;

        ui.popup("Set Cel Menu", ||
        {
            if ui.is_mouse_clicked(imgui::MouseButton::Middle)
            {
                self.current_frame = (self.current_frame + 1) % animation.frame_count();
            }

            if imgui::Selectable::new("Empty Cel").close_popups(false).build(ui)
            {
                self.queue_action(Box::new(CostumeAction::SetCel
                {
                    anim_index : self.current_animation,
                    frame_index : self.current_frame,
                    track_row : self.current_track_row,
                    cel : AnimationCel::new()
                }));

                picked_cel = true;
            }
            if let Some(old_cel) = animation.cel(self.current_frame.saturating_sub(1), self.current_track_row)
            {
                if imgui::Selectable::new("Clone Previous Cel").close_popups(false).build(ui)
                {
                    self.queue_action(Box::new(CostumeAction::SetCel
                    {
                        anim_index : self.current_animation,
                        frame_index : self.current_frame,
                        track_row : self.current_track_row,
                        cel : old_cel.clone()
                    }));

                    picked_cel = true;
                }
            }

            ui.separator();

            let mut package_to_load : Option<String> = None;

            for package in packages.package_iter()
            {
                ui.menu(&package, ||
                {
                    if !packages.package_loaded(ResourceType::Sheet, package)
                    {
                        package_to_load = Some(package.clone());
                    }

                    for sheet_name in packages.package_contents(ResourceType::Sheet, package)
                    {
                        ui.menu(&sheet_name, ||
                        {
                            let sheet_id = sheet_store.get_id(package, sheet_name).unwrap_or(DataId::new(0));

                            if let Some(sheet) = sheet_store.get(sheet_id)
                            {
                                for slice_id in 0..sheet.slice_count()
                                {
                                    if let Some(slice_name) = sheet.slice_name(slice_id)
                                    {
                                        if imgui::Selectable::new(&slice_name).close_popups(false).build(ui)
                                        {
                                            let mut cel = AnimationCel::new();

                                            cel.sheet = DataHandle::with_path(package, sheet_name);
                                            cel.slice = SliceHandle::new(&slice_name);

                                            self.queue_action(Box::new(CostumeAction::SetCel
                                            {
                                                anim_index : self.current_animation,
                                                frame_index : self.current_frame,
                                                track_row : self.current_track_row,
                                                cel
                                            }));

                                            picked_cel = true;
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }

            if let Some(package) = package_to_load
            {
                packages.load_package_later(ResourceType::Sheet, &package);
            }
        });

        if picked_cel && ui.io().key_ctrl
        {
            self.current_frame = (self.current_frame + 1) % animation.frame_count();
            self.animation_timer = 0.0;
        }
    }

    fn filmstrip(&mut self, packages : &mut Packages, ui : &imgui::Ui, mut sheet_store : &mut DataStore<Sheet>, animation : &Animation)
    {
        self.set_cel_menu(packages, ui, &mut sheet_store, animation);

        let style_filmstrip_spacing = ui.push_style_var(imgui::StyleVar::ItemSpacing([-1.0, 4.0]));

        // FIXME ImGui currently has a bug where adding a row of images extends the vertical scroll when it shouldn't.
        //  For now, turn off the vertical scrolling controls and force scroll Y to 0.
        ui.set_scroll_y(0.0);

        for i in 0..animation.frame_count()
        {
            if let Some(cel) = animation.cel(i, self.current_track_row)
            {
                let cursor_pos = ui.cursor_pos();
                let main_style = ui.clone_style();
                let mut main_button_color = main_style.colors[imgui::StyleColor::Button as usize];
                let transparent_color = [self.transparent_color[0], self.transparent_color[1], self.transparent_color[2], 1.0];

                if self.current_frame != i
                {
                    main_button_color = transparent_color;
                }

                let _cel_button_colors = [
                    ui.push_style_color(imgui::StyleColor::Border, crate::util::color_to_border_color(transparent_color)),
                    ui.push_style_color(imgui::StyleColor::Button, main_button_color)];

                if ui.button_with_size(format!("###cel{}", i), [CEL_BUTTON_SIZE, CEL_BUTTON_SIZE])
                {
                    self.current_frame = i;
                    self.animation_timer = 0.0;
                }

                if ui.is_item_hovered() && ui.is_mouse_clicked(imgui::MouseButton::Right)
                {
                    self.current_frame = i;
                    self.animation_timer = 0.0;
                    ui.open_popup("Set Cel Menu");
                }

                let (button_min, button_max) = (ui.item_rect_min(), ui.item_rect_max());

                ui.same_line();
                let new_cursor_pos = ui.cursor_pos();

                if self.current_frame == i && self.scroll_cel_list
                {
                    ui.set_scroll_here_x();
                    self.scroll_cel_list = false;
                }

                let draw_list = ui.get_window_draw_list();

                if ui.is_rect_visible(button_min, button_max)
                {
                    if let Some((image, fit)) = crate::util::drawable::cel_image_fit(cel, CEL_BUTTON_SIZE, CEL_BUTTON_SIZE, &mut sheet_store, packages)
                    {
                        draw_list.with_clip_rect_intersect(button_min, button_max, ||
                        {
                            ui.set_cursor_pos([cursor_pos[0] + fit.x, cursor_pos[1] + fit.y]);
                            image.build(ui);
                        });
                    }
                }

                let label = format!("{}", i);
                let size = ui.calc_text_size(&label);
                ui.set_cursor_pos([cursor_pos[0] + CEL_BUTTON_SIZE / 2.0 - size[0] / 2.0, cursor_pos[1] + CEL_BUTTON_SIZE]);

                ui.text(label);

                ui.set_cursor_pos(new_cursor_pos);
            }
        }

        style_filmstrip_spacing.pop();
    }

    fn animation_track_buttons(&mut self, ui : &imgui::Ui, costume : &Costume, animation : &Animation)
    {
        ui.popup("Add Animation Track Menu", ||
        {
            for i in 0..costume.track_count()
            {
                if animation.costume_track_to_row(i as u16).is_none()
                {
                    if let Some(track_name) = costume.track_name(i)
                    {
                        if imgui::MenuItem::new(&track_name).build(ui)
                        {
                            self.queue_action(Box::new(CostumeAction::AddAnimationTrack
                            {
                                anim_index : self.current_animation,
                                track_row : self.current_track_row,
                                track : AnimationTrack::new(i as u16)
                            }));
                        }
                    }
                }
            }
        });

        if ui.button("Add###Add Animation Track")
        {
            ui.open_popup("Add Animation Track Menu");
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Up###Animation Track Up") && self.current_track_row > 0
        {
            self.queue_action(Box::new(CostumeAction::MoveAnimationTrack
            {
                anim_index : self.current_animation,
                old_track_row : self.current_track_row,
                new_track_row : self.current_track_row.saturating_sub(1)
            }));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Down###Animation Track Down")
        {
            self.queue_action(Box::new(CostumeAction::MoveAnimationTrack
            {
                anim_index : self.current_animation,
                old_track_row : self.current_track_row,
                new_track_row : self.current_track_row + 1
            }));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove###Remove Animation Track")
        {
            self.queue_action(Box::new(CostumeAction::RemoveAnimationTrack
            {
                anim_index : self.current_animation,
                track_row : self.current_track_row
            }));
        }
    }

    fn filmstrip_buttons(&mut self, packages : &mut Packages, ui : &imgui::Ui, sheet_store : &mut DataStore<Sheet>, animation : &Animation)
    {
        self.set_cel_menu(packages, ui, sheet_store, animation);

        if ui.button("Add Frame")
        {
            self.queue_action(Box::new(CostumeAction::AddFrame
            {
                anim_index : self.current_animation,
                frame_index : self.current_frame,
                frame : AnimationFrame::new()
            }));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Set Cel")
        {
            ui.open_popup("Set Cel Menu");
        }
        if ui.is_item_hovered()
        {
            ui.tooltip_text("Set the slice for this cel.\nCtrl + Click: Set the slice and advance to next frame.\nMiddle-click: Advance to next frame.");
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Left") && self.current_frame > 0
        {
            if ui.io().key_ctrl
            {
                self.queue_action(Box::new(CostumeAction::MoveCel
                {
                    anim_index : self.current_animation,
                    old_frame_index : self.current_frame,
                    new_frame_index : self.current_frame.saturating_sub(1),
                    track_row : self.current_track_row
                }));
            }
            else
            {
                self.queue_action(Box::new(CostumeAction::MoveFrame
                {
                    anim_index : self.current_animation,
                    old_frame_index : self.current_frame,
                    new_frame_index : self.current_frame.saturating_sub(1)
                }));
            }
        }
        if ui.is_item_hovered()
        {
            ui.tooltip_text("Reorder this frame left, affecting all tracks.\nCtrl: Reorder the cel left for this track only.");
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Right")
        {
            if ui.io().key_ctrl
            {
                self.queue_action(Box::new(CostumeAction::MoveCel
                {
                    anim_index : self.current_animation,
                    old_frame_index : self.current_frame,
                    new_frame_index : self.current_frame + 1,
                    track_row : self.current_track_row
                }));
            }
            else
            {
                self.queue_action(Box::new(CostumeAction::MoveFrame
                {
                    anim_index : self.current_animation,
                    old_frame_index : self.current_frame,
                    new_frame_index : self.current_frame + 1
                }));
            }
        }
        if ui.is_item_hovered()
        {
            ui.tooltip_text("Reorder this frame right, affecting all tracks.\nCtrl: Reorder the cel right for this track only.");
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove Frame")
        {
            self.queue_action(Box::new(CostumeAction::RemoveFrame
            {
                anim_index : self.current_animation,
                frame_index : self.current_frame
            }));
        }

        ui.same_line_with_spacing(0.0, 20.0);
        ui.checkbox("Play Animation", &mut self.play_animation);
    }
}

impl Document for CostumeDocument
{
    fn load(game: &mut GameControl, package_name: &str, item_name: &str) -> Self where Self: Sized
    {
        let mut data_id = DataId::new(0);

        if let Ok(id) = game.res().store_mut::<Costume>().get_id_mut(package_name, item_name)
        {
            data_id = id;
        }

        let dpi_factor = game.renderer().current_display_scale_factor();

        CostumeDocument
        {
            action_history : ActionHistory::new(),
            data_id,
            show_origin : true,
            transparent_color : [0.5, 0.5001, 0.5002],
            zoom_control : ZoomControl::new(EDITOR_ZOOM_LEVELS.to_vec(), (1.0 * dpi_factor).round()),
            dpi_factor,
            current_animation : 0,
            current_frame : 0,
            current_track_row : 0,
            play_animation : false,
            animation_timer : 0.0,
            tics : 0,
            focus_animation_name_input : false,
            scroll_anim_list : false,
            scroll_cel_list : false
        }
    }

    fn action_history(&mut self) -> &mut ActionHistory
    {
        &mut self.action_history
    }

    fn apply_action(&mut self, game: &mut GameControl, action: &Box<dyn Any>) -> bool
    {
        if let Some(action) = action.downcast_ref::<CostumeAction>()
        {
            let mut costume_store = game.res().store_mut::<Costume>();

            if let Some(costume) = costume_store.get_mut(self.data_id)
            {
                match action
                {
                    CostumeAction::AddAnimation { index, animation } =>
                    {
                        self.scroll_anim_list = true;
                        self.add_animation(*index, costume, animation.clone());
                        return true;
                    }
                    CostumeAction::RenameAnimation { index, name } =>
                    {
                        self.scroll_anim_list = true;
                        return costume.rename_animation(*index, name);
                    }
                    CostumeAction::SetAnimation { index, animation } =>
                    {
                        self.scroll_anim_list = true;
                        return costume.set_animation(*index, animation.clone());
                    }
                    CostumeAction::MoveAnimation { old_index, new_index } =>
                    {
                        self.scroll_anim_list = true;
                        return self.move_animation(costume, *old_index, *new_index);
                    }
                    CostumeAction::RemoveAnimation { index } =>
                    {
                        self.scroll_anim_list = true;
                        return costume.remove_animation(*index).is_some();
                    }
                    CostumeAction::AddTrack { index, name } =>
                    {
                        return costume.insert_track(*index, name);
                    }
                    CostumeAction::RenameTrack { index, name } =>
                    {
                        return costume.rename_track(*index, name);
                    }
                    CostumeAction::RemoveTrack { index } =>
                    {
                        return costume.remove_track(*index);
                    }
                    CostumeAction::SetCel { anim_index, frame_index, track_row, cel } =>
                    {
                        if let Some(mut animation) = costume.animation(*anim_index).cloned()
                        {
                            if animation.set_cel(*frame_index, *track_row, cel.clone())
                            {
                                self.scroll_cel_list = true;
                                return costume.set_animation(*anim_index, animation);
                            }
                        }
                    }
                    CostumeAction::MoveCel { anim_index, old_frame_index, new_frame_index, track_row } =>
                    {
                        if let Some(mut animation) = costume.animation(*anim_index).cloned()
                        {
                            if let (Some(old_cel), Some(new_cel)) = (animation.cel(*old_frame_index, *track_row).cloned(),
                                                                    animation.cel(*new_frame_index, *track_row).cloned())
                            {
                                if animation.set_cel(*new_frame_index, *track_row, old_cel)
                                    && animation.set_cel(*old_frame_index, *track_row, new_cel)
                                {
                                    self.current_frame = *new_frame_index;
                                    self.animation_timer = 0.0;
                                    self.scroll_cel_list = true;
                                    return costume.set_animation(*anim_index, animation);
                                }
                            }
                        }
                    }
                    CostumeAction::AddFrame { anim_index, frame_index, frame } =>
                    {
                        return self.add_frame(*anim_index, *frame_index, costume, frame.clone());
                    }
                    CostumeAction::SetFrame { anim_index, frame_index, frame } =>
                    {
                        if let Some(mut animation) = costume.animation(*anim_index).cloned()
                        {
                            if animation.set_frame(*frame_index, frame.clone())
                            {
                                self.scroll_cel_list = true;
                                return costume.set_animation(*anim_index, animation);
                            }
                        }
                    }
                    CostumeAction::MoveFrame { anim_index, old_frame_index, new_frame_index } =>
                    {
                        if let Some(mut animation) = costume.animation(*anim_index).cloned()
                        {
                            if animation.move_frame(*old_frame_index, *new_frame_index)
                            {
                                self.current_frame = *new_frame_index;
                                self.animation_timer = 0.0;
                                self.scroll_cel_list = true;
                                return costume.set_animation(*anim_index, animation);
                            }
                        }
                    }
                    CostumeAction::RemoveFrame { anim_index, frame_index } =>
                    {
                        if let Some(mut animation) = costume.animation(*anim_index).cloned()
                        {
                            if animation.remove_frame(*frame_index)
                            {
                                self.scroll_cel_list = true;
                                return costume.set_animation(*anim_index, animation);
                            }
                        }
                    }
                    CostumeAction::AddAnimationTrack { anim_index, track_row, track } =>
                    {
                        return self.add_animation_track(*anim_index, *track_row, costume, track.clone());
                    }
                    CostumeAction::MoveAnimationTrack { anim_index, old_track_row, new_track_row } =>
                    {
                        if let Some(mut animation) = costume.animation(*anim_index).cloned()
                        {
                            if animation.move_track(*old_track_row, *new_track_row)
                            {
                                self.current_track_row = *new_track_row;
                                return costume.set_animation(*anim_index, animation);
                            }
                        }
                    }
                    CostumeAction::RemoveAnimationTrack { anim_index, track_row } =>
                    {
                        if let Some(mut animation) = costume.animation(*anim_index).cloned()
                        {
                            if animation.remove_track(*track_row)
                            {
                                return costume.set_animation(*anim_index, animation);
                            }
                        }
                    }
                }
            }
        }

        false
    }

    fn do_ui(&mut self, packages : &mut Packages, _config : &mut EditorConfig,
             _project : &mut ProjectConfig, game : &mut GameControl, ui : &imgui::Ui) -> DocumentState
    {
        self.tics = self.tics.wrapping_add(1);
        self.dpi_factor = game.renderer().current_display_scale_factor();

        let mut result = DocumentState::new();

        ui.menu_bar(||
        {
            ui.menu("View", ||
            {
                if imgui::MenuItem::new("Show Origin").selected(self.show_origin).build(ui)
                {
                    self.show_origin = !self.show_origin;
                }

                ui.menu("Background Color", ||
                {
                    imgui::ColorPicker::new("##background_color_picker", imgui::EditableColor::Float3(&mut self.transparent_color)).build(ui);
                });
            });
            ui.menu("Zoom", ||
            {
                for level in &EDITOR_ZOOM_LEVELS
                {
                    if imgui::MenuItem::new(format!("{}%", (level * 100.0) as i32)).selected(self.zoom_control.zoom() == *level).build(ui)
                    {
                        self.zoom_control.set_zoom(*level);
                    }
                }
            });
        });

        result.active = ui.is_window_focused();

        let mut costume_store = game.res().store_mut::<Costume>();
        let mut sheet_store = game.res().store_mut::<Sheet>();

        if let Some(costume) = costume_store.get_mut(self.data_id)
        {
            ui.columns(2, "##costume_columns", false);

            if let Some(costume_controls) = imgui::ChildWindow::new("costume_controls").size([240.0, 0.0])
                .draw_background(true).border(false).horizontal_scrollbar(true).begin(ui)
            {
                if let Some(tab_bar) = imgui::TabBar::new("controls_tabbar")
                    .flags(imgui::TabBarFlags::FITTING_POLICY_SCROLL).begin(ui)
                {
                    if let Some(tab) = imgui::TabItem::new("Costume").begin(ui)
                    {
                        self.animation_view(ui, costume);

                        tab.end();
                    }

                    if let Some(tab) = imgui::TabItem::new("Track Names").begin(ui)
                    {
                        self.track_name_view(ui, costume);

                        tab.end();
                    }

                    tab_bar.end();
                }

                costume_controls.end();
            }

            if self.play_animation
            {
                if let Some(animation) = costume.animation(self.current_animation)
                {
                    if let Some(frame) = animation.frame(self.current_frame)
                    {
                        self.animation_timer += 1.0;

                        if self.animation_timer >= frame.delay
                        {
                            self.current_frame += 1;

                            if animation.looping()
                            {
                                self.current_frame = self.current_frame % animation.frame_count();
                            }

                            self.current_frame = self.current_frame.min((animation.frame_count()).max(1) - 1);

                            self.animation_timer -= frame.delay;
                        }
                    }
                }
            }

            ui.set_current_column_width(ui.item_rect_size()[0] + 10.0);
            ui.next_column();

            if let Some(animation) = costume.animation(self.current_animation)
            {
                let main_style = ui.clone_style();
                let filmstrip_height = CEL_BUTTON_SIZE + ui.current_font_size() + 8.0 + main_style.scrollbar_size;
                let upper_bar_height = filmstrip_height + ui.current_font_size() * 2.0
                    + main_style.frame_padding[1] * 2.0 + main_style.item_spacing[1] * 3.0;

                let style_upper_bar_spacing = ui.push_style_var(imgui::StyleVar::ItemSpacing([0.0, main_style.item_spacing[1]]));
                let style_window_padding = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));

                if let Some(track_film_view) = imgui::ChildWindow::new("track_film_view").size([ui.content_region_avail()[0] + 1.0, upper_bar_height])
                    .scroll_bar(false).scrollable(false).begin(ui)
                {
                    style_window_padding.pop();

                    self.current_frame = self.current_frame.min((animation.frame_count()).max(1) - 1);
                    self.current_track_row = self.current_track_row.min((animation.track_count()).max(1) - 1);

                    ui.columns(2, "##costume_filmstrip_columns", false);

                    style_upper_bar_spacing.pop();

                    ui.text("Animation Tracks");

                    let style_track_padding = ui.push_style_var(imgui::StyleVar::WindowPadding([3.0, 2.0]));
                    let colors_track_list = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);

                    if let Some(track_list) = imgui::ChildWindow::new("animation_track_list").size([200.0, filmstrip_height]).border(true)
                        .draw_background(true).begin(ui)
                    {
                        for i in 0..animation.track_count()
                        {
                            if let Some(track) = animation.track(i)
                            {
                                let track_name = match costume.track_name(track.track_id as usize)
                                {
                                    Some(name) => name,
                                    None => format!("<costume track {}>", track.track_id)
                                };

                                if imgui::Selectable::new(&track_name).selected(self.current_track_row == i).build(ui)
                                {
                                    self.current_track_row = i;
                                }
                            }
                        }

                        track_list.end();
                    }

                    colors_track_list.pop();
                    style_track_padding.pop();

                    ui.set_current_column_width(ui.item_rect_size()[0] + 10.0);

                    self.animation_track_buttons(ui, costume, animation);

                    let style_upper_bar_spacing = ui.push_style_var(imgui::StyleVar::ItemSpacing([0.0, main_style.item_spacing[1]]));
                    ui.next_column();
                    style_upper_bar_spacing.pop();

                    ui.text("Cels");

                    let colors_filmstrip = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);
                    let style_filmstrip_padding = ui.push_style_var(imgui::StyleVar::WindowPadding([4.0, 4.0]));

                    if let Some(filmstrip) = imgui::ChildWindow::new("filmstrip").size([-1.0, filmstrip_height]).draw_background(true).border(true)
                        .horizontal_scrollbar(true).always_horizontal_scrollbar(true).scroll_bar(false).scrollable(false).begin(ui)
                    {
                        style_filmstrip_padding.pop();

                        self.filmstrip(packages, ui, &mut sheet_store, animation);

                        filmstrip.end();
                    }
                    else
                    {
                        style_filmstrip_padding.pop();
                    }

                    colors_filmstrip.pop();

                    self.filmstrip_buttons(packages, ui, &mut sheet_store, animation);

                    track_film_view.end();
                }
                else
                {
                    style_window_padding.pop();
                    style_upper_bar_spacing.pop();
                }

                let style_var = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));
                let style_color = ui.push_style_color(imgui::StyleColor::ChildBg, [self.transparent_color[0], self.transparent_color[1], self.transparent_color[2], 1.0]);

                imgui::ChildWindow::new("texture_slice_item").size([0.0, 0.0])
                    .scrollable(false).scroll_bar(false).draw_background(true).border(true).build(ui, ||
                {
                    if ui.is_window_hovered_with_flags(imgui::WindowHoveredFlags::CHILD_WINDOWS)
                    {
                        self.zoom_control.update_mouse_wheel(ui.io().mouse_wheel);
                    }

                    if let Some(cel) = animation.cel(self.current_frame, self.current_track_row)
                    {
                        if let Some((image, rect)) = crate::util::drawable::cel_image(cel, &mut sheet_store, packages)
                        {
                            let slice_space = ui.window_size();
                            let center_x = self.scaled_round(slice_space[0] / 2.0);
                            let center_y = self.scaled_round(slice_space[1] / 2.0);
                            let image_x = self.scaled_round(center_x - rect.x * self.adjusted_zoom());
                            let image_y = self.scaled_round(center_y - rect.y * self.adjusted_zoom());

                            ui.set_cursor_pos([image_x, image_y]);
                            crate::util::widget::round_cursor_screen_pos(ui);

                            image.size([self.scaled_round(rect.w * self.adjusted_zoom()),
                                        self.scaled_round(rect.h * self.adjusted_zoom())]).build(ui);

                            ui.set_cursor_pos([center_x, center_y]);
                            crate::util::widget::round_cursor_screen_pos(ui);

                            let draw_list = ui.get_window_draw_list();
                            let cursor_pos = ui.cursor_screen_pos();

                            if self.show_origin
                            {
                                self.draw_crosshair(&draw_list, cursor_pos[0], cursor_pos[1]);
                            }
                        }
                    }
                    else
                    {
                        ui.text("No cel selected!");
                    }
                });

                style_color.pop();
                style_var.pop();
            }
        }

        result
    }
}

use std::cmp::Ordering;
use std::collections::{BTreeMap, HashMap};
use std::ops::{Deref, DerefMut};

use keeshond::renderer::{Sheet, SpriteSlice, Color};
use image::{RgbaImage, GrayImage, GenericImage, GenericImageView, ImageBuffer};
use rectangle_pack::{GroupedRectsToPlace, RectToInsert, TargetBin, RectanglePackOk};

use crate::import::{ImportItemPath, ImportGroupItem, ImportConfig};

const MIN_TEXTURE_SIZE : u32 = 32;
const MAX_TEXTURE_TILE_DIMENSIONS : u32 = 4096;
const MAX_TEXTURE_TILE_DIMENSIONS_MANUAL : u32 = 128;

fn automatic_or_number_printf(number : u32) -> &'static str
{
    if number == 0
    {
        return "Automatic";
    }

    "%d"
}

#[derive(Clone, Eq, PartialEq)]
pub enum ImportedImage
{
    Rgb(RgbaImage),
    Indexed(GrayImage)
}

impl PartialOrd<Self> for ImportedImage
{
    fn partial_cmp(&self, other : &Self) -> Option<Ordering>
    {
        Some(self.cmp(other))
    }
}

impl Ord for ImportedImage
{
    fn cmp(&self, other : &Self) -> Ordering
    {
        match self
        {
            ImportedImage::Rgb(image) =>
            {
                match other
                {
                    ImportedImage::Rgb(other_image) =>
                    {
                        image.as_raw().cmp(other_image.as_raw())
                    }
                    ImportedImage::Indexed(_) => { Ordering::Greater }
                }
            }
            ImportedImage::Indexed(image) =>
            {
                match other
                {
                    ImportedImage::Rgb(_) => { Ordering::Less }
                    ImportedImage::Indexed(other_image) =>
                    {
                        image.as_raw().cmp(other_image.as_raw())
                    }
                }
            }
        }
    }
}

impl ImportedImage
{
    pub fn size(&self) -> (u32, u32)
    {
        match self
        {
            ImportedImage::Rgb(image) => { (image.width(), image.height()) }
            ImportedImage::Indexed(image) => { (image.width(), image.height()) }
        }
    }

    pub fn blit(&mut self, other : &ImportedImage, x : u32, y : u32) -> Result<(), String>
    {
        match self
        {
            ImportedImage::Rgb(image) =>
            {
                if let ImportedImage::Rgb(sub) = other
                {
                    if let Err(error) = image.copy_from(sub, x, y)
                    {
                        return Err(error.to_string());
                    }
                }
                else
                {
                    return Err(String::from("Color format mismatch (destination is RGB)"));
                }
            }
            ImportedImage::Indexed(image) =>
            {
                if let ImportedImage::Indexed(sub) = other
                {
                    if let Err(error) = image.copy_from(sub, x, y)
                    {
                        return Err(error.to_string());
                    }
                }
                else
                {
                    return Err(String::from("Color format mismatch (destination is indexed)"));
                }
            }
        }

        Ok(())
    }

    fn crop_dimensions<T : GenericImageView, F : Fn(T::Pixel) -> bool>(image : &T, is_transarent : F) -> keeshond::util::Rect<u32>
    {
        let mut empty = true;
        let width = image.width();
        let height = image.height();
        let mut x1 = width - 1;
        let mut y1 = height - 1;
        let mut x2 = 0;
        let mut y2 = 0;

        for y in 0..height
        {
            for x in 0..width
            {
                if !is_transarent(image.get_pixel(x, y))
                {
                    empty = false;
                    x1 = x1.min(x);
                    y1 = y1.min(y);
                    x2 = x2.max(x);
                    y2 = y2.max(y);
                }
            }
        }

        if empty
        {
            return keeshond::util::Rect
            {
                x : width / 2,
                y : height / 2,
                w : width.min(2),
                h : height.min(2)
            };
        }

        keeshond::util::Rect
        {
            x : x1,
            y : y1,
            w : x2 - x1 + 1,
            h : y2 - y1 + 1
        }
    }

    pub fn optimize(&mut self, padding : u32) -> (i32, i32)
    {
        match self
        {
            ImportedImage::Rgb(image) =>
            {
                let rect = ImportedImage::crop_dimensions(image, |pixel|
                {
                    pixel.0[3] == 0
                });

                let image_width = rect.w + padding * 2;
                let image_height = rect.h + padding * 2;

                let cropped = image.view(rect.x, rect.y, rect.w, rect.h);
                let mut new_image = RgbaImage::from_raw(image_width, image_height, vec![0; (image_width * image_height * 4) as usize]).unwrap();

                new_image.copy_from(&cropped, padding, padding).unwrap();

                crate::util::sheet::defringe_image(&mut new_image, false);

                *image = new_image;

                return (-(rect.x as i32) + padding as i32, -(rect.y as i32) + padding as i32);
            }
            ImportedImage::Indexed(image) =>
            {
                let rect = ImportedImage::crop_dimensions(image, |pixel|
                {
                    pixel.0[0] == 0
                });

                let image_width = rect.w + 2;
                let image_height = rect.h + 2;

                let cropped = image.view(rect.x, rect.y, rect.w, rect.h);
                let mut new_image = GrayImage::from_raw(image_width, image_height, vec![0; (image_width * image_height) as usize]).unwrap();

                new_image.copy_from(&cropped, 1, 1).unwrap();
                *image = new_image;

                return (-(rect.x as i32) + 1, -(rect.y as i32) + 1);
            }
        }
    }
}

#[derive(Clone, PartialEq, PartialOrd)]
pub struct ImageImportData
{
    pub image : ImportedImage,
    pub origin_x : f32,
    pub origin_y : f32
}

pub struct FrameTrackData
{
    pub track_name : String,
    pub index : usize
}

pub struct FrameImportData
{
    pub tracks : Vec<FrameTrackData>,
    pub delay : f64
}

pub struct AnimationImportData
{
    pub animation_name : String,
    pub frames : Vec<FrameImportData>,
    pub looping : bool
}

pub struct SpriteImportData
{
    pub palette : Option<Vec<Color>>,
    pub images : Vec<ImageImportData>,
    pub animations : Vec<AnimationImportData>,
    pub default_track_name : String,
    pub spawn_type_name : String,
    pub dest_sheet_package : String,
    pub dest_sheet : String,
    pub dest_costume_package : String,
    pub dest_costume : String,
    pub dest_spawnable_package : String,
    pub dest_spawnable : String
}

impl SpriteImportData
{
    pub fn dedup(&mut self)
    {
        // List of sequential IDs that we will sort based on contents, to sort each frame
        //  without actually moving the frames around
        let mut ids : Vec<usize> = (0..self.images.len()).collect();

        ids.sort_by(|a, b|
        {
            self.images[*a].partial_cmp(&self.images[*b]).unwrap_or(Ordering::Equal)
        });

        // Find duplicates using this newly sorted ID list
        let mut dups = HashMap::new();
        let mut last_maybe_dup = ids[0];

        for i in 1..ids.len()
        {
            // Always remember to resolve the IDs back to the original frame vec indexes
            let j = ids[i];

            if self.images[j] == self.images[last_maybe_dup]
            {
                dups.insert(j, last_maybe_dup);
            }
            else
            {
                last_maybe_dup = j;
            }
        }

        // Remove each duplicate image from the list of frames, keeping track of which IDs need
        //  to be shifted to the left and by how much
        let mut dup_shift = vec![0; self.images.len()];
        let mut i = 0;
        let mut dup_count = 0;

        self.images.retain(|_|
        {
            let is_dup = dups.contains_key(&(i));

            if is_dup
            {
                dup_count += 1;
            }

            dup_shift[i] += dup_count;

            i += 1;
            !is_dup
        });

        // Update animation data to point to deduplicated frame IDs, shifted to the left to account
        //  for deleted frames
        for animation in &mut self.animations
        {
            for frame in &mut animation.frames
            {
                for track in &mut frame.tracks
                {
                    if dups.contains_key(&track.index)
                    {
                        track.index = dups[&track.index];
                    }

                    track.index -= dup_shift[track.index];
                }
            }
        }
    }
}

pub struct BackgroundImportData
{
    pub palette : Option<Vec<Color>>,
    pub image : ImportedImage,
    pub dest_package : String,
    pub dest_sheet : String,
    pub tilegen : Option<TilesetGenInfo>
}

pub struct TilesetGenInfo
{
    pub width : u32,
    pub height : u32,
    pub x : u32,
    pub y : u32,
    pub space_x : u32,
    pub space_y : u32,
    pub rows : u32,
    pub columns : u32,
}

impl TilesetGenInfo
{
    pub fn from_config(config : &mut ImportConfig) -> TilesetGenInfo
    {
        TilesetGenInfo
        {
            width : config.value_int_or_default("tilegen_w", 32) as u32,
            height : config.value_int_or_default("tilegen_h", 32) as u32,
            x : config.value_int_or_default("tilegen_x", 0) as u32,
            y : config.value_int_or_default("tilegen_y", 0) as u32,
            space_x : config.value_int_or_default("tilegen_space_x", 0) as u32,
            space_y : config.value_int_or_default("tilegen_space_y", 0) as u32,
            rows : config.value_int_or_default("tilegen_rows", 0) as u32,
            columns : config.value_int_or_default("tilegen_columns", 0) as u32
        }
    }

    pub fn save_to_config(&self, config : &mut ImportConfig)
    {
        config.set_int("tilegen_w", self.width as i32);
        config.set_int("tilegen_h", self.height as i32);
        config.set_int("tilegen_x", self.x as i32);
        config.set_int("tilegen_y", self.y as i32);
        config.set_int("tilegen_space_x", self.space_x as i32);
        config.set_int("tilegen_space_y", self.space_y as i32);
        config.set_int("tilegen_rows", self.rows as i32);
        config.set_int("tilegen_columns", self.columns as i32);
    }

    pub fn tilegen_ui(&mut self, ui : &imgui::Ui) -> bool
    {
        let mut update = false;
        let mut size = [self.width, self.height];
        let mut offset = [self.x, self.y];
        let mut spacing = [self.space_x, self.space_y];

        update |= imgui::Drag::new("Tile Size").display_format("%d px").build_array(ui, &mut size);
        update |= imgui::Drag::new("Start Offset").display_format("%d px").build_array(ui, &mut offset);
        update |= imgui::Drag::new("Tile Spacing").display_format("%d px").build_array(ui, &mut spacing);
        update |= imgui::Drag::new("Rows").range(0, MAX_TEXTURE_TILE_DIMENSIONS_MANUAL)
            .display_format(automatic_or_number_printf(self.rows)).build(ui, &mut self.rows);
        update |= imgui::Drag::new("Columns").range(0, MAX_TEXTURE_TILE_DIMENSIONS_MANUAL)
            .display_format(automatic_or_number_printf(self.columns)).build(ui, &mut self.columns);

        if update
        {
            self.width = size[0];
            self.height = size[1];
            self.x = offset[0];
            self.y = offset[1];
            self.space_x = spacing[0];
            self.space_y = spacing[1];
        }

        update
    }
}

pub struct TilesetSliceInfo
{
    pub rows : u32,
    pub columns : u32,
    pub tile_walk_x : u32,
    pub tile_walk_y : u32
}

pub fn tileset_slice_info(width : u32, height : u32, gen_info : &TilesetGenInfo) -> TilesetSliceInfo
{
    let tile_walk_x = gen_info.width + gen_info.space_x;
    let tile_walk_y = gen_info.height + gen_info.space_y;
    let max_columns = (width - gen_info.x.min(width) + gen_info.space_x) / tile_walk_x;
    let max_rows = (height - gen_info.y.min(height) + gen_info.space_y) / tile_walk_y;
    let mut columns = gen_info.columns.min(max_columns);
    let mut rows = gen_info.rows.min(max_rows);

    if columns == 0
    {
        columns = max_columns;
    }
    if rows == 0
    {
        rows = max_rows;
    }

    TilesetSliceInfo
    {
        rows,
        columns,
        tile_walk_x,
        tile_walk_y
    }
}

pub fn create_tileset_slices(gen_info : &TilesetGenInfo, tileset_info : &TilesetSliceInfo) -> Vec<SpriteSlice>
{
    let mut slices = Vec::new();

    for y in 0..tileset_info.rows
    {
        for x in 0..tileset_info.columns
        {
            slices.push(SpriteSlice
            {
                texture_x : gen_info.x as f32 + (tileset_info.tile_walk_x * x) as f32,
                texture_y : gen_info.y as f32 + (tileset_info.tile_walk_y * y) as f32,
                texture_w : gen_info.width as f32,
                texture_h : gen_info.height as f32,
                origin_x : 0.0,
                origin_y : 0.0
            });
        }
    }

    slices
}

fn create_tileset_texture_internal<P, C>(texture : &ImageBuffer<P, C>, new_texture : &mut ImageBuffer<P, C>, gen_info : &TilesetGenInfo, tileset_info : &TilesetSliceInfo)
    where
        P : image::Pixel + 'static,
        P::Subpixel : 'static,
        C : Deref<Target = [P::Subpixel]> + DerefMut,
{
    let new_tile_width = gen_info.width + 2;
    let new_tile_height = gen_info.height + 2;
    let (width, _) = new_texture.dimensions();
    let new_columns = width / new_tile_width;

    for y in 0..tileset_info.rows
    {
        for x in 0..tileset_info.columns
        {
            let source_x = gen_info.x + (tileset_info.tile_walk_x * x);
            let source_y = gen_info.y + (tileset_info.tile_walk_y * y);
            let index = y * tileset_info.columns + x;
            let dest_x = (index % new_columns) * new_tile_width;
            let dest_y = (index / new_columns) * new_tile_height;

            let mut tile_image = ImageBuffer::new(new_tile_width, new_tile_height);
            let texture_view = texture.view(source_x, source_y, gen_info.width, gen_info.height);

            // Main tile image
            tile_image.copy_from(&texture_view, 1, 1).expect("Image copy failed");

            // Tile image edge padding
            tile_image.copy_within(image::math::Rect { x : 1, y : 1, width : 1, height : gen_info.height }, 0, 1);
            tile_image.copy_within(image::math::Rect { x : gen_info.width, y : 1, width : 1, height : gen_info.height }, gen_info.width + 1, 1);
            tile_image.copy_within(image::math::Rect { x : 0, y : 1, width : new_tile_width, height : 1 }, 0, 0);
            tile_image.copy_within(image::math::Rect { x : 0, y : gen_info.height, width : new_tile_width, height : 1 }, 0, gen_info.height + 1);

            new_texture.copy_from(&tile_image, dest_x, dest_y).expect("Image copy failed");
        }
    }
}

pub fn create_tileset_texture_and_slices<P>(texture : &ImageBuffer<P, Vec<<P as image::Pixel>::Subpixel>>, gen_info : &mut TilesetGenInfo)
                                            -> (ImageBuffer<P, Vec<<P as image::Pixel>::Subpixel>>, Vec<SpriteSlice>)
    where
        P : image::Pixel + 'static,
        P::Subpixel : 'static
{
    let mut tileset_info = tileset_slice_info(texture.width(), texture.height(), gen_info);

    let width = (texture.width() as f32).max(gen_info.width as f32 + 2.0).log2().ceil().exp2() as u32;
    let total_tiles = tileset_info.columns * tileset_info.rows;
    let new_columns = (width / (gen_info.width + 2)).min(MAX_TEXTURE_TILE_DIMENSIONS);
    let new_rows = ((total_tiles as f32 / new_columns as f32).ceil() as u32).min(MAX_TEXTURE_TILE_DIMENSIONS);
    let height = ((new_rows * (gen_info.height + 2)) as f32).log2().ceil().exp2() as u32;

    let mut new_texture = ImageBuffer::<P, Vec<<P as image::Pixel>::Subpixel>>::new(width, height);

    create_tileset_texture_internal(&texture, &mut new_texture, gen_info, &tileset_info);

    gen_info.x = 1;
    gen_info.y = 1;
    gen_info.space_x = 2;
    gen_info.space_y = 2;
    gen_info.columns = new_columns;
    gen_info.rows = new_rows;

    tileset_info = tileset_slice_info(width, height, gen_info);

    let slices = create_tileset_slices(gen_info, &tileset_info);

    (new_texture, slices)
}

struct PackerSlice
{
    image : ImportedImage,
    slice : SpriteSlice,
    name : Option<String>
}

pub struct SheetPacker
{
    palette : Option<Vec<Color>>,
    slices : Vec<PackerSlice>
}

fn slice_printable_name(slice_index : usize, sheet : &Sheet) -> String
{
    if let Some(name) = sheet.slice_name(slice_index)
    {
        format!("\"{}\"", name)
    }
    else
    {
        format!("{}", slice_index)
    }
}

fn get_slice_pixel_rect(slice_index : usize, sheet : &Sheet) -> Result<keeshond::util::Rect<u32>, String>
{
    let slice = sheet.slice(slice_index).expect("Invalid slice index");
    let slice_name = slice_printable_name(slice_index, sheet);

    if slice.texture_x < 0.0 || slice.texture_y < 0.0
    {
        return Err(format!("Slice {} is outside of sheet", slice_name));
    }
    if slice.texture_w < 0.0 { return Err(format!("Slice {} has negative width", slice_name)); }
    if slice.texture_h < 0.0 { return Err(format!("Slice {} has negative height", slice_name)); }

    let pixel_rect = keeshond::util::Rect::<u32>
    {
        x : slice.texture_x.floor() as u32,
        y : slice.texture_y.floor() as u32,
        w : slice.texture_w.ceil() as u32,
        h : slice.texture_h.ceil() as u32
    };

    if pixel_rect.x2() > sheet.width() as u32
        || pixel_rect.y2() > sheet.height() as u32
    {
        return Err(format!("Slice {} is outside of sheet", slice_name));
    }

    Ok(pixel_rect)
}

fn sheet_raw_to_image(sheet : &Sheet) -> Result<ImportedImage, String>
{
    match &sheet.palette()
    {
        None =>
        {
            if let Some(image) = RgbaImage::from_raw(sheet.width(), sheet.height(), sheet.data().clone())
            {
                return Ok(ImportedImage::Rgb(image));
            }
        }
        Some(_) =>
        {
            if let Some(image) = GrayImage::from_raw(sheet.width(), sheet.height(), sheet.data().clone())
            {
                return Ok(ImportedImage::Indexed(image));
            }
        }
    }

    Err(String::from("Could not create image from sheet"))
}

fn get_subimage(image : &ImportedImage, rect : keeshond::util::Rect<u32>) -> ImportedImage
{
    match image
    {
        ImportedImage::Rgb(image) =>
        {
            ImportedImage::Rgb(image.view(rect.x, rect.y, rect.w, rect.h).to_image())
        }
        ImportedImage::Indexed(image) =>
        {
            ImportedImage::Indexed(image.view(rect.x, rect.y, rect.w, rect.h).to_image())
        }
    }
}

impl SheetPacker
{
    pub fn new() -> SheetPacker
    {
        SheetPacker
        {
            slices : Vec::new(),
            palette : None
        }
    }

    fn check_same_format(&mut self, other_palette : &Option<Vec<Color>>) -> Result<(), String>
    {
        if self.slices.is_empty()
        {
            self.palette = other_palette.clone();

            return Ok(());
        }

        if self.palette.is_some() != other_palette.is_some()
        {
            return Err(String::from("Sheet color format mismatch."));
        }

        if self.palette != *other_palette
        {
            return Err(String::from("Sheet color palette mismatch."));
        }

        Ok(())
    }

    fn check_sheet_same_format(&mut self, sheet : &Sheet) -> Result<(), String>
    {
        self.check_same_format(&sheet.palette())
    }

    fn check_import_same_format(&mut self, imported : &SpriteImportData) -> Result<(), String>
    {
        self.check_same_format(&imported.palette)
    }

    pub fn add_sheet_slices(&mut self, sheet : &Sheet, ignore_prefixes : &Vec<String>) -> Result<(), String>
    {
        if sheet.slice_count() == 0
        {
            return Ok(())
        }

        self.check_sheet_same_format(sheet)?;

        let image = sheet_raw_to_image(sheet)?;

        'slice_loop : for i in 0..sheet.slice_count()
        {
            if let Some(name) = sheet.slice_name(i)
            {
                for prefix in ignore_prefixes
                {
                    if name.starts_with(prefix)
                    {
                        continue 'slice_loop;
                    }
                }
            }

            let orig_slice = sheet.slice(i).unwrap();
            let pixel_rect = get_slice_pixel_rect(i, sheet)?;
            let subimage = get_subimage(&image, pixel_rect);

            self.slices.push(PackerSlice
            {
                image : subimage,
                slice : SpriteSlice
                {
                    texture_x : orig_slice.texture_x - pixel_rect.x as f32,
                    texture_y : orig_slice.texture_y - pixel_rect.y as f32,
                    texture_w : orig_slice.texture_w,
                    texture_h : orig_slice.texture_h,
                    origin_x : orig_slice.origin_x,
                    origin_y : orig_slice.origin_y
                },
                name : sheet.slice_name(i)
            });
        }

        Ok(())
    }

    pub fn add_import_slices(&mut self, group : &Vec<ImportGroupItem<SpriteImportData>>,
        prefixes : &Vec<String>) -> Result<(), String>
    {
        assert_eq!(group.len(), prefixes.len());

        for i in 0..group.len()
        {
            let item = &group[i];
            let prefix = &prefixes[i];

            self.check_import_same_format(&item.data)?;

            for j in 0..item.data.images.len()
            {
                let image = &item.data.images[j];
                let (width, height) = image.image.size();

                self.slices.push(PackerSlice
                {
                    image : image.image.clone(),
                    slice : SpriteSlice
                    {
                        texture_x : 0.0,
                        texture_y : 0.0,
                        texture_w : width as f32,
                        texture_h : height as f32,
                        origin_x : image.origin_x,
                        origin_y : image.origin_y
                    },
                    name : Some(format!("{}{}", prefix, j))
                });
            }
        }

        Ok(())
    }

    fn apply_placements_to_sheet(&self, sheet : &mut Sheet,
                                 placements : RectanglePackOk<usize, usize>,
                                 size_w : u32, size_h : u32) -> Result<(), String>
    {
        sheet.set_palette(self.palette.clone());
        sheet.clear_slices();

        let mut image = match sheet.palette()
        {
            None => { ImportedImage::Rgb(RgbaImage::from_raw(size_w, size_h, vec![0; (size_w * size_h * 4) as usize]).unwrap()) }
            Some(_) => { ImportedImage::Indexed(GrayImage::from_raw(size_w, size_h, vec![0; (size_w * size_h) as usize]).unwrap()) }
        };

        for (rect_id, place) in placements.packed_locations()
        {
            if let Some(to_place) = self.slices.get(*rect_id)
            {
                let slice = &to_place.slice;

                image.blit(&to_place.image, place.1.x(), place.1.y())?;

                sheet.add_slice(SpriteSlice
                {
                    texture_x : slice.texture_x + place.1.x() as f32,
                    texture_y : slice.texture_y + place.1.y() as f32,
                    texture_w : slice.texture_w,
                    texture_h : slice.texture_h,
                    origin_x : slice.origin_x,
                    origin_y : slice.origin_y
                });

                // TODO unnamed slices might get shuffled around
                if let Some(name) = &to_place.name
                {
                    sheet.rename_slice(sheet.slice_count() - 1, name);
                }
            }
        }

        match image
        {
            ImportedImage::Rgb(image) => { sheet.set_rgba_data_with_size(image.to_vec(), size_w, size_h); }
            ImportedImage::Indexed(image) => { sheet.set_indexed_data_with_size(image.to_vec(), size_w, size_h, self.palette.clone().unwrap()); }
        }

        Ok(())
    }

    pub fn pack_into_sheet(&self, sheet : &mut Sheet, max_size : u32) -> Result<(), String>
    {
        let mut success = false;
        let mut rects_to_place = GroupedRectsToPlace::<usize, ()>::new();
        let mut texture_sizes_to_try = Vec::new();
        let mut total_slice_pixels = 0;

        for i in 0..self.slices.len()
        {
            let slice = &self.slices[i];
            let (width, height) = slice.image.size();

            rects_to_place.push_rect(i, None, RectToInsert::new(width, height, 1));
            total_slice_pixels += width * height;
        }

        let mut size = MIN_TEXTURE_SIZE;

        while size <= max_size
        {
            if size * size >= total_slice_pixels
            {
                texture_sizes_to_try.push((size, size));
            }
            if size * size * 2 >= total_slice_pixels && size * 2 <= max_size
            {
                texture_sizes_to_try.push((size * 2, size));
            }

            size *= 2;
        }

        for (size_w, size_h) in &texture_sizes_to_try
        {
            let mut bins = BTreeMap::<usize, TargetBin>::new();
            bins.insert(0, TargetBin::new(*size_w, *size_h, 1));

            match rectangle_pack::pack_rects(&rects_to_place,
                                       &mut bins,
                                       &rectangle_pack::volume_heuristic,
                                       &rectangle_pack::contains_smallest_box)
            {
                Ok(placements) =>
                {
                    self.apply_placements_to_sheet(sheet, placements, *size_w, *size_h)?;
                    success = true;
                    break;
                }
                Err(_) => { continue; }
            }
        }

        if !success
        {
            return Err(format!("Not enough room in max texture size ({}x{}) for all sprites", max_size, max_size));
        }

        Ok(())
    }
}

pub fn update_sheet_with_import_group(sheet : &mut Sheet,
                                      group : &Vec<ImportGroupItem<SpriteImportData>>,
                                      max_size : u32,
                                      basedir : &str) -> Result<(), String>
{
    let mut packer = SheetPacker::new();
    let mut prefixes = Vec::new();

    for item in group
    {
        let path_info = ImportItemPath::from_absolute(item.pathbuf.clone(), basedir);
        let prefix = format!("{}/{}//", path_info.package, path_info.name);

        prefixes.push(prefix);
    }

    packer.add_sheet_slices(sheet, &prefixes)?;
    packer.add_import_slices(group, &prefixes)?;
    packer.pack_into_sheet(sheet, max_size)?;

    Ok(())
}

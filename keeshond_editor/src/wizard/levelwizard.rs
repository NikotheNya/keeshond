use keeshond::gameloop::GameControl;
use keeshond_treats::level::Level;

use crate::editorgui::{Packages};
use crate::wizard::{Wizard, WizardState};
use keeshond::util::BakedRect;

pub struct LevelWizard
{
    package_name : String,
    item : String,
    bounds_min : [f64; 2],
    bounds_max : [f64; 2]
}

impl Wizard for LevelWizard
{
    fn window_title(&self) -> &'static str
    {
        "Level Wizard"
    }

    fn load(_game : &mut GameControl) -> Self where Self: Sized
    {
        LevelWizard
        {
            package_name : String::new(),
            item : String::new(),
            bounds_min : [0.0, 0.0],
            bounds_max : [1280.0, 720.0]
        }
    }

    fn do_ui(&mut self, packages : &mut Packages, game : &mut GameControl, window_builder : imgui::Window<&'static str>, ui : &imgui::Ui) -> WizardState
    {
        let mut state = WizardState::new();

        window_builder.build(ui, ||
        {
            ui.text_wrapped("Levels are arrangements of Spawnables you can load into your scene.");

            ui.spacing();
            ui.spacing();

            ui.input_text("Package", &mut self.package_name).build();
            ui.input_text("Name (.keelvl)", &mut self.item).build();

            ui.spacing();

            imgui::Drag::new("Bounds Min").display_format("%.2f").build_array(ui, &mut self.bounds_min);
            imgui::Drag::new("Bounds Max").display_format("%.2f").build_array(ui, &mut self.bounds_max);

            ui.spacing();
            ui.spacing();

            if ui.button("Create New Level")
            {
                let mut level = Level::new();
                level.set_bounds(BakedRect
                {
                    x1: self.bounds_min[0],
                    y1: self.bounds_min[1],
                    x2: self.bounds_max[0],
                    y2: self.bounds_max[1]
                });

                if self.make_resource(level, &self.package_name, &format!("{}.keelvl", self.item), packages, game)
                {
                    state.open = false;
                }
            }
        });

        state
    }
}

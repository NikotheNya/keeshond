#![windows_subsystem = "windows"]

extern crate keeshond;
extern crate keeshond_derive;
extern crate num;
extern crate confy;
extern crate image;
extern crate palette;
extern crate toodee;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate log;
extern crate sdl2;
extern crate notify;
extern crate walkdir;
extern crate path_slash;
extern crate chrono;
extern crate sha2;
extern crate hex;
extern crate rectangle_pack;
extern crate asefile;

use keeshond::gameloop::{GameInfo, Gameloop};
use keeshond::renderer::ViewportMode;
use crate::editorgui::EditorGui;

mod util;
mod editorgui;
mod document;
mod wizard;
mod import;
mod importer;

////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "keeshond_editor",
        friendly_name : "Keeshond Game Editor",
        base_width : 1280,
        base_height : 720,
        viewport_mode : ViewportMode::OneToOneUnscaled,
        texture_filtering : false,
        allow_system_sleep : true,
        low_energy_app : true,
        log_stats : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);

    let icon_font = imgui::FontSource::TtfData
    {
        data : include_bytes!("../res/fontello.ttf"),
        size_pixels : 16.0,
        config : Some(imgui::FontConfig
        {
            name : Some("fontello.ttf, 16px".to_string()),
            pixel_snap_h : false,
            glyph_extra_spacing : [0.0, 0.0],
            oversample_h : 3,
            oversample_v : 3,
            .. Default::default()
        })
    };

    {
        let mut fonts = gameloop.control_mut().imgui_mut().unwrap().fonts();

        fonts.add_font(&[icon_font]);
    }

    EditorGui::set_scene(gameloop.control_mut());

    gameloop.run();
}

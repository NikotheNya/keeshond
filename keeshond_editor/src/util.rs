pub mod drawable;
pub mod sheet;
pub mod widget;
pub mod zoom;

use num::Float;

const PASCAL_DISALLOWED_CHARS : [char; 33] = [' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')',
    '*', '+', ',', '-', '.', '/', ':', ';', '<', '=',
    '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{',
    '|', '}', '~'];

pub fn keycode_pressed(keycode : sdl2::keyboard::Keycode, repeat : bool, ui : &imgui::Ui) -> bool
{
    if let Some(scancode) = sdl2::keyboard::Scancode::from_keycode(keycode)
    {
        if repeat
        {
            return ui.is_key_index_pressed(scancode as i32);
        }
        else
        {
            return ui.is_key_index_pressed_no_repeat(scancode as i32);
        }
    }

    false
}

pub fn scaled_round(value : f32, scale : f32) -> f32
{
    (value / scale).round() * scale
}

pub fn scaled_floor(value : f32, scale : f32) -> f32
{
    (value / scale).floor() * scale
}

pub struct BoxFitResult<T : Float>
{
    pub x : T,
    pub y : T,
    pub width : T,
    pub height : T,
    pub scale : T
}

pub fn box_fit<T : Float>(base_width : T, base_height : T, parent_width : T, parent_height : T) -> BoxFitResult<T>
{
    let mut result = BoxFitResult::<T>
    {
        x : T::from(0.0).unwrap(),
        y : T::from(0.0).unwrap(),
        width : parent_width,
        height : parent_height,
        scale : T::from(1.0).unwrap(),
    };

    let base_ratio = base_width / base_height;
    let ratio = parent_width / parent_height;

    if ratio > base_ratio
    {
        // Wider than base resolution
        result.scale = parent_height / base_height;
        result.width = (parent_height * base_ratio) as T;
        result.x = ((parent_width) - result.width) / T::from(2.0).unwrap();
    }
    else if ratio < base_ratio
    {
        // Taller than base resolution
        result.scale = parent_width / base_width;
        result.height = (parent_width / base_ratio) as T;
        result.y = ((parent_height) - result.height) / T::from(2.0).unwrap();
    }

    result
}

pub fn color_to_border_color(color : [f32; 4]) -> [f32; 4]
{
    if color[0] + color[1] + color[2] > 1.5
    {
        return [0.0, 0.0, 0.0, 1.0]
    }

    [1.0, 1.0, 1.0, 1.0]
}

pub fn option_bool_to_double_bool(option_bool : Option<bool>) -> (bool, bool)
{
    if let Some(some_bool) = option_bool
    {
        return (true, some_bool);
    }

    (false, false)
}

pub fn double_bool_to_option_bool(is_some : bool, inner_value : bool) -> Option<bool>
{
    if !is_some
    {
        return None;
    }

    Some(inner_value)
}

pub fn name_to_pascal_case(name : &str) -> String
{
    let parts : Vec<String> = name.split(&PASCAL_DISALLOWED_CHARS[..]).map(|part| part.to_string()).collect();
    let mut new_name = String::new();

    for part in &parts
    {
        let mut start_of_word = true;

        for letter in part.chars()
        {
            if start_of_word
            {
                new_name.push(letter.to_ascii_uppercase());
                start_of_word = false;
            }
            else
            {
                new_name.push(letter.to_ascii_lowercase());
            }
        }
    }

    new_name
}

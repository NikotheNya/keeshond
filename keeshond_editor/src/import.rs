use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::fs::File;
use std::io::BufReader;
use std::path::{Component, PathBuf};
use std::sync::mpsc::{channel, Receiver};
use std::time::{Duration, Instant};

use chrono::{DateTime, TimeZone, Utc};
use notify::{DebouncedEvent, RecommendedWatcher, RecursiveMode, Watcher};
use path_slash::{PathBufExt, PathExt};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use walkdir::WalkDir;

use keeshond::gameloop::GameControl;
use keeshond::renderer::Sheet;
use keeshond::datapack::{DataHandle, DataObject};
use keeshond_treats::spawnable::{Representation, Spawnable, SpawnableSet};
use keeshond_treats::visual::{Animation, AnimationCel, AnimationFrame, AnimationTrack, Costume, SliceHandle};

use crate::editorgui::Packages;
use crate::importer::ase::{AsepriteBackgroundImporter, AsepriteSpriteImporter};
use crate::import::sheet::{BackgroundImportData, ImportedImage, SpriteImportData};

pub mod sheet;

const REGISTRY_CONFIG_FILENAME : &str = "Registry.toml";
const LOCAL_REGISTRY_CONFIG_FILENAME : &str = "Registry.local.toml";
const DEFAULT_MAX_TEXTURE_SIZE : u32 = 4096;
const DEFAULT_PADDING : u32 = 1;

const EXCLUDED_FILENAMES : [&str; 8] =
[
    REGISTRY_CONFIG_FILENAME,
    LOCAL_REGISTRY_CONFIG_FILENAME,
    "desktop.ini",
    "Thumbs.db",
    ".DS_Store",
    "ehthumbs.db",
    ".Spotlight-V100",
    ".Trashes"
];

#[derive(Clone)]
pub struct ImportItemPath
{
    pub path : PathBuf,
    pub package : String,
    pub name : String
}

impl ImportItemPath
{
    pub fn from_absolute(pathbuf : PathBuf, basedir : &str) -> ImportItemPath
    {
        let mut relative_path = pathbuf.clone();
        let basepath = PathBuf::from(basedir);

        if let Ok(newpath) = pathbuf.strip_prefix(basepath)
        {
            relative_path = newpath.into();
        }

        let mut package = String::from("common");
        let components : Vec<Component> = relative_path.components().collect();

        if components.len() > 1
        {
            if let Some(Component::Normal(folder)) = components.get(0)
            {
                package = folder.to_string_lossy().to_string();
            }
        }

        let name = match relative_path.strip_prefix(&package)
        {
            Ok(stripped) => { stripped.with_extension("").to_slash_lossy() }
            Err(_) => { relative_path.with_extension("").to_slash_lossy() }
        };

        ImportItemPath
        {
            path : pathbuf.clone(),
            package,
            name
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ImportConfig
{
    #[serde(flatten)]
    entries : BTreeMap<String, String>
}

impl ImportConfig
{
    pub fn new() -> ImportConfig
    {
        ImportConfig
        {
            entries : BTreeMap::new()
        }
    }

    pub fn set_value(&mut self, key : &str, value : &str)
    {
        self.entries.insert(key.to_string(), value.to_string());
    }

    pub fn set_bool(&mut self, key : &str, value : bool)
    {
        let string = match value
        {
            true => { "true" }
            false => { "false" }
        };

        self.set_value(key, string);
    }

    pub fn set_int(&mut self, key : &str, value : i32)
    {
        self.set_value(key, &value.to_string());
    }

    pub fn set_float(&mut self, key : &str, value : f32)
    {
        self.set_value(key, &value.to_string());
    }

    pub fn value(&self, key : &str) -> Option<&String>
    {
        self.entries.get(key)
    }

    pub fn value_or_default(&mut self, key : &str, default : &str) -> &String
    {
        if !self.entries.contains_key(key)
        {
            self.entries.insert(key.to_string(), default.to_string());
        }

        self.entries.get(key).unwrap()
    }

    pub fn value_bool_or_default(&mut self, key : &str, default : bool) -> bool
    {
        let default_string = match default
        {
            true => { "true" }
            false => { "false" }
        };

        self.value_or_default(key, default_string) == "true"
    }

    pub fn value_int_or_default(&mut self, key : &str, default : i32) -> i32
    {
        self.value_or_default(key, &default.to_string()).parse().unwrap_or(default)
    }

    pub fn value_float_or_default(&mut self, key : &str, default : f32) -> f32
    {
        self.value_or_default(key, &default.to_string()).parse().unwrap_or(default)
    }
}



pub enum ImportItem
{
    Sprite(SpriteImportData),
    Background(BackgroundImportData)
}

pub enum ImporterResult
{
    Ok(ImportItem),
    NeedsConfig,
    Err(String)
}

#[derive(Copy, Clone, PartialEq)]
enum RegistryEntryUpdateType
{
    None,
    Ok
    {
        sha2 : [u8; 32]
    },
    Deferred,
    NeedsConfig,
    UpdateTimestamp
}

impl RegistryEntryUpdateType
{
    fn is_ok(&self) -> bool
    {
        if let RegistryEntryUpdateType::Ok { .. } = self
        {
            return true;
        }

        false
    }
}

pub trait Importer
{
    fn name(&self) -> &'static str;
    fn extensions(&self) -> &[&str];
    fn description(&self) -> &'static str;
    fn new() -> Self where Self : Sized;
    fn import(&mut self, path : &ImportItemPath, config : Option<&mut ImportConfig>) -> ImporterResult;
    fn config_ui(&self, path : &ImportItemPath, config : &mut ImportConfig, ui : &imgui::Ui);
}

pub struct ImportGroupItem<T>
{
    pathbuf : PathBuf,
    sha2 : [u8; 32],
    data : T
}

#[derive(Eq, PartialEq)]
pub enum ImportPendingResult
{
    Ok,
    NeedsConfig
}

enum ProcessImportResult
{
    Ok,
    Deferred,
    Err(String)
}

#[derive(Clone, Serialize, Deserialize)]
struct RegistryEntry
{
    #[serde(default)]
    importer : Option<String>,
    #[serde(default)]
    auto_import_on_change : bool,
    #[serde(default)]
    last_sha2 : Option<String>,
    #[serde(default)]
    config : Option<ImportConfig>
}

impl Default for RegistryEntry
{
    fn default() -> Self
    {
        RegistryEntry
        {
            importer : None,
            config : None,
            auto_import_on_change : false,
            last_sha2 : None
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
struct LocalRegistryEntry
{
    last_import : DateTime<Utc>
}

impl Default for LocalRegistryEntry
{
    fn default() -> Self
    {
        LocalRegistryEntry
        {
            last_import : Utc.ymd(1970, 1, 1).and_hms(0, 0, 0)
        }
    }
}

struct AutoImportNotifier
{
    receiver : Receiver<DebouncedEvent>,
    #[allow(dead_code)] // RAII object
    watcher : RecommendedWatcher
}

impl AutoImportNotifier
{
    fn new(import_directory : &str) -> Result<AutoImportNotifier, notify::Error>
    {
        let (sender, receiver) = channel();

        let mut watcher : RecommendedWatcher = Watcher::new(sender, Duration::from_secs(2))?;

        std::fs::create_dir_all(import_directory)?;
        watcher.watch(import_directory, RecursiveMode::Recursive)?;

        info!("Watching AutoImport directory {}", import_directory);

        Ok(AutoImportNotifier
        {
            receiver,
            watcher
        })
    }
}

fn valid_import_file(pathbuf : &PathBuf) -> bool
{
    pathbuf.is_file() && !EXCLUDED_FILENAMES.contains(&&*pathbuf.file_name().unwrap_or_default().to_string_lossy().to_string())
}

fn add_auto_pending(registry : &BTreeMap<String, RegistryEntry>, import_directory : &String,
                    changed_files : &mut BTreeSet<ChangedFileInfo>, changed_file_info : ChangedFileInfo) -> bool
{
    let registry_path = filepath_for_registry(changed_file_info.pathbuf.clone(), import_directory);
    let mut auto_import = true;

    if let Some(entry) = registry.get(&registry_path)
    {
        auto_import = entry.auto_import_on_change;
    }

    if auto_import && valid_import_file(&changed_file_info.pathbuf)
    {
        changed_files.insert(changed_file_info);
        return true;
    }

    false
}

fn filepath_for_registry(pathbuf : PathBuf, basedir : &str) -> String
{
    let basepath = PathBuf::from(basedir);

    match pathbuf.strip_prefix(basepath)
    {
        Ok(newpath) => { newpath.to_slash_lossy() }
        Err(_) => { pathbuf.to_slash_lossy() }
    }
}

#[derive(Clone)]
struct CurrentItem
{
    name : String,
    path : PathBuf
}

struct ImportProcessingState
{
    deferred_sheets : BTreeMap<(String, String), Vec<ImportGroupItem<SpriteImportData>>>
}

impl ImportProcessingState
{
    fn new() -> ImportProcessingState
    {
        ImportProcessingState
        {
            deferred_sheets : BTreeMap::new()
        }
    }
}

enum FileHashInfo
{
    Modified
    {
        sha2 : [u8; 32]
    },
    Unmodified
}

#[derive(Ord, PartialOrd, Eq, PartialEq)]
struct ChangedFileInfo
{
    pathbuf : PathBuf,
    ignore_same_hash : bool
}

pub struct AutoImport
{
    import_directory : String,
    max_texture_size : u32,
    notifier : Option<AutoImportNotifier>,
    changed_files : BTreeSet<ChangedFileInfo>,
    needs_config_files : BTreeSet<PathBuf>,
    configured_files : BTreeSet<PathBuf>,
    configured_file_names : Vec<String>,
    registry : BTreeMap<String, RegistryEntry>,
    local_registry : BTreeMap<String, LocalRegistryEntry>,
    importers : HashMap<String, Box<dyn Importer>>,
    importer_extensions : HashMap<String, Vec<String>>,
    ui_current_item : Option<CurrentItem>
}

impl AutoImport
{
    pub fn new() -> AutoImport
    {
        let mut auto_import = AutoImport
        {
            import_directory : "data_src/".to_string(),
            max_texture_size : DEFAULT_MAX_TEXTURE_SIZE,
            notifier : None,
            changed_files : BTreeSet::new(),
            needs_config_files : BTreeSet::new(),
            configured_files : BTreeSet::new(),
            configured_file_names : Vec::new(),
            registry : BTreeMap::new(),
            local_registry : BTreeMap::new(),
            importers : HashMap::new(),
            importer_extensions : HashMap::new(),
            ui_current_item : None
        };

        let mut importers : Vec<Box<dyn Importer>> = vec!
        [
            Box::new(AsepriteSpriteImporter::new()),
            Box::new(AsepriteBackgroundImporter::new())
        ];

        for importer in importers.drain(..)
        {
            let name = importer.name().to_string();

            for extension in importer.extensions()
            {
                auto_import.importer_extensions.entry(extension.to_string()).or_default().push(name.clone());
            }

            auto_import.importers.insert(name, importer);
        }

        auto_import
    }

    #[allow(dead_code)]
    pub fn directory(&self) -> &String
    {
        &self.import_directory
    }

    pub fn set_directory(&mut self, directory : &str) -> Result<(), notify::Error>
    {
        if self.import_directory != directory
        {
            self.import_directory = directory.to_string();
            self.load_registry();
            return self.set_auto_refresh_active(self.auto_refresh_active());
        }

        Ok(())
    }

    fn load_registry(&mut self)
    {
        self.registry = confy::load_path(format!("{}/{}", self.import_directory, REGISTRY_CONFIG_FILENAME)).unwrap_or(BTreeMap::new());
        self.local_registry = confy::load_path(format!("{}/{}", self.import_directory, LOCAL_REGISTRY_CONFIG_FILENAME)).unwrap_or(BTreeMap::new());
    }

    fn save_registry(&self)
    {
        if let Err(error) = confy::store_path(format!("{}/{}", self.import_directory, REGISTRY_CONFIG_FILENAME), &self.registry)
        {
            error!("Error while saving AutoImport registry: {}", error);
        }

        if let Err(error) = confy::store_path(format!("{}/{}", self.import_directory, LOCAL_REGISTRY_CONFIG_FILENAME), &self.local_registry)
        {
            error!("Error while saving local AutoImport registry: {}", error);
        }
    }

    pub fn auto_refresh_active(&self) -> bool
    {
        self.notifier.is_some()
    }

    pub fn set_auto_refresh_active(&mut self, active : bool) -> Result<(), notify::Error>
    {
        if active && self.notifier.is_none()
        {
            self.notifier = Some(AutoImportNotifier::new(&self.import_directory)?);
        }
        else if !active
        {
            self.notifier = None;
        }

        Ok(())
    }

    pub(crate) fn receive_changes(&mut self)
    {
        if let Some(notifier) = &mut self.notifier
        {
            loop
            {
                match notifier.receiver.try_recv()
                {
                    Ok(event) =>
                    {
                        match event
                        {
                            DebouncedEvent::Create(path) => { add_auto_pending(&self.registry, &self.import_directory, &mut self.changed_files,
                                                                              ChangedFileInfo { pathbuf : path, ignore_same_hash : false }); }
                            DebouncedEvent::Write(path) => { add_auto_pending(&self.registry, &self.import_directory, &mut self.changed_files,
                                                                              ChangedFileInfo { pathbuf : path, ignore_same_hash : false }); }
                            DebouncedEvent::Rename(_, path) => { add_auto_pending(&self.registry, &self.import_directory, &mut self.changed_files,
                                                                              ChangedFileInfo { pathbuf : path, ignore_same_hash : false }); }
                            _ => {}
                        }
                    },
                    Err(_) => { break; }
                }
            }
        }
    }

    pub fn has_pending_items(&self) -> bool
    {
        !self.changed_files.is_empty()
    }

    fn apply_bg_sheet_image(bg_info : &mut BackgroundImportData, new_sheet : &mut Sheet)
    {
        match &bg_info.image
        {
            ImportedImage::Rgb(image) =>
            {
                new_sheet.set_rgba_data_with_size(image.to_vec(), image.width(), image.height());
            }
            ImportedImage::Indexed(image) =>
            {
                new_sheet.set_indexed_data_with_size(image.to_vec(), image.width(), image.height(), bg_info.palette.as_ref().unwrap_or(&Vec::new()).clone());
            }
        }
    }

    fn check_sha2(entry : &RegistryEntry, changed_file : &ChangedFileInfo) -> std::io::Result<FileHashInfo>
    {
        let file = File::open(&changed_file.pathbuf)?;
        let mut reader = BufReader::new(file);

        let mut hasher = Sha256::default();
        std::io::copy(&mut reader, &mut hasher)?;
        let sha2 = hasher.finalize().into();

        if changed_file.ignore_same_hash || hex::encode(sha2) != entry.last_sha2.clone().unwrap_or_default()
        {
            return Ok(FileHashInfo::Modified { sha2 });
        }

        Ok(FileHashInfo::Unmodified)
    }

    fn process_import(item : ImportItem, pathbuf : PathBuf, sha2 : [u8; 32], state : &mut ImportProcessingState,
                       packages : &mut Packages, game : &mut GameControl) -> ProcessImportResult
    {
        match item
        {
            ImportItem::Sprite(mut sprite_info) =>
            {
                for image in &mut sprite_info.images
                {
                    let (offset_x, offset_y) = image.image.optimize(DEFAULT_PADDING);

                    image.origin_x += offset_x as f32;
                    image.origin_y += offset_y as f32;
                }

                sprite_info.dedup();

                let entry = state.deferred_sheets
                    .entry((sprite_info.dest_sheet_package.clone(), sprite_info.dest_sheet.clone()))
                    .or_default();
                entry.push(ImportGroupItem::<SpriteImportData>
                {
                    pathbuf,
                    sha2,
                    data : sprite_info
                });

                return ProcessImportResult::Deferred;
            }
            ImportItem::Background(mut bg_info) =>
            {
                let mut new_sheet = AutoImport::read_existing_resource(packages, game, &bg_info.dest_package,
                                                                       &bg_info.dest_sheet).unwrap_or(Sheet::new(0, 0, None));

                if let Some(tilegen) = &mut bg_info.tilegen.as_mut()
                {
                    let mut slices;

                    match &bg_info.image
                    {
                        ImportedImage::Rgb(image) =>
                        {
                            let (new_texture, new_slices) = crate::import::sheet::create_tileset_texture_and_slices(&image, tilegen);

                            bg_info.image = ImportedImage::Rgb(new_texture);
                            slices = new_slices;
                        }
                        ImportedImage::Indexed(image) =>
                        {
                            let (new_texture, new_slices) = crate::import::sheet::create_tileset_texture_and_slices(&image, tilegen);

                            bg_info.image = ImportedImage::Indexed(new_texture);
                            slices = new_slices;
                        }
                    }

                    new_sheet.clear_slices();

                    for slice in slices.drain(..)
                    {
                        new_sheet.add_slice(slice);
                    }
                }

                AutoImport::apply_bg_sheet_image(&mut bg_info, &mut new_sheet);

                if let Err(error) = AutoImport::write_imported_resource(packages, game,
                                                                        &bg_info.dest_package,
                                                                        &bg_info.dest_sheet,
                                                                        &mut new_sheet)
                {
                    return ProcessImportResult::Err(error);
                }
            }
        }

        ProcessImportResult::Ok
    }

    fn read_existing_resource<T : DataObject + Clone + 'static>(packages : &mut Packages, game : &mut GameControl, package : &str, name : &str) -> Option<T>
    {
        let store = game.res().store::<T>();

        if let Ok(existing) = store.load_direct(package, name, packages.source_id())
        {
            return Some(existing);
        }

        None
    }

    fn write_imported_resource<T : DataObject + 'static>(packages : &mut Packages,
                                               game : &mut GameControl,
                                               package : &str, name : &str,
                                               new_resource : &mut T) -> Result<(), String>
    {
        let full_name = format!("{}/{}", T::folder_name(), name);

        {
            let source_manager = game.source_manager();
            let mut source_borrow = source_manager.borrow_mut();
            let source = source_borrow.source(packages.source_id()).expect("Source not available to import into!");

            // TODO fix so we don't have to manually prepend type folder name
            if let Err(error) = new_resource.write(package, &full_name, source)
            {
                return Err(format!("Could not write imported resource: {}", error.to_string()));
            }
        }

        Ok(())
    }

    fn update_registry_entry(&mut self, pathbuf : &PathBuf, update_type : RegistryEntryUpdateType)
    {
        let registry_path = filepath_for_registry(pathbuf.clone(), &self.import_directory);

        match update_type
        {
            RegistryEntryUpdateType::Ok { sha2 } =>
            {
                let mut entry = self.registry.entry(registry_path.clone()).or_insert(RegistryEntry::default());
                let mut local_entry = self.local_registry.entry(registry_path.clone()).or_insert(LocalRegistryEntry::default());

                entry.last_sha2 = Some(hex::encode(sha2));
                entry.auto_import_on_change = true;
                local_entry.last_import = Utc::now();

                self.configured_files.insert(pathbuf.clone());
                self.needs_config_files.remove(pathbuf);
                self.configured_file_names.clear();
            }
            RegistryEntryUpdateType::UpdateTimestamp =>
            {
                let mut local_entry = self.local_registry.entry(registry_path.clone()).or_insert(LocalRegistryEntry::default());

                local_entry.last_import = Utc::now();
            }
            RegistryEntryUpdateType::NeedsConfig =>
            {
                self.needs_config_files.insert(pathbuf.clone());
                self.configured_files.remove(pathbuf);
                self.configured_file_names.clear();
            }
            _ => {}
        }
    }

    fn import_item(&mut self, changed_file : &ChangedFileInfo, state : &mut ImportProcessingState, packages : &mut Packages, game : &mut GameControl)
    {
        let mut update_type = RegistryEntryUpdateType::None;
        let registry_path = filepath_for_registry(changed_file.pathbuf.clone(), &self.import_directory);
        let path_info = ImportItemPath::from_absolute(changed_file.pathbuf.clone(), &self.import_directory);

        info!("Importing item \"{}\"", registry_path);

        let import_start = Instant::now();

        let entry = self.registry.entry(registry_path.clone()).or_insert(RegistryEntry::default());

        if let Some(importer_name) = &entry.importer
        {
            if let Some(importer) = self.importers.get_mut(importer_name)
            {
                match AutoImport::check_sha2(entry, changed_file)
                {
                    Ok(FileHashInfo::Modified { sha2 }) =>
                    {
                        match importer.import(&path_info, entry.config.as_mut())
                        {
                            ImporterResult::Ok(item) =>
                            {
                                match AutoImport::process_import(item, changed_file.pathbuf.clone(), sha2, state, packages, game)
                                {
                                    ProcessImportResult::Ok => { update_type = RegistryEntryUpdateType::Ok { sha2 }; }
                                    ProcessImportResult::Deferred => { update_type = RegistryEntryUpdateType::Deferred; }
                                    ProcessImportResult::Err(error) =>
                                    {
                                        error!("Error importing \"{}\": {}", registry_path, error);
                                    }
                                }
                            }
                            ImporterResult::NeedsConfig =>
                            {
                                info!("{} Importer needs additional configuration for \"{}\"", importer_name, registry_path);
                                update_type = RegistryEntryUpdateType::NeedsConfig;
                            }
                            ImporterResult::Err(error) =>
                            {
                                error!("{} Importer: error importing \"{}\": {}", importer_name, registry_path, error);
                            }
                        }
                    }
                    Ok(FileHashInfo::Unmodified) =>
                    {
                        info!("File \"{}\" is unchanged, not importing", registry_path);
                        update_type = RegistryEntryUpdateType::UpdateTimestamp;
                    },
                    Err(error) =>
                    {
                        error!("Unable to read hash for \"{}\": {}", registry_path, error);
                    }
                }
            }
            else
            {
                info!("{} Importer missing for \"{}\"", importer_name, registry_path);
            }
        }
        else
        {
            info!("Additional configuration needed for \"{}\"", registry_path);
            update_type = RegistryEntryUpdateType::NeedsConfig;
        }

        self.update_registry_entry(&changed_file.pathbuf, update_type);

        if update_type.is_ok() || update_type == RegistryEntryUpdateType::Deferred
        {
            let import_time = (Instant::now() - import_start).as_millis();

            info!("Processed \"{}\" in {} ms", registry_path, import_time);
        }
    }

    fn do_import_shared_sheet(&mut self, package : &str, name : &str, group : &Vec<ImportGroupItem<SpriteImportData>>,
                              packages : &mut Packages, game : &mut GameControl) -> Result<(), String>
    {
        let mut new_sheet = AutoImport::read_existing_resource(packages, game, package, name).unwrap_or(Sheet::new(0, 0, None));

        sheet::update_sheet_with_import_group(&mut new_sheet, group, self.max_texture_size, &self.import_directory)?;

        for item in group
        {
            let path_info = ImportItemPath::from_absolute(item.pathbuf.clone(), &self.import_directory);
            let prefix = format!("{}/{}//", path_info.package, path_info.name);
            let mut new_costume = AutoImport::read_existing_resource(packages, game,
                                                                     &item.data.dest_costume_package,
                                                                     &item.data.dest_costume).unwrap_or(Costume::new());
            let mut new_spawnables = AutoImport::read_existing_resource(packages, game,
                                                                        &item.data.dest_spawnable_package,
                                                                        &item.data.dest_spawnable).unwrap_or(SpawnableSet::new());
            let mut default_track_name = item.data.default_track_name.clone();

            if default_track_name.is_empty()
            {
                default_track_name = String::from("default");
            }

            for import_animation in &item.data.animations
            {
                let mut animation = Animation::new();

                for frame in &import_animation.frames
                {
                    animation.add_frame(AnimationFrame
                    {
                        delay : frame.delay
                    });

                    for track in &frame.tracks
                    {
                        let mut track_name = track.track_name.clone();

                        if track_name.is_empty()
                        {
                            track_name = default_track_name.clone();
                        }

                        let costume_track_id = match new_costume.track_id(&track_name)
                        {
                            None => { new_costume.add_track(&track_name) }
                            Some(id) => { id }
                        };

                        let row = match animation.costume_track_to_row(costume_track_id as u16)
                        {
                            None => { animation.add_track(AnimationTrack::new(costume_track_id as u16)) }
                            Some(row) => { row }
                        };

                        animation.set_cel(animation.frame_count() - 1, row, AnimationCel
                        {
                            sheet : DataHandle::with_path(&package, &name),
                            slice : SliceHandle::new(&format!("{}{}", prefix, track.index)),
                            x : 0.0,
                            y : 0.0
                        });
                    }
                }

                animation.set_looping(import_animation.looping);

                match new_costume.animation_id(&import_animation.animation_name)
                {
                    None =>
                    {
                        new_costume.add_animation(animation);
                        new_costume.rename_animation(new_costume.animation_count() - 1, &import_animation.animation_name);
                    }
                    Some(id) => { new_costume.set_animation(id, animation); }
                }
            }

            if new_spawnables.spawnable_by_name(&item.data.spawn_type_name).is_none()
            {
                let mut spawnable = Spawnable::new();
                spawnable.set_representation(Representation::Actor
                {
                    costume : DataHandle::with_path(&item.data.dest_costume_package,
                                                    &item.data.dest_costume)
                });

                new_spawnables.add_spawnable(spawnable);
                new_spawnables.rename_spawnable(new_spawnables.spawnable_count() - 1, &item.data.spawn_type_name);
            }

            if !item.data.dest_costume_package.is_empty() && !item.data.dest_costume.is_empty()
            {
                AutoImport::write_imported_resource(packages, game, &item.data.dest_costume_package,
                                                    &item.data.dest_costume, &mut new_costume)?;

                if !item.data.dest_spawnable_package.is_empty() && !item.data.dest_spawnable.is_empty()
                {
                    AutoImport::write_imported_resource(packages, game, &item.data.dest_spawnable_package,
                                                        &item.data.dest_spawnable, &mut new_spawnables)?;
                }
            }
        }

        AutoImport::write_imported_resource(packages, game, package, name, &mut new_sheet)?;

        Ok(())
    }

    fn import_shared_sheet(&mut self, package : &str, name : &str, group : &Vec<ImportGroupItem<SpriteImportData>>,
                           packages : &mut Packages, game : &mut GameControl)
    {
        let mut success = false;
        let mut update_types = Vec::new();

        info!("Handling shared sheet for \"{}/{}\" with {} new or changed source item(s)", package, name, group.len());

        let import_start = Instant::now();

        match self.do_import_shared_sheet(package, name, group, packages, game)
        {
            Ok(_) =>
            {
                for item in group
                {
                    update_types.push(RegistryEntryUpdateType::Ok
                    {
                        sha2 : item.sha2.clone()
                    });
                }

                success = true;
            }
            Err(error) =>
            {
                for _ in 0..group.len()
                {
                    update_types.push(RegistryEntryUpdateType::None);
                }

                error!("Could not update shared sheet \"{}/{}\": {}", package, name, error);
            }
        }

        for i in 0..group.len()
        {
            self.update_registry_entry(&group[i].pathbuf, update_types[i]);
        }

        if success
        {
            let import_time = (Instant::now() - import_start).as_millis();

            info!("Updated shared sheet \"{}/{}\" in {} ms", package, name, import_time);
        }
    }

    pub fn import_pending(&mut self, packages : &mut Packages, game : &mut GameControl) -> ImportPendingResult
    {
        let mut changed_files = BTreeSet::new();

        std::mem::swap(&mut changed_files, &mut self.changed_files);

        let mut state = ImportProcessingState::new();

        for changed_file in &changed_files
        {
            self.import_item(changed_file, &mut state, packages, game);
        }

        for ((package, path), group) in &state.deferred_sheets
        {
            self.import_shared_sheet(package, path, group, packages, game);
        }

        self.save_registry();

        packages.reload();

        if !self.needs_config_files.is_empty()
        {
            return ImportPendingResult::NeedsConfig;
        }

        ImportPendingResult::Ok
    }

    pub fn mark_all_pending(&mut self, force_old_files : bool)
    {
        let walkdir = WalkDir::new(&self.import_directory).min_depth(1).into_iter();

        for dir_entry in walkdir
        {
            if let Ok(entry) = dir_entry
            {
                let pathbuf = entry.clone().into_path();
                let registry_path = filepath_for_registry(entry.clone().into_path(), &self.import_directory);

                if !valid_import_file(&pathbuf)
                {
                    continue;
                }

                if let Ok(metadata) = entry.metadata()
                {
                    if let Ok(modified) = metadata.modified()
                    {
                        let import_time : DateTime<Utc> = DateTime::from(modified);
                        let entry = self.registry.entry(registry_path.clone()).or_insert(RegistryEntry::default());
                        let local_entry = self.local_registry.entry(registry_path).or_insert(LocalRegistryEntry::default());

                        if force_old_files || entry.auto_import_on_change && import_time > local_entry.last_import
                        {
                            self.changed_files.insert(ChangedFileInfo { pathbuf, ignore_same_hash : force_old_files });
                        }
                        else if entry.config.is_some()
                        {
                            self.configured_files.insert(pathbuf);
                        }
                        else
                        {
                            self.needs_config_files.insert(pathbuf);
                        }
                    }
                }
            }
        }

        self.configured_file_names.clear();
    }

    fn ui_config_list(&mut self, ui : &imgui::Ui)
    {
        let main_style = ui.clone_style();
        let style_list = ui.push_style_var(imgui::StyleVar::WindowPadding([3.0, 2.0]));
        let colors_list = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);

        imgui::ChildWindow::new("item_list").border(true).draw_background(true).build(ui, ||
        {
            if imgui::Selectable::new("General Information###///info").selected(self.ui_current_item.is_none()).build(ui)
            {
                self.ui_current_item = None;
            }

            ui.spacing();

            crate::util::widget::window_placeholder_text(&format!("Items needing configuration ({})", self.needs_config_files.len()), ui);

            ui.spacing();

            for pathbuf in &self.needs_config_files
            {
                let name = filepath_for_registry(pathbuf.clone(), &self.import_directory);
                let mut selected = false;

                if let Some(item) = &self.ui_current_item
                {
                    selected = item.name == name;
                }

                if imgui::Selectable::new(&name).selected(selected).build(ui)
                {
                    self.ui_current_item = Some(CurrentItem
                    {
                        name,
                        path : pathbuf.clone()
                    });
                }
            }

            crate::util::widget::window_placeholder_text(&format!("Configured items ({})", self.configured_files.len()), ui);

            ui.spacing();

            if self.configured_file_names.is_empty()
            {
                for pathbuf in &self.configured_files
                {
                    let name = filepath_for_registry(pathbuf.clone(), &self.import_directory);

                    self.configured_file_names.push(name);
                }
            }

            let mut index = 0;

            for pathbuf in &self.configured_files
            {
                let name = self.configured_file_names.get(index).cloned().unwrap_or(String::new());
                index += 1;
                let mut selected = false;

                if let Some(item) = &self.ui_current_item
                {
                    selected = item.name == name;
                }

                if imgui::Selectable::new(&name).selected(selected).build(ui)
                {
                    self.ui_current_item = Some(CurrentItem
                    {
                        name,
                        path : pathbuf.clone()
                    });
                }
            }
        });

        colors_list.pop();
        style_list.pop();
    }

    fn ui_info(&mut self, ui : &imgui::Ui)
    {
        ui.text_wrapped("Keeshond AutoImport lets you easily make assets \
                                in external applications and get them in-engine even \
                                as you make changes to the source files.");

        if !self.needs_config_files.is_empty()
        {
            ui.spacing();
            ui.spacing();

            ui.text_wrapped("Some items currently need configuration so that \
                                    Keeshond Editor knows how to import them. Use the list \
                                    to the left to select a file to configure and import.");
        }

        ui.spacing();
        ui.spacing();
        crate::util::widget::column_separator(ui);
        ui.spacing();
        ui.spacing();

        ui.text_wrapped(format!("AutoImport directory: {}", self.import_directory));

        ui.spacing();
        ui.spacing();

        ui.text_wrapped("Available importers:");

        ui.spacing();

        for importer in self.importers.values()
        {
            let mut extensions = String::new();

            for extension in importer.extensions()
            {
                if !extensions.is_empty()
                {
                    extensions.push_str(", ");
                }

                extensions.push_str(&format!(".{}", extension));
            }

            ui.bullet();
            ui.text_wrapped(format!("{} ({})", importer.name(), extensions));

            ui.indent_by(40.0);
            ui.text_wrapped(importer.description());
            ui.unindent_by(40.0);
        }

        ui.columns(1, "", false);
    }

    fn ui_pick_importer(&self, path : &PathBuf, ui : &imgui::Ui) -> Option<String>
    {
        let mut choice = None;
        let mut any_importers = false;

        ui.text("Select an importer for this file:");

        ui.spacing();
        ui.spacing();

        if let Some(os_extension) = path.extension()
        {
            let extension = os_extension.to_string_lossy().to_string();

            if let Some(importers) = self.importer_extensions.get(&extension)
            {
                for name in importers
                {
                    if let Some(importer) = self.importers.get(name)
                    {
                        if ui.button(name)
                        {
                            choice = Some(name.clone());
                        }

                        ui.text_wrapped(importer.description());
                        ui.spacing();
                        ui.spacing();

                        any_importers = true;
                    }
                }
            }
        }

        if !any_importers
        {
            crate::util::widget::window_placeholder_text("(No importers available for this filetype)", ui);
        }

        choice
    }

    fn ui_config_importer(&mut self, path : &PathBuf, importer_name : &String, entry : &mut RegistryEntry, ui : &imgui::Ui)
    {
        let path_info = ImportItemPath::from_absolute(path.clone(), &self.import_directory);

        ui.text(format!("Using {} Importer", importer_name));
        ui.same_line_with_spacing(0.0, 8.0);
        if ui.small_button("Change###Change Importer Button")
        {
            ui.open_popup("Change Importer");
        }

        ui.spacing();
        ui.spacing();

        match self.importers.get(importer_name)
        {
            None =>
            {
                crate::util::widget::window_placeholder_text("(This importer is not available)", ui);
            }
            Some(importer) =>
            {
                if entry.config.is_none()
                {
                    entry.config = Some(ImportConfig::new());
                }

                crate::util::widget::column_separator(ui);

                ui.spacing();
                ui.spacing();

                importer.config_ui(&path_info, entry.config.as_mut().unwrap(), ui);

                ui.spacing();
                ui.spacing();

                if ui.button("Import Now")
                {
                    if !add_auto_pending(&self.registry, &self.import_directory, &mut self.changed_files,
                                         ChangedFileInfo { pathbuf : path.clone(), ignore_same_hash : true })
                    {
                        let registry_path = filepath_for_registry(path.clone(), &self.import_directory);
                        error!("\"{}\" is not a valid import file.", registry_path);
                    }
                }
            }
        }

        ui.popup_modal("Change Importer").resizable(false).always_auto_resize(true).save_settings(false).build(ui, ||
        {
            ui.text("Switching to a different importer will erase the configuration for this file. Proceed?");

            ui.spacing();
            ui.spacing();

            if ui.button("Yes, change importer")
            {
                entry.importer = None;
                entry.config = None;

                ui.close_current_popup();
            }
            ui.same_line();
            if ui.button("No, keep config and importer")
            {
                ui.close_current_popup();
            }
        });
    }

    pub fn do_ui(&mut self, ui : &imgui::Ui, open : &mut bool)
    {
        let size = ui.io().display_size;

        if let Some(import_window) = imgui::Window::new("AutoImport")
            .position([size[0] / 2.0, size[1] / 1.98], imgui::Condition::Appearing)
            .position_pivot([0.5, 0.50]).size([850.0, 600.0], imgui::Condition::Appearing)
            .collapsible(false).opened(open).save_settings(false).begin(ui)
        {
            ui.columns(2, "##auto_import_columns", false);

            self.ui_config_list(ui);

            ui.set_current_column_width(300.0);
            ui.next_column();

            match self.ui_current_item.clone()
            {
                None =>
                {
                    crate::util::widget::window_placeholder_text("General Information", ui);

                    ui.spacing();
                    ui.spacing();

                    self.ui_info(ui);
                }
                Some(item) =>
                {
                    crate::util::widget::window_placeholder_text(&item.name, ui);

                    ui.spacing();
                    ui.spacing();

                    let mut registry = BTreeMap::new();

                    std::mem::swap(&mut self.registry, &mut registry);

                    if let Some(entry) = registry.get_mut(&item.name)
                    {
                        match entry.importer.clone()
                        {
                            None =>
                            {
                                entry.importer = self.ui_pick_importer(&item.path, ui);
                            }
                            Some(importer_name) =>
                            {
                                self.ui_config_importer(&item.path, &importer_name, entry, ui);
                            }
                        }
                    }

                    std::mem::swap(&mut self.registry, &mut registry);
                }
            }

            import_window.end();
        }
    }
}

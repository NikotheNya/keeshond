#![feature(test)]

extern crate test;
extern crate keeshond;

use test::Bencher;
use rand::seq::SliceRandom;

use keeshond::gameloop::{GameInfo, GameControl};
use keeshond::scene::{BaseScene, SimpleScene, NullSceneType, Entity, Component, ThinkerSystem, ThinkerArgs, EntityComponent, ComponentRef, ComponentRefMut};

const NUM_ITER : usize = 10_000;

struct Position
{
    x : f64,
    y : f64
}

impl Position
{
    pub fn new() -> Position
    {
        Position
        {
            x : 0.0,
            y : 0.0
        }
    }
}

impl Component for Position
{
    fn maintain_ordering() -> bool where Self : Sized { false }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

impl<'a> ComponentRef<'a> for Position
{
    type StoreRef = &'a Position;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for Position
{
    type StoreRefMut = &'a mut Position;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

struct Velocity
{
    vel_x : f64,
    vel_y : f64
}

impl Velocity
{
    pub fn new() -> Velocity
    {
        Velocity
        {
            vel_x : 0.1,
            vel_y : 0.2
        }
    }
}

impl Component for Velocity
{
    fn maintain_ordering() -> bool where Self : Sized { false }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

impl<'a> ComponentRef<'a> for Velocity
{
    type StoreRef = &'a Velocity;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for Velocity
{
    type StoreRefMut = &'a mut Velocity;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

struct PositionVelocity
{
    x : f64,
    y : f64,
    vel_x : f64,
    vel_y : f64
}

impl PositionVelocity
{
    pub fn new() -> PositionVelocity
    {
        PositionVelocity
        {
            x : 0.0,
            y : 0.0,
            vel_x : 0.1,
            vel_y : 0.2
        }
    }
}

impl Component for PositionVelocity
{
    fn maintain_ordering() -> bool where Self : Sized { false }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

impl<'a> ComponentRef<'a> for PositionVelocity
{
    type StoreRef = &'a PositionVelocity;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for PositionVelocity
{
    type StoreRefMut = &'a mut PositionVelocity;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

struct PositionOrdered
{
    x : f64,
    y : f64
}


impl PositionOrdered
{
    pub fn new() -> PositionOrdered
    {
        PositionOrdered
        {
            x : 0.0,
            y : 0.0
        }
    }
}

impl Component for PositionOrdered
{
    fn maintain_ordering() -> bool where Self : Sized { true }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

impl<'a> ComponentRef<'a> for PositionOrdered
{
    type StoreRef = &'a PositionOrdered;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for PositionOrdered
{
    type StoreRefMut = &'a mut PositionOrdered;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

struct VelocityOrdered
{
    vel_x : f64,
    vel_y : f64
}

impl VelocityOrdered
{
    pub fn new() -> VelocityOrdered
    {
        VelocityOrdered
        {
            vel_x : 0.1,
            vel_y : 0.2
        }
    }
}

impl Component for VelocityOrdered
{
    fn maintain_ordering() -> bool where Self : Sized { true }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

impl<'a> ComponentRef<'a> for VelocityOrdered
{
    type StoreRef = &'a VelocityOrdered;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for VelocityOrdered
{
    type StoreRefMut = &'a mut VelocityOrdered;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}


////////////////////////////////////////


struct PosVelThinker {}

impl ThinkerSystem<NullSceneType> for PosVelThinker
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let mut pos_store = args.components.store_mut::<Position>();
        let vel_store = args.components.store_mut::<Velocity>();
        
        let iter = pos_store.iter_mut();
    
        for (entity, pos) in iter
        {
            if let Some(vel) = vel_store.get_entity(&entity)
            {
                pos.x += vel.vel_x;
                pos.y += vel.vel_y;
            }
        }
    }
}

fn make_scene_multi_component() -> SimpleScene
{
    let mut my_scene = SimpleScene::new();
    let mut positions = Vec::with_capacity(NUM_ITER);
    let mut velocities = Vec::with_capacity(NUM_ITER);

    for _ in 0..NUM_ITER
    {
        let entity = my_scene.add_entity();
        positions.push(EntityComponent
        {
            entity : entity.clone(),
            component : Position::new()
        });
        velocities.push(EntityComponent
        {
            entity,
            component : Velocity::new()
        });
    }
    
    my_scene.add_multiple_components(positions);
    my_scene.add_multiple_components(velocities);
    
    my_scene.add_thinker_system(Box::new(PosVelThinker {}));
    
    my_scene
}

struct PosVelSingleThinker {}

impl ThinkerSystem<NullSceneType> for PosVelSingleThinker
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let mut posvel_store = args.components.store_mut::<PositionVelocity>();
        
        let iter = posvel_store.iter_mut();
    
        for (_, component) in iter
        {
            component.x += component.vel_x;
            component.y += component.vel_y;
        }
    }
}

fn make_scene_single_component() -> SimpleScene
{
    let mut my_scene = SimpleScene::new();
    let mut positions = Vec::with_capacity(NUM_ITER);

    for _ in 0..NUM_ITER
    {
        let entity = my_scene.add_entity();
        positions.push(EntityComponent
        {
            entity : entity.clone(),
            component : PositionVelocity::new()
        });
    }

    my_scene.add_multiple_components(positions);
    my_scene.add_thinker_system(Box::new(PosVelSingleThinker {}));
    
    my_scene
}

struct PosVelOrderedThinker {}

impl ThinkerSystem<NullSceneType> for PosVelOrderedThinker
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let mut pos_store = args.components.store_mut::<PositionOrdered>();
        let vel_store = args.components.store_mut::<VelocityOrdered>();
        
        let iter = pos_store.iter_mut();
    
        for (entity, pos) in iter
        {
            if let Some(vel) = vel_store.get_entity(&entity)
            {
                pos.x += vel.vel_x;
                pos.y += vel.vel_y;
            }
        }
    }
}

fn make_scene_removal_ordered() -> SimpleScene
{
    let mut my_scene = SimpleScene::new();
    let mut positions = Vec::with_capacity(NUM_ITER);
    let mut velocities = Vec::with_capacity(NUM_ITER);

    for _ in 0..NUM_ITER
    {
        let entity = my_scene.add_entity();
        positions.push(EntityComponent
        {
            entity : entity.clone(),
            component : PositionOrdered::new()
        });
        velocities.push(EntityComponent
        {
            entity,
            component : VelocityOrdered::new()
        });
    }

    my_scene.add_multiple_components(positions);
    my_scene.add_multiple_components(velocities);

    my_scene.add_thinker_system(Box::new(PosVelOrderedThinker {}));
    
    my_scene
}

struct DeferredBuildMultiThinker {}

impl ThinkerSystem<NullSceneType> for DeferredBuildMultiThinker
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        for _ in 0..NUM_ITER
        {
            let entity = args.scene.add_entity_later();
            args.scene.add_component_later(&entity, Position::new());
            args.scene.add_component_later(&entity, Velocity::new());
        }
    }
}

struct DeferredBuildSingleThinker {}

impl ThinkerSystem<NullSceneType> for DeferredBuildSingleThinker
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        for _ in 0..NUM_ITER
        {
            let entity = args.scene.add_entity_later();
            args.scene.add_component_later(&entity, PositionVelocity::new());
        }
    }
}


////////////////////////////////////////


#[bench]
fn bench_build_multi_component(bencher : &mut Bencher)
{
    bencher.iter(|| make_scene_multi_component());
}

#[bench]
fn bench_update_multi_component(bencher : &mut Bencher)
{
    let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
    let mut scene = make_scene_multi_component();
    
    bencher.iter(|| scene.think(&mut dummy));
}

#[bench]
fn bench_build_single_component(bencher : &mut Bencher)
{
    bencher.iter(|| make_scene_single_component());
}

#[bench]
fn bench_update_single_component(bencher : &mut Bencher)
{
    let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
    let mut scene = make_scene_single_component();
    
    bencher.iter(|| scene.think(&mut dummy));
}

#[bench]
fn bench_update_empty_slots(bencher : &mut Bencher)
{
    let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
    let mut scene = make_scene_single_component();
    
    for i in 1..NUM_ITER - 2
    {
        assert!(scene.remove_entity(&Entity::new(i, 1)));
    }
    
    bencher.iter(|| scene.think(&mut dummy));
}

#[bench]
fn bench_build_no_component(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut my_scene = Box::new(SimpleScene::new());
    
        for _ in 0..NUM_ITER
        {
            my_scene.add_entity();
        }
    });
}

#[bench]
fn bench_build_rebuild_2_cycles(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut my_scene = Box::new(SimpleScene::new());
        
        my_scene.add_thinker_system(Box::new(PosVelThinker {}));
        
        for gen in 1..3
        {
            let mut positions = Vec::with_capacity(NUM_ITER);
            let mut velocities = Vec::with_capacity(NUM_ITER);

            for _ in 0..NUM_ITER
            {
                let entity = my_scene.add_entity();
                positions.push(EntityComponent
                {
                    entity : entity.clone(),
                    component : Position::new()
                });
                velocities.push(EntityComponent
                {
                    entity,
                    component : Velocity::new()
                });
            }

            my_scene.add_multiple_components(positions);
            my_scene.add_multiple_components(velocities);
            
            for i in 0..NUM_ITER
            {
                assert!(my_scene.remove_entity(&Entity::new(i, gen)));
            }
        }
    });
}

#[bench]
fn bench_build_delete_all(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
        let mut my_scene = make_scene_multi_component();
        
        for i in 0..NUM_ITER
        {
            assert!(my_scene.remove_entity(&Entity::new(i, 1)));
        }
        
        // Force flushing of the component removal
        my_scene.think(&mut dummy);
    });
}

#[bench]
fn bench_build_delete_all_ordered(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
        let mut my_scene = make_scene_removal_ordered();
        
        for i in 0..NUM_ITER
        {
            assert!(my_scene.remove_entity(&Entity::new(i, 1)));
        }
        
        // Force flushing of the component removal
        my_scene.think(&mut dummy);
    });
}

#[bench]
fn bench_build_delete_gradual(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
        let mut my_scene = make_scene_multi_component();
        let rng = &mut rand::thread_rng();
        
        let mut ids : Vec<usize> = (0..NUM_ITER).collect();
        ids.shuffle(rng);
        
        while !ids.is_empty()
        {
            for _ in 0..(NUM_ITER / 10) + 1
            {
                if let Some(id) = ids.pop()
                {
                    assert!(my_scene.remove_entity(&Entity::new(id, 1)));
                }
            }
            
            // Force flushing of the component removal
            my_scene.think(&mut dummy);
        }
    });
}

#[bench]
fn bench_build_delete_gradual_ordered(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
        let mut my_scene = make_scene_removal_ordered();
        let rng = &mut rand::thread_rng();
        
        let mut ids : Vec<usize> = (0..NUM_ITER).collect();
        ids.shuffle(rng);
        
        while !ids.is_empty()
        {
            for _ in 0..(NUM_ITER / 10) + 1
            {
                if let Some(id) = ids.pop()
                {
                    assert!(my_scene.remove_entity(&Entity::new(id, 1)));
                }
            }
            
            // Force flushing of the component removal
            my_scene.think(&mut dummy);
        }
    });
}

#[bench]
fn bench_build_single_deferred(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
        let mut my_scene = SimpleScene::new();

        my_scene.add_thinker_system(Box::new(DeferredBuildSingleThinker {}));

        my_scene.think(&mut dummy);
    });
}

#[bench]
fn bench_build_multi_deferred(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
        let mut my_scene = SimpleScene::new();

        my_scene.add_thinker_system(Box::new(DeferredBuildMultiThinker {}));

        my_scene.think(&mut dummy);
    });
}

#[bench]
fn bench_update_10000_systems_overhead(bencher : &mut Bencher)
{
    let mut dummy = GameControl::new_headless(&GameInfo::default()).unwrap();
    let mut scene = Box::new(SimpleScene::new());
    
    let new_entity = scene.add_entity();
    scene.add_component(&new_entity, PositionVelocity::new());

    scene.add_thinker_system(Box::new(PosVelSingleThinker {}));
    
    bencher.iter(||
    {
        for _ in 0..10_000
        {
            scene.think(&mut dummy);
        }
    });
}

extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{DrawerSystem, ThinkerSystem, SceneType, SpawnControl, DynArgList, SceneConfig, ThinkerArgs, DrawerArgs};
use keeshond::renderer::{DrawSpriteInfo, SliceId, Sheet, DrawOptions, DrawOrdering};
use keeshond::util::SimpleTransform;

use keeshond::datapack::{DataHandle, DataId, LoadErrorMode};
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

use rand::Rng;

const DOGGY_SCALE : f64 = 32.0;
const DOGGY_ANGLE_MAX : f32 = 0.2;
const DOGGY_GRAVITY : f64 = 0.8;
const DOGGY_HSPEED_MIN : f64 = 4.0;
const DOGGY_HSPEED_MAX : f64 = 10.0;
const DOGGY_VSPEED_MAX : f64 = 2.0;
const DOGGY_BOUNCE_MIN : f64 = 25.0;
const DOGGY_BOUNCE_MAX : f64 = 34.0;
const DOGGY_SLICE_MAX : SliceId = 3;


////////////////////////////////////////


keeshond::spawnables!
{
    DoggySpawnId;
    Doggy
}

struct DoggyScene
{
    
}

impl SceneType for DoggyScene
{
    type SpawnableIdType = DoggySpawnId;
    
    fn new() -> Self where Self : Sized { DoggyScene {} }
    fn config(&mut self, game: &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.thinker(DoggyThinker {})
            .drawer(DoggyDrawer::new(game));

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : DoggySpawnId,
        _game : &mut GameControl, transform : &SimpleTransform, _args : &DynArgList)
    {
        match spawnable_id
        {
            DoggySpawnId::Doggy =>
            {
                spawn.with(Doggy::new(transform.x, transform.y));
            }
        }
    }
}


////////////////////////////////////////


#[derive(Component)]
#[component(removal = "ordered")]
struct Doggy
{
    x : f64,
    y : f64,
    old_x : f64,
    old_y : f64,
    vel_x : f64,
    vel_y : f64,
    scale_x : f32,
    scale_y : f32,
    angle : f32,
    slice : SliceId
}

impl Doggy
{
    pub fn new(x : f64, y : f64) -> Doggy
    {
        let mut rng = rand::thread_rng();
        let mut scale_xy = 1.0;
        let mut angle = 0.0;
        let mut slice = 0;
        
        if rng.gen_bool(0.5)
        {
            scale_xy = rng.gen_range(0.8..1.2) * scale_xy;
            angle = rng.gen_range(-DOGGY_ANGLE_MAX..DOGGY_ANGLE_MAX);
        }

        if rng.gen_bool(0.5)
        {
            slice = rng.gen_range(0..DOGGY_SLICE_MAX);
        }
        
        Doggy
        {
            x,
            y,
            old_x : x,
            old_y : y,
            vel_x : rng.gen_range(DOGGY_HSPEED_MIN..DOGGY_HSPEED_MAX),
            vel_y : rng.gen_range(0.0..DOGGY_VSPEED_MAX),
            scale_x : scale_xy,
            scale_y : scale_xy,
            angle,
            slice
        }
    }
    
    #[inline(always)]
    pub fn get_render_position(&self, interpolation : f32) -> (f32, f32)
    {
        (self.x as f32 + (self.x - self.old_x) as f32 * interpolation, self.y as f32 + (self.y - self.old_y) as f32 * interpolation)
    }
}

struct DoggyThinker {}

impl ThinkerSystem<DoggyScene> for DoggyThinker
{
    fn start(&mut self, args : ThinkerArgs<DoggyScene>)
    {
        for _ in 0..2
        {
            args.scene.spawn_later(DoggySpawnId::Doggy, args.game, &SimpleTransform::with_xy(16.0, 16.0));
        }
    }
    
    fn think(&mut self, args : ThinkerArgs<DoggyScene>)
    {
        let mut rng = rand::thread_rng();
        let mut doggy_store = args.components.store_mut::<Doggy>();
        
        let (base_width, base_height) = args.game.renderer().base_size();
        
        let left_side = DOGGY_SCALE / 2.0;
        let right_side = base_width as f64 - left_side;
        let bottom_side = base_height as f64 - left_side;
        
        for (_, doggy) in doggy_store.iter_mut()
        {
            doggy.old_x = doggy.x;
            doggy.old_y = doggy.y;

            doggy.vel_y += DOGGY_GRAVITY;
            doggy.x += doggy.vel_x;
            doggy.y += doggy.vel_y;
            
            if doggy.x < left_side && doggy.vel_x < 0.0
            {
                doggy.x = left_side;
                doggy.vel_x = rng.gen_range(DOGGY_HSPEED_MIN..DOGGY_HSPEED_MAX);
            }
            else if doggy.x > right_side && doggy.vel_x > 0.0
            {
                doggy.x = right_side;
                doggy.vel_x = rng.gen_range(-DOGGY_HSPEED_MAX..-DOGGY_HSPEED_MIN);
            }
            
            if doggy.y > bottom_side
            {
                doggy.y = bottom_side;
                doggy.vel_y = rng.gen_range(-DOGGY_BOUNCE_MAX..-DOGGY_BOUNCE_MIN);
            }
        }

        let (mouse_x, mouse_y) = args.game.input().cursor_position();

        if args.game.input().held("spawn_faster")
        {
            for _ in 0..1000
            {
                args.scene.spawn_later(DoggySpawnId::Doggy, args.game, &SimpleTransform::with_xy(16.0, 16.0));
            }
        }
        else if args.game.input().held("spawn") || args.game.input().down_once("spawn_once")
        {
            for _ in 0..100
            {
                args.scene.spawn_later(DoggySpawnId::Doggy, args.game, &SimpleTransform::with_xy(16.0, 16.0));
            }
        }
        else if args.game.input().held("spawn_mouse")
        {
            for _ in 0..100
            {
                args.scene.spawn_later(DoggySpawnId::Doggy, args.game, &SimpleTransform::with_xy(mouse_x, mouse_y));
            }
        }
        if args.game.input().held("despawn")
        {
            let mut iter = doggy_store.iter_mut();
            
            for _ in 0..100
            {
                if let Some((entity, _)) = iter.next()
                {
                    args.scene.remove_entity_later(&entity);
                }
            }
        }
        
        if args.game.input().down_once("reset")
        {
            args.game.goto_new_scene::<DoggyScene>();
        }
    }
}

struct DoggyDrawer
{
    doggy_sheet : DataHandle<Sheet>
}

impl DoggyDrawer
{
    fn new(control : &mut GameControl) -> DoggyDrawer
    {
        let doggy_sheet = DataHandle::with_path("doggymark", "doggy.png");
        doggy_sheet.resolve(&mut control.res().store_mut(), LoadErrorMode::Fatal);

        DoggyDrawer
        {
            doggy_sheet
        }
    }
}

impl DrawerSystem for DoggyDrawer
{
    fn draw(&self, args : DrawerArgs)
    {
        let doggy_store = args.components.store::<Doggy>();
        let iter = doggy_store.iter();
        
        for (_, doggy) in iter
        {
            let (x, y) = doggy.get_render_position(args.interpolation);
            let transform = args.transform.clone();

            args.drawing.draw_sprite(&DrawSpriteInfo
            {
                sheet_id : self.doggy_sheet.id(),
                shader_id : DataId::new(0),
                x,
                y,
                angle : doggy.angle,
                scale_x : doggy.scale_x,
                scale_y : doggy.scale_y,
                slice : doggy.slice,
                transform,
                r : 1.0,
                g : 1.0,
                b : 1.0,
                alpha : 1.0,
                depth : 0.0
            }, &DrawOptions
            {
                ordering : DrawOrdering::Painter
            });
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "doggymark",
        friendly_name : "Keeshond Doggymark",
        base_width : 1280,
        base_height : 720,
        texture_filtering : false,
        cursor_visible : true,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    gameloop.control_mut().renderer_mut().set_window_title("Keeshond Doggymark | Space to spawn doggos! | S - Spawn slowly | Delete - Despawn | R - Reset");
    
    let mut rng = rand::thread_rng();
    
    // Make first few numbers more random
    for _ in 0..100
    {
        rng.gen::<f64>();
    }
    
    let input = gameloop.control_mut().input_mut();

    input.add_bind("spawn", "space");
    input.add_bind("spawn_mouse", "mouse button left");
    input.add_bind("spawn_faster", "f");
    input.add_bind("spawn_once", "s");
    input.add_bind("despawn", "delete");
    input.add_bind("despawn", "mouse button right");
    input.add_bind("reset", "r");
    
    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    gameloop.control_mut().goto_new_scene::<DoggyScene>();
    
    gameloop.run();
}

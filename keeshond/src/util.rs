
use core::iter::Iterator;
use std::collections::{BTreeMap, HashMap};

use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::ops::{Add, AddAssign, MulAssign, Sub, SubAssign};
use std::fmt::Debug;

const ANGLE_UP_RIGHT_RIGHT : f64 = -std::f64::consts::FRAC_PI_8;
const ANGLE_UP_RIGHT : f64 = -std::f64::consts::FRAC_PI_4;
const ANGLE_UP_UP_RIGHT : f64 = -std::f64::consts::FRAC_PI_4 - std::f64::consts::FRAC_PI_8;
const ANGLE_UP_UP_LEFT : f64 = -std::f64::consts::FRAC_PI_2 - std::f64::consts::FRAC_PI_8;
const ANGLE_UP_LEFT : f64 = -std::f64::consts::FRAC_PI_2 - std::f64::consts::FRAC_PI_4;
const ANGLE_UP_LEFT_LEFT : f64 = -std::f64::consts::FRAC_PI_2 - std::f64::consts::FRAC_PI_4 - std::f64::consts::FRAC_PI_8;
const ANGLE_DOWN_LEFT_LEFT : f64 = std::f64::consts::FRAC_PI_2 + std::f64::consts::FRAC_PI_4 + std::f64::consts::FRAC_PI_8;
const ANGLE_DOWN_LEFT : f64 = std::f64::consts::FRAC_PI_2 + std::f64::consts::FRAC_PI_4;
const ANGLE_DOWN_DOWN_LEFT : f64 = std::f64::consts::FRAC_PI_2 + std::f64::consts::FRAC_PI_8;
const ANGLE_DOWN_DOWN_RIGHT : f64 = std::f64::consts::FRAC_PI_4 + std::f64::consts::FRAC_PI_8;
const ANGLE_DOWN_RIGHT : f64 = std::f64::consts::FRAC_PI_4;
const ANGLE_DOWN_RIGHT_RIGHT : f64 = std::f64::consts::FRAC_PI_8;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum InputDirection4
{
    Left,
    Right,
    Up,
    Down
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum InputDirection8
{
    Left,
    UpLeft,
    Up,
    UpRight,
    Right,
    DownRight,
    Down,
    DownLeft
}

impl From<InputDirection4> for InputDirection8
{
    fn from(dir_4 : InputDirection4) -> Self
    {
        match dir_4
        {
            InputDirection4::Left => { InputDirection8::Left }
            InputDirection4::Right => { InputDirection8::Right }
            InputDirection4::Up => { InputDirection8::Up }
            InputDirection4::Down => { InputDirection8::Down }
        }
    }
}

pub fn xy_to_angle(x : f64, y : f64) -> Option<f64>
{
    if x == 0.0 && y == 0.0
    {
        return None;
    }

    Some(y.atan2(x))
}

pub fn xy_to_distance(x : f64, y : f64) -> f64
{
    (x.powi(2) + y.powi(2)).sqrt()
}

pub fn xy_to_distance_clamped(x : f64, y : f64) -> f64
{
    (x.powi(2) + y.powi(2)).sqrt().min(1.0)
}

pub fn angle_to_four_way_direction(angle : f64) -> Option<InputDirection4>
{
    if angle < ANGLE_UP_LEFT || angle >= ANGLE_DOWN_LEFT
    {
        return Some(InputDirection4::Left)
    }
    else if angle >= ANGLE_UP_RIGHT && angle < ANGLE_DOWN_RIGHT
    {
        return Some(InputDirection4::Right)
    }
    else if angle >= ANGLE_UP_LEFT && angle < ANGLE_UP_RIGHT
    {
        return Some(InputDirection4::Up)
    }
    else if angle >= ANGLE_DOWN_RIGHT && angle < ANGLE_DOWN_LEFT
    {
        return Some(InputDirection4::Down)
    }

    None
}

pub fn angle_to_four_way_direction_deadzone(angle : f64, deadzone : f64) -> Option<InputDirection4>
{
    if angle < ANGLE_UP_LEFT - deadzone || angle > ANGLE_DOWN_LEFT + deadzone
    {
        return Some(InputDirection4::Left)
    }
    else if angle > ANGLE_UP_RIGHT + deadzone && angle < ANGLE_DOWN_RIGHT - deadzone
    {
        return Some(InputDirection4::Right)
    }
    else if angle > ANGLE_UP_LEFT + deadzone && angle < ANGLE_UP_RIGHT - deadzone
    {
        return Some(InputDirection4::Up)
    }
    else if angle > ANGLE_DOWN_RIGHT + deadzone && angle < ANGLE_DOWN_LEFT - deadzone
    {
        return Some(InputDirection4::Down)
    }

    None
}

pub fn angle_to_eight_way_direction(angle : f64) -> Option<InputDirection8>
{
    if angle < ANGLE_UP_LEFT_LEFT || angle >= ANGLE_DOWN_LEFT_LEFT
    {
        return Some(InputDirection8::Left)
    }
    else if angle >= ANGLE_UP_LEFT_LEFT && angle < ANGLE_UP_UP_LEFT
    {
        return Some(InputDirection8::UpLeft)
    }
    else if angle >= ANGLE_UP_UP_LEFT && angle < ANGLE_UP_UP_RIGHT
    {
        return Some(InputDirection8::Up)
    }
    else if angle >= ANGLE_UP_UP_RIGHT && angle < ANGLE_UP_RIGHT_RIGHT
    {
        return Some(InputDirection8::UpRight)
    }
    else if angle >= ANGLE_UP_RIGHT_RIGHT && angle < ANGLE_DOWN_RIGHT_RIGHT
    {
        return Some(InputDirection8::Right)
    }
    else if angle >= ANGLE_DOWN_RIGHT_RIGHT && angle < ANGLE_DOWN_DOWN_RIGHT
    {
        return Some(InputDirection8::DownRight)
    }
    else if angle >= ANGLE_DOWN_DOWN_RIGHT && angle < ANGLE_DOWN_DOWN_LEFT
    {
        return Some(InputDirection8::Down)
    }
    else if angle >= ANGLE_DOWN_DOWN_LEFT && angle < ANGLE_DOWN_LEFT_LEFT
    {
        return Some(InputDirection8::DownLeft)
    }

    None
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Serialize, Deserialize)]
pub struct Rect<T : Copy + Add<Output = T> + Sub<Output = T> + AddAssign + SubAssign + MulAssign + PartialOrd>
{
    pub x : T,
    pub y : T,
    pub w : T,
    pub h : T
}

impl<T : Debug + Copy + Add<Output = T> + Sub<Output = T> + AddAssign + SubAssign + MulAssign + PartialOrd> Rect<T>
{
    #[inline(always)]
    pub fn x2(&self) -> T
    {
        self.x + self.w
    }

    #[inline(always)]
    pub fn y2(&self) -> T
    {
        self.y + self.h
    }

    pub fn translate(&mut self, x : T, y : T)
    {
        self.x += x;
        self.y += y;
    }

    pub fn extrude(&mut self, amount : T)
    {
        self.x -= amount;
        self.y -= amount;
        self.w += amount + amount;
        self.h += amount + amount;
    }

    pub fn contains(&self, x : T, y : T) -> bool
    {
        x >= self.x && x <= self.x + self.w && y >= self.y && y <= self.y + self.h
    }

    pub fn bake(&self) -> BakedRect<T>
    {
        BakedRect
        {
            x1 : self.x,
            y1 : self.y,
            x2 : self.x2(),
            y2 : self.y2()
        }
    }
}

pub fn rotate_point_f32(x : f32, y : f32, angle : f32) -> (f32, f32)
{
    let (sine, cosine) = angle.sin_cos();

    let new_x = x * cosine - y * sine;
    let new_y = x * sine + y * cosine;

    (new_x, new_y)
}

pub fn rotate_point_f64(x : f64, y : f64, angle : f64) -> (f64, f64)
{
    let (sine, cosine) = angle.sin_cos();

    let new_x = x * cosine - y * sine;
    let new_y = x * sine + y * cosine;

    (new_x, new_y)
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Serialize, Deserialize)]
pub struct BakedRect<T : Copy + Add<Output = T> + Sub<Output = T> + AddAssign + SubAssign + MulAssign + PartialOrd>
{
    pub x1 : T,
    pub y1 : T,
    pub x2 : T,
    pub y2 : T
}

impl<T : Copy + Add<Output = T> + Sub<Output = T> + AddAssign + SubAssign + MulAssign + PartialOrd> BakedRect<T>
{
    #[inline(always)]
    pub fn w(&self) -> T
    {
        self.x2 - self.x1
    }

    #[inline(always)]
    pub fn h(&self) -> T
    {
        self.y2 - self.y1
    }

    pub fn translate(&mut self, x : T, y : T)
    {
        self.x1 += x;
        self.y1 += y;
        self.x2 += x;
        self.y2 += y;
    }

    pub fn clone_translated(&self, x : T, y : T) -> BakedRect<T>
    {
        BakedRect
        {
            x1 : self.x1 + x,
            y1 : self.y1 + y,
            x2 : self.x2 + x,
            y2 : self.y2 + y
        }
    }

    pub fn scale(&mut self, scale_x : T, scale_y : T)
    {
        self.x1 *= scale_x;
        self.y1 *= scale_y;
        self.x2 *= scale_x;
        self.y2 *= scale_y;
    }

    pub fn extrude(&mut self, amount : T)
    {
        self.x1 -= amount;
        self.y1 -= amount;
        self.x2 += amount;
        self.y2 += amount;
    }

    pub fn combine(&mut self, other : &BakedRect<T>)
    {
        if other.x1 < self.x1 { self.x1 = other.x1; }
        if other.y1 < self.y1 { self.y1 = other.y1; }
        if other.x2 > self.x2 { self.x2 = other.x2; }
        if other.y2 > self.y2 { self.y2 = other.y2; }
    }

    pub fn intersection(&mut self, other : &BakedRect<T>)
    {
        if other.x1 > self.x1 { self.x1 = other.x1; }
        if other.y1 > self.y1 { self.y1 = other.y1; }
        if other.x2 < self.x2 { self.x2 = other.x2; }
        if other.y2 < self.y2 { self.y2 = other.y2; }
    }

    pub fn make_positive(&mut self)
    {
        if self.x2 < self.x1
        {
            std::mem::swap(&mut self.x1, &mut self.x2);
        }
        if self.y2 < self.y1
        {
            std::mem::swap(&mut self.y1, &mut self.y2);
        }
    }

    pub fn contains(&self, x : T, y : T) -> bool
    {
        x >= self.x1 && x <= self.x2 && y >= self.y1 && y <= self.y2
    }

    pub fn touches(&self, other : &BakedRect<T>) -> bool
    {
        self.x1 <= other.x2 && self.x2 >= other.x1 && self.y1 <= other.y2 && self.y2 >= other.y1
    }

    pub fn overlaps(&self, other : &BakedRect<T>) -> bool
    {
        self.x1 < other.x2 && self.x2 > other.x1 && self.y1 < other.y2 && self.y2 > other.y1
    }
}

impl BakedRect<f32>
{
    pub fn grow_for_angle(&mut self, angle : f32)
    {
        let point_1 = rotate_point_f32(self.x1, self.y1, angle);
        let point_2 = rotate_point_f32(self.x2, self.y1, angle);
        let point_3 = rotate_point_f32(self.x1, self.y2, angle);
        let point_4 = rotate_point_f32(self.x2, self.y2, angle);

        self.x1 = point_1.0.min(point_2.0.min(point_3.0.min(point_4.0)));
        self.y1 = point_1.1.min(point_2.1.min(point_3.1.min(point_4.1)));
        self.x2 = point_1.0.max(point_2.0.max(point_3.0.max(point_4.0)));
        self.y2 = point_1.1.max(point_2.1.max(point_3.1.max(point_4.1)));
    }
}

impl BakedRect<f64>
{
    pub fn grow_for_angle(&mut self, angle : f64)
    {
        let point_1 = rotate_point_f64(self.x1, self.y1, angle);
        let point_2 = rotate_point_f64(self.x2, self.y1, angle);
        let point_3 = rotate_point_f64(self.x1, self.y2, angle);
        let point_4 = rotate_point_f64(self.x2, self.y2, angle);

        self.x1 = point_1.0.min(point_2.0.min(point_3.0.min(point_4.0)));
        self.y1 = point_1.1.min(point_2.1.min(point_3.1.min(point_4.1)));
        self.x2 = point_1.0.max(point_2.0.max(point_3.0.max(point_4.0)));
        self.y2 = point_1.1.max(point_2.1.max(point_3.1.max(point_4.1)));
    }
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Serialize, Deserialize)]
pub struct Circle<T : Copy + Add<Output = T> + AddAssign>
{
    pub x : T,
    pub y : T,
    pub radius : T
}

impl<T : Copy + Add<Output = T> + AddAssign> Circle<T>
{
    pub fn translate(&mut self, x : T, y : T)
    {
        self.x += x;
        self.y += y;
    }

    pub fn clone_translated(&self, x : T, y : T) -> Circle<T>
    {
        Circle
        {
            x : self.x + x,
            y : self.y + y,
            radius : self.radius
        }
    }
}

impl Circle<f32>
{
    pub fn overlaps(&self, other : &Circle<f32>) -> bool
    {
        let distance = self.radius + other.radius;

        ((other.x - self.x).powi(2) + (other.y - self.y).powi(2)).sqrt() < distance
    }

    pub fn point_overlaps(&self, x : f32, y : f32) -> bool
    {
        ((x - self.x).powi(2) + (y - self.y).powi(2)).sqrt() < self.radius
    }
}

impl Circle<f64>
{
    pub fn overlaps(&self, other : &Circle<f64>) -> bool
    {
        let distance = self.radius + other.radius;

        ((other.x - self.x).powi(2) + (other.y - self.y).powi(2)).sqrt() < distance
    }

    pub fn point_overlaps(&self, x : f64, y : f64) -> bool
    {
        ((x - self.x).powi(2) + (y - self.y).powi(2)).sqrt() < self.radius
    }
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Serialize, Deserialize)]
pub struct OrientedRect<T : Copy + Add<Output = T> + Sub<Output = T> + AddAssign + SubAssign + MulAssign + PartialOrd>
{
    pub rect : BakedRect<T>,
    pub angle : T,
    pub scale_x : T,
    pub scale_y : T
}

impl OrientedRect<f64>
{
    pub fn points(&self) -> [(f64, f64); 4]
    {
        let mut points = [(self.rect.x1, self.rect.y1), (self.rect.x2, self.rect.y1),
            (self.rect.x2, self.rect.y2), (self.rect.x1, self.rect.y2)];

        for point in &mut points
        {
            point.0 *= self.scale_x;
            point.1 *= self.scale_y;

            (point.0, point.1) = rotate_point_f64(point.0, point.1, self.angle);
        }

        points
    }
}

impl OrientedRect<f32>
{
    pub fn points(&self) -> [(f32, f32); 4]
    {
        let mut points = [(self.rect.x1, self.rect.y1), (self.rect.x2, self.rect.y1),
            (self.rect.x2, self.rect.y2), (self.rect.x1, self.rect.y2)];

        for point in &mut points
        {
            point.0 *= self.scale_x;
            point.1 *= self.scale_y;

            (point.0, point.1) = rotate_point_f32(point.0, point.1, self.angle);
        }

        points
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SimpleTransform
{
    pub x : f64,
    pub y : f64,
    pub z : f64,
    pub angle : f64,
    pub scale_x : f64,
    pub scale_y : f64
}

impl Default for SimpleTransform
{
    fn default() -> Self
    {
        SimpleTransform
        {
            x : 0.0,
            y : 0.0,
            z : 0.0,
            angle : 0.0,
            scale_x : 1.0,
            scale_y : 1.0
        }
    }
}

impl SimpleTransform
{
    pub fn with_xy(x : f64, y : f64) -> SimpleTransform
    {
        SimpleTransform
        {
            x,
            y,
            z : 0.0,
            angle : 0.0,
            scale_x : 1.0,
            scale_y : 1.0
        }
    }

    pub fn with_pos(x : f64, y : f64, z : f64) -> SimpleTransform
    {
        SimpleTransform
        {
            x,
            y,
            z,
            angle : 0.0,
            scale_x : 1.0,
            scale_y : 1.0
        }
    }

    pub fn local_to_world_pos(&self, mut x : f64, mut y : f64) -> (f64, f64)
    {
        x *= self.scale_x;
        y *= self.scale_y;
        let (mut x, mut y) = rotate_point_f64(x, y, self.angle);
        x += self.x;
        y += self.y;

        (x, y)
    }

    pub fn world_to_local_pos(&self, mut x : f64, mut y : f64) -> (f64, f64)
    {
        x -= self.x;
        y -= self.y;
        let (mut x, mut y) = rotate_point_f64(x, y, -self.angle);
        x /= self.scale_x.max(0.0000001);
        y /= self.scale_y.max(0.0000001);

        (x, y)
    }
}

fn sort_map<S, T : Serialize + Ord, U : Serialize>(map : &HashMap<T, U>, serializer : S) -> Result<S::Ok, S::Error> where S : Serializer
{
    let sorted : BTreeMap<_, _> = map.iter().collect();

    sorted.serialize(serializer)
}

#[derive(Clone, Default, Serialize, Debug)]
pub struct NamedItemStore<T>
{
    #[serde(rename = "i")]
    #[serde(alias = "items")]
    items : Vec<T>,
    #[serde(serialize_with = "sort_map")]
    #[serde(rename = "n")]
    #[serde(alias = "names")]
    names : HashMap<String, usize>,
    #[serde(skip_serializing)]
    ids : HashMap<usize, String>
}

impl<T> NamedItemStore<T>
{
    pub fn new() -> NamedItemStore<T>
    {
        NamedItemStore::<T>
        {
            items : Vec::new(),
            names : HashMap::new(),
            ids : HashMap::new()
        }
    }

    pub fn get(&self, id : usize) -> Option<&T>
    {
        self.items.get(id)
    }

    pub fn get_mut(&mut self, id : usize) -> Option<&mut T>
    {
        self.items.get_mut(id)
    }

    pub fn set(&mut self, id : usize, item : T) -> bool
    {
        if let Some(old_item) = self.items.get_mut(id)
        {
            *old_item = item;

            return true;
        }

        false
    }

    pub fn get_name(&self, id : usize) -> Option<String>
    {
        self.ids.get(&id).cloned()
    }

    pub fn get_id(&self, name : &str) -> Option<usize>
    {
        self.names.get(name).cloned()
    }

    pub fn insert_item(&mut self, pos : usize, item : T) -> bool
    {
        if pos > self.items.len()
        {
            return false;
        }

        for pos in (pos..self.items.len()).rev()
        {
            if let Some(moved_name) = self.ids.remove(&pos)
            {
                self.names.insert(moved_name.clone(), pos + 1);
                self.ids.insert(pos + 1, moved_name);
            }
        }

        self.items.insert(pos, item);

        true
    }

    pub fn remove_item(&mut self, id : usize) -> Option<T>
    {
        if id >= self.items.len()
        {
            return None;
        }

        self.rename_item(id, "");

        for pos in (id + 1)..self.items.len()
        {
            if let Some(moved_name) = self.ids.remove(&pos)
            {
                self.names.insert(moved_name.clone(), pos - 1);
                self.ids.insert(pos - 1, moved_name);
            }
        }

        Some(self.items.remove(id))
    }

    pub fn rename_item(&mut self, id : usize, new_name : &str) -> bool
    {
        if !new_name.is_empty() && self.names.contains_key(new_name)
        {
            return false;
        }

        if let Some(old_name) = self.ids.get(&id).cloned()
        {
            let mut bad = false;

            if self.ids.remove(&id).is_none() { bad = true; }
            if self.names.remove(&old_name).is_none() { bad = true; }

            if bad
            {
                warn!("Inconsistent item name storage: {} {}", id, old_name);
                return false;
            }
        }

        if !new_name.is_empty()
        {
            self.ids.insert(id, String::from(new_name));
            self.names.insert(String::from(new_name), id);
        }

        true
    }

    pub fn clear(&mut self)
    {
        self.items.clear();
        self.ids.clear();
        self.names.clear();
    }

    pub fn len(&self) -> usize
    {
        self.items.len()
    }
}

impl<'a, T> IntoIterator for &'a NamedItemStore<T>
{
    type Item = &'a T;
    type IntoIter = std::slice::Iter<'a, T>;

    fn into_iter(self) -> Self::IntoIter
    {
        self.items.iter()
    }
}

impl<'a, T> IntoIterator for &'a mut NamedItemStore<T>
{
    type Item = &'a mut T;
    type IntoIter = std::slice::IterMut<'a, T>;

    fn into_iter(self) -> Self::IntoIter
    {
        self.items.iter_mut()
    }
}

#[derive(Deserialize, Debug)]
struct NamedItemStoreRaw<T>
{
    #[serde(rename = "i")]
    #[serde(alias = "items")]
    items : Vec<T>,
    #[serde(rename = "n")]
    #[serde(alias = "names")]
    names : HashMap<String, usize>
}

impl<'de, T : Deserialize<'de>> Deserialize<'de> for NamedItemStore<T>
{
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
        where D : Deserializer<'de>
    {
        let store_raw = NamedItemStoreRaw::<T>::deserialize(deserializer)?;

        let mut ids = HashMap::new();

        for (name, id) in &store_raw.names
        {
            ids.insert(*id, name.clone());
        }

        Ok(NamedItemStore::<T>
        {
            items : store_raw.items,
            names : store_raw.names,
            ids
        })
    }
}

use sdl2::event::Event;
use sdl2::keyboard::{Scancode, Mod, Keycode};
use sdl2::mouse::MouseButton;

use sdl2_sys;

use imgui;
use imgui_sys;

use std::os::raw::*;

extern "C" fn sdl_get_clipboard(_user_data: *mut c_void) -> *const c_char
{
    unsafe { sdl2_sys::SDL_GetClipboardText() }
}

extern "C" fn sdl_set_clipboard(_user_data: *mut c_void, text: *const c_char)
{
    unsafe { sdl2_sys::SDL_SetClipboardText(text); }
}

fn scancode_map_for_keycode(keycode : Keycode) -> u32
{
    if let Some(scancode) = Scancode::from_keycode(keycode)
    {
        let mut value = scancode as u32;

        if value >= 512
        {
            value = u32::MAX;
        }

        return value;
    }

    u32::MAX
}

pub struct ImGuiHandler
{
    imgui : imgui::Context,
    last_cursor : sdl2::mouse::SystemCursor,
    cursor : Option<sdl2::mouse::Cursor>
}

impl ImGuiHandler
{
    pub fn new() -> Result<ImGuiHandler, String>
    {
        let mut imgui = imgui::Context::create();
        
        let lato_font = imgui::FontSource::TtfData
        {
            data : include_bytes!("../res/Lato-Medium.ttf"),
            size_pixels : 15.0,
            config : Some(imgui::FontConfig
            {
                name : Some("Lato-Medium.ttf, 15px".to_string()),
                pixel_snap_h : false,
                glyph_extra_spacing : [0.0, 0.0],
                .. Default::default()
            })
        };
        
        let proggy_font = imgui::FontSource::DefaultFontData
        {
            config : Some(imgui::FontConfig
            {
                oversample_h : 1,
                pixel_snap_h : true,
                .. Default::default()
            })
        };
        
        imgui.fonts().add_font(&[lato_font]);
        imgui.fonts().add_font(&[proggy_font]);
        
        imgui.io_mut().font_global_scale = 1.0;
        
        imgui.io_mut().mouse_pos = [0.0, 0.0];
        
        imgui.io_mut().key_map[imgui::Key::Tab as usize] = scancode_map_for_keycode(Keycode::Tab);
        imgui.io_mut().key_map[imgui::Key::LeftArrow as usize] = scancode_map_for_keycode(Keycode::Left);
        imgui.io_mut().key_map[imgui::Key::RightArrow as usize] = scancode_map_for_keycode(Keycode::Right);
        imgui.io_mut().key_map[imgui::Key::UpArrow as usize] = scancode_map_for_keycode(Keycode::Up);
        imgui.io_mut().key_map[imgui::Key::DownArrow as usize] = scancode_map_for_keycode(Keycode::Down);
        imgui.io_mut().key_map[imgui::Key::PageUp as usize] = scancode_map_for_keycode(Keycode::PageUp);
        imgui.io_mut().key_map[imgui::Key::PageDown as usize] = scancode_map_for_keycode(Keycode::PageDown);
        imgui.io_mut().key_map[imgui::Key::Home as usize] = scancode_map_for_keycode(Keycode::Home);
        imgui.io_mut().key_map[imgui::Key::End as usize] = scancode_map_for_keycode(Keycode::End);
        imgui.io_mut().key_map[imgui::Key::Insert as usize] = scancode_map_for_keycode(Keycode::Insert);
        imgui.io_mut().key_map[imgui::Key::Delete as usize] = scancode_map_for_keycode(Keycode::Delete);
        imgui.io_mut().key_map[imgui::Key::Backspace as usize] = scancode_map_for_keycode(Keycode::Backspace);
        imgui.io_mut().key_map[imgui::Key::Space as usize] = scancode_map_for_keycode(Keycode::Space);
        imgui.io_mut().key_map[imgui::Key::Enter as usize] = scancode_map_for_keycode(Keycode::Return);
        imgui.io_mut().key_map[imgui::Key::Escape as usize] = scancode_map_for_keycode(Keycode::Escape);
        imgui.io_mut().key_map[imgui::Key::KeyPadEnter as usize] = scancode_map_for_keycode(Keycode::KpEnter);
        imgui.io_mut().key_map[imgui::Key::A as usize] = scancode_map_for_keycode(Keycode::A);
        imgui.io_mut().key_map[imgui::Key::C as usize] = scancode_map_for_keycode(Keycode::C);
        imgui.io_mut().key_map[imgui::Key::V as usize] = scancode_map_for_keycode(Keycode::V);
        imgui.io_mut().key_map[imgui::Key::X as usize] = scancode_map_for_keycode(Keycode::X);
        imgui.io_mut().key_map[imgui::Key::Y as usize] = scancode_map_for_keycode(Keycode::Y);
        imgui.io_mut().key_map[imgui::Key::Z as usize] = scancode_map_for_keycode(Keycode::Z);
        
        let cursor;
        
        match sdl2::mouse::Cursor::from_system(sdl2::mouse::SystemCursor::Arrow)
        {
            Ok(cursor_ok) =>
            {
                cursor = Some(cursor_ok);
            },
            Err(error) =>
            {
                cursor = None;
                warn!("Could not create system cursor: {}", error);
            }
        }
        
        if cursor.is_some()
        {
            imgui.io_mut().backend_flags = imgui::BackendFlags::HAS_MOUSE_CURSORS;
        }
        imgui.io_mut().config_flags = imgui::ConfigFlags::IS_SRGB;
        imgui.io_mut().config_windows_resize_from_edges = true;
        
        unsafe
        {
            let io = imgui_sys::igGetIO();
            
            (*io).BackendPlatformName = cstr!("Keeshond Game Engine SDL2");
            (*io).GetClipboardTextFn = Some(sdl_get_clipboard);
            (*io).SetClipboardTextFn = Some(sdl_set_clipboard);
        }
        
        Ok(ImGuiHandler
        {
            imgui,
            last_cursor : sdl2::mouse::SystemCursor::Arrow,
            cursor
        })
    }
    
    pub fn imgui(&self) -> &imgui::Context
    {
        &self.imgui
    }
    
    pub fn imgui_mut(&mut self) -> &mut imgui::Context
    {
        &mut self.imgui
    }
    
    fn set_mouse_button(&mut self, button : MouseButton, is_down : bool)
    {
        let mut button_states = self.imgui.io_mut().mouse_down;
        
        match button
        {
            MouseButton::Left => { button_states[0] = is_down; },
            MouseButton::Right => { button_states[1] = is_down; },
            MouseButton::Middle => { button_states[2] = is_down; },
            MouseButton::X1 => { button_states[3] = is_down; },
            MouseButton::X2 => { button_states[4] = is_down; },
            _ => {}
        }
        
        self.imgui.io_mut().mouse_down = button_states;
    }
    
    fn set_key(&mut self, scancode : Option<Scancode>, is_down : bool, keymod : Mod)
    {
        if let Some(code) = scancode
        {
            self.imgui.io_mut().keys_down[code as usize] = is_down;
        }
        self.imgui.io_mut().key_ctrl = keymod.intersects(Mod::LCTRLMOD | Mod::RCTRLMOD);
        self.imgui.io_mut().key_shift = keymod.intersects(Mod::LSHIFTMOD | Mod::RSHIFTMOD);
        self.imgui.io_mut().key_alt = keymod.intersects(Mod::LALTMOD | Mod::RALTMOD);
        self.imgui.io_mut().key_super = keymod.intersects(Mod::LGUIMOD | Mod::RGUIMOD);
    }
    
    pub fn update_cursor(&mut self, mouse_util : &mut sdl2::mouse::MouseUtil, imgui_cursor : Option<imgui::MouseCursor>)
    {
        let mut show_mouse = true;
        let system_cursor;
        
        if let Some(some_cursor) = imgui_cursor
        {
            system_cursor = match some_cursor
            {
                imgui::MouseCursor::Arrow =>
                {
                    sdl2::mouse::SystemCursor::Arrow
                },
                imgui::MouseCursor::TextInput =>
                {
                    sdl2::mouse::SystemCursor::IBeam
                },
                imgui::MouseCursor::ResizeAll =>
                {
                    sdl2::mouse::SystemCursor::SizeAll
                },
                imgui::MouseCursor::ResizeNS =>
                {
                    sdl2::mouse::SystemCursor::SizeNS
                },
                imgui::MouseCursor::ResizeEW =>
                {
                    sdl2::mouse::SystemCursor::SizeWE
                },
                imgui::MouseCursor::ResizeNESW =>
                {
                    sdl2::mouse::SystemCursor::SizeNESW
                },
                imgui::MouseCursor::ResizeNWSE =>
                {
                    sdl2::mouse::SystemCursor::SizeNWSE
                },
                imgui::MouseCursor::Hand =>
                {
                    sdl2::mouse::SystemCursor::Hand
                },
                imgui::MouseCursor::NotAllowed =>
                {
                    sdl2::mouse::SystemCursor::No
                }
            }
        }
        else
        {
            show_mouse = false;
            system_cursor = sdl2::mouse::SystemCursor::Arrow;
        }
        
        if self.cursor.is_some() && system_cursor != self.last_cursor
        {
            if let Ok(new_cursor) = sdl2::mouse::Cursor::from_system(system_cursor)
            {
                new_cursor.set();
                self.cursor = Some(new_cursor);
            }
            else
            {
                warn!("Unable to set system cursor {:?}", system_cursor);
            }
            
            mouse_util.show_cursor(show_mouse);
            
            self.last_cursor = system_cursor;
        }
    }
    
    pub fn update_size_and_delta(&mut self, frame_delta : f32, width : f32, height : f32, scale : f32)
    {
        self.imgui.io_mut().delta_time = frame_delta.max(0.0000001);
        self.imgui.io_mut().display_size = [width / scale, height / scale];
        self.imgui.io_mut().display_framebuffer_scale = [scale, scale];
    }
    
    pub fn handle_event(&mut self, event : &Event) -> bool
    {
        let mut eat_event = false;

        match event
        {
            Event::MouseMotion { x, y, .. } =>
            {
                let [scale, _] = self.imgui.io_mut().display_framebuffer_scale;
                self.imgui.io_mut().mouse_pos = [*x as f32 / scale, *y as f32 / scale];

                if self.imgui.io_mut().want_capture_mouse
                {
                    eat_event = true;
                }
            },
            Event::MouseButtonDown { mouse_btn, .. } =>
            {
                self.set_mouse_button(*mouse_btn, true);

                if self.imgui.io_mut().want_capture_mouse
                {
                    eat_event = true;
                }
            },
            Event::MouseButtonUp { mouse_btn, .. } =>
            {
                self.set_mouse_button(*mouse_btn, false);

                if self.imgui.io_mut().want_capture_mouse
                {
                    eat_event = true;
                }
            },
            Event::MouseWheel { x, y, .. } =>
            {
                self.imgui.io_mut().mouse_wheel_h += *x as f32;
                self.imgui.io_mut().mouse_wheel += *y as f32;
            },
            Event::KeyDown { scancode, keymod, .. } =>
            {
                self.set_key(*scancode, true, *keymod);

                if self.imgui.io_mut().want_capture_keyboard
                {
                    eat_event = true;
                }
            },
            Event::KeyUp { scancode, keymod, .. } =>
            {
                self.set_key(*scancode, false, *keymod);
            },
            Event::TextInput { text, .. } =>
            {
                for letter in text.chars()
                {
                    self.imgui.io_mut().add_input_character(letter);
                }
            },
            _ => {}
        }

        eat_event
    }
}

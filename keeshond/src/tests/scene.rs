use crate::datapack::{DataId, DataMultistore};
use crate::gameloop::{GameInfo, GameControl, GameControlOptions};
use crate::renderer::{DrawTransform, DrawBackgroundColorInfo, DrawOptions, DrawOrdering};
use crate::scene::{BaseScene, SimpleScene, NullSceneType, Entity, Component, ThinkerSystem, DrawerSystem, ThinkerArgs, DrawerArgs, FreezeFlags, ComponentRef, ComponentRefMut};
use rand::seq::SliceRandom;

#[derive(Debug)]
struct FooComponent { runs : u32 }

impl FooComponent
{
    fn new() -> FooComponent
    {
        FooComponent { runs : 0 }
    }
}

impl Component for FooComponent
{
    fn maintain_ordering() -> bool where Self : Sized { false }
}

impl<'a> ComponentRef<'a> for FooComponent
{
    type StoreRef = &'a FooComponent;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for FooComponent
{
    type StoreRefMut = &'a mut FooComponent;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

#[derive(Debug)]
struct BarComponent { runs : u32 }

impl BarComponent
{
    fn new() -> BarComponent
    {
        BarComponent { runs : 0 }
    }
}

impl Component for BarComponent
{
    fn maintain_ordering() -> bool where Self : Sized { false }
}

impl<'a> ComponentRef<'a> for BarComponent
{
    type StoreRef = &'a BarComponent;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for BarComponent
{
    type StoreRefMut = &'a mut BarComponent;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

#[derive(Debug)]
struct BazComponent { runs : u32 }

impl BazComponent
{
    fn new() -> BazComponent
    {
        BazComponent { runs : 0 }
    }
}

impl Component for BazComponent
{
    fn maintain_ordering() -> bool where Self : Sized { false }
}

impl<'a> ComponentRef<'a> for BazComponent
{
    type StoreRef = &'a BazComponent;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for BazComponent
{
    type StoreRefMut = &'a mut BazComponent;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

#[derive(Debug)]
struct FooComponentOrdered { runs : u32 }

impl FooComponentOrdered
{
    fn new() -> FooComponentOrdered
    {
        FooComponentOrdered { runs : 0 }
    }
}

impl Component for FooComponentOrdered
{
    fn maintain_ordering() -> bool where Self : Sized { true }
}

impl<'a> ComponentRef<'a> for FooComponentOrdered
{
    type StoreRef = &'a FooComponentOrdered;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for FooComponentOrdered
{
    type StoreRefMut = &'a mut FooComponentOrdered;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

#[derive(Debug)]
struct BarComponentOrdered { runs : u32 }

impl BarComponentOrdered
{
    fn new() -> BarComponentOrdered
    {
        BarComponentOrdered { runs : 0 }
    }
}

impl Component for BarComponentOrdered
{
    fn maintain_ordering() -> bool where Self : Sized { true }
}

impl<'a> ComponentRef<'a> for BarComponentOrdered
{
    type StoreRef = &'a BarComponentOrdered;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for BarComponentOrdered
{
    type StoreRefMut = &'a mut BarComponentOrdered;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        self
    }
}

#[derive(Debug)]
struct FooSystem {}

impl FooSystem
{
    fn new() -> FooSystem
    {
        FooSystem {}
    }
}

impl ThinkerSystem<NullSceneType> for FooSystem
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let mut foo_store = args.components.store_mut::<FooComponent>();
        let iter = foo_store.iter_mut();
    
        for (_, foo) in iter
        {
            foo.runs += 1;
        }
    }
}

#[derive(Debug)]
struct FooBarSystem {}

impl FooBarSystem
{
    fn new() -> FooBarSystem
    {
        FooBarSystem {}
    }
}

impl ThinkerSystem<NullSceneType> for FooBarSystem
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let mut foo_store = args.components.store_mut::<FooComponent>();
        let mut bar_store = args.components.store_mut::<BarComponent>();
        
        let iter = foo_store.iter_mut();
    
        for (entity, foo) in iter
        {
            if let Some(bar) = bar_store.get_entity_mut(&entity)
            {
                foo.runs += 10;
                bar.runs += 10;
            }
        }
    }
}

#[derive(Debug)]
struct FooBarFreezeSystem {}

impl FooBarFreezeSystem
{
    fn new() -> FooBarFreezeSystem
    {
        FooBarFreezeSystem {}
    }
}

impl ThinkerSystem<NullSceneType> for FooBarFreezeSystem
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let mut foo_store = args.components.store_mut::<FooComponent>();
        let mut bar_store = args.components.store_mut::<BarComponent>();
        let mut baz_store = args.components.store_mut::<BazComponent>();

        for (_, foo) in foo_store.iter_mut()
        {
            foo.runs += 1;
        }

        for (_, bar) in bar_store.iter_all_mut()
        {
            bar.runs += 1;
        }

        for (_, baz, _) in baz_store.iter_filtered_mut(FreezeFlags::REASON_HIDDEN)
        {
            baz.runs += 1;
        }
    }
}

#[derive(Debug)]
struct FooSystemOrdered {}

impl FooSystemOrdered
{
    fn new() -> FooSystemOrdered
    {
        FooSystemOrdered {}
    }
}

impl ThinkerSystem<NullSceneType> for FooSystemOrdered
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let mut foo_store = args.components.store_mut::<FooComponentOrdered>();
        let iter = foo_store.iter_mut();

        for (_, foo) in iter
        {
            foo.runs += 1;
        }
    }
}

#[derive(Debug)]
struct FooBarSystemOrdered {}

impl FooBarSystemOrdered
{
    fn new() -> FooBarSystemOrdered
    {
        FooBarSystemOrdered {}
    }
}

impl ThinkerSystem<NullSceneType> for FooBarSystemOrdered
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let mut foo_store = args.components.store_mut::<FooComponentOrdered>();
        let mut bar_store = args.components.store_mut::<BarComponentOrdered>();
        
        let iter = foo_store.iter_mut();
    
        for (entity, foo) in iter
        {
            if let Some(bar) = bar_store.get_entity_mut(&entity)
            {
                foo.runs += 10;
                bar.runs += 10;
            }
        }
    }
}

#[derive(Debug)]
struct BarAdderSystem {}

impl BarAdderSystem
{
    fn new() -> BarAdderSystem
    {
        BarAdderSystem {}
    }
}

impl ThinkerSystem<NullSceneType> for BarAdderSystem
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        let bar_store = args.components.store_mut::<BarComponent>();

        print!("{}", bar_store.count());

        let entity = args.scene.add_entity_later();
        args.scene.add_component_later::<BarComponent>(&entity, BarComponent::new());
    }
}

#[derive(Debug)]
struct DrawSystemGood {}

impl DrawSystemGood
{
    fn new() -> DrawSystemGood
    {
        DrawSystemGood {}
    }
}

impl DrawerSystem for DrawSystemGood
{
    fn draw(&self, args : DrawerArgs)
    {
        let foo_store = args.components.store::<FooComponent>();
        
        for (_, foo) in foo_store.iter()
        {
            args.drawing.draw_background_color(&DrawBackgroundColorInfo
            {
                r : foo.runs as f32,
                g : 1.0,
                b : 1.0,
                alpha : 1.0,
                depth : 0.0,
                shader_id : DataId::new(0)
            }, &DrawOptions
            {
                ordering : DrawOrdering::Painter
            });
        }
    }
}

#[derive(Debug)]
struct DrawSystemBad {}

impl DrawSystemBad
{
    fn new() -> DrawSystemBad
    {
        DrawSystemBad {}
    }
}

impl DrawerSystem for DrawSystemBad
{
    fn draw(&self, args : DrawerArgs)
    {
        let mut foo_store = args.components.store_mut::<FooComponent>();
        
        for (_, foo) in foo_store.iter_mut()
        {
            foo.runs += 1;

            args.drawing.draw_background_color(&DrawBackgroundColorInfo
            {
                r : foo.runs as f32,
                g : 1.0,
                b : 1.0,
                alpha : 1.0,
                depth : 0.0,
                shader_id : DataId::new(0)
            }, &DrawOptions
            {
                ordering : DrawOrdering::Painter
            });
        }
    }
}

#[test]
fn scene_empty()
{
    let mut scene = SimpleScene::new();
    
    // Check basic state
    assert_eq!(scene.entity_count(), 0);
    
    // Check empty state doesn't break when used for weird stuff
    scene.think(&mut GameControl::new(&GameInfo::default(), GameControlOptions::headless()).unwrap());
    
    let bad_handle = Entity::new(0, 0);
    
    assert_eq!(scene.remove_entity(&bad_handle), false);
    assert_eq!(scene.remove_component::<FooComponent>(&bad_handle), false);
    assert_eq!(scene.add_component(&bad_handle, FooComponent::new()), false);
    assert_eq!(scene.remove_component::<FooComponent>(&bad_handle), false);
}

#[test]
fn scene_add_entity()
{
    let mut scene = SimpleScene::new();
    
    assert_eq!(scene.entity_count(), 0);
    
    let entity = scene.add_entity();
    
    assert_eq!(scene.entity_count(), 1);
    assert_eq!(scene.has_entity(&entity), true);
    
    assert_eq!(scene.remove_entity(&entity), true);
    
    assert_eq!(scene.entity_count(), 0);
    assert_eq!(scene.has_entity(&entity), false);
    
    let entity1 = scene.add_entity();
    assert_eq!(scene.entity_count(), 1);
    assert_eq!(scene.has_entity(&entity1), true);
    
    let entity2 = scene.add_entity();
    assert_eq!(scene.entity_count(), 2);
    assert_eq!(scene.has_entity(&entity2), true);
    
    let entity3 = scene.add_entity();
    assert_eq!(scene.entity_count(), 3);
    assert_eq!(scene.has_entity(&entity3), true);
}

#[test]
fn scene_null_entity()
{
    let mut scene = SimpleScene::new();

    let entity1 = Entity::null();
    assert!(entity1.is_null());

    let entity2 = Entity::default();
    assert!(entity2.is_null());

    let entity3 = scene.add_entity();
    assert!(!entity3.is_null());

    assert_eq!(scene.add_component(&entity2, FooComponent::new()), false);
    assert_eq!(scene.add_component(&entity3, FooComponent::new()), true);

    assert_eq!(scene.has_entity(&entity2), false);
    assert_eq!(scene.has_entity(&entity3), true);
}

#[test]
fn scene_basic_system()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();
    
    let entity = scene.add_entity();
    assert_eq!(scene.add_component(&entity, FooComponent::new()), true);
    
    let store = scene.component_control().store::<FooComponent>();
    let component = store.get_entity(&entity).unwrap();
    assert_eq!(component.runs, 0);
    
    std::mem::drop(store);
    
    scene.think(&mut gameloop);
    
    // System not added yet, should remain 0
    
    let store = scene.component_control().store::<FooComponent>();
    let component = store.get_entity(&entity).unwrap();
    assert_eq!(component.runs, 0);
    
    std::mem::drop(store);
    
    scene.add_thinker_system(Box::new(FooSystem::new()));
    
    scene.think(&mut gameloop);
    
    // System was added, should have processed the component
    
    let store = scene.component_control().store::<FooComponent>();
    let component = store.get_entity(&entity).unwrap();
    assert_eq!(component.runs, 1);
}

#[test]
fn scene_multi_component_system()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();
    
    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();
    let entity3 = scene.add_entity();
    let entity4 = scene.add_entity();
    assert_eq!(scene.add_component(&entity2, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity3, BarComponent::new()), true);
    assert_eq!(scene.add_component(&entity4, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity4, BarComponent::new()), true);
    
    scene.add_thinker_system(Box::new(FooSystem::new()));
    
    scene.think(&mut gameloop);
    
    // Only the Foo system was added
    
    {
        let store = scene.component_control().store::<FooComponent>();
        assert!(store.get_entity(&entity1).is_none());
        assert_eq!(store.get_entity(&entity2).unwrap().runs, 1);
        assert!(store.get_entity(&entity3).is_none());
        assert_eq!(store.get_entity(&entity4).unwrap().runs, 1);
        let store = scene.component_control().store::<BarComponent>();
        assert!(store.get_entity(&entity1).is_none());
        assert!(store.get_entity(&entity2).is_none());
        assert_eq!(store.get_entity(&entity3).unwrap().runs, 0);
        assert_eq!(store.get_entity(&entity4).unwrap().runs, 0);
    }
    
    scene.add_thinker_system(Box::new(FooBarSystem::new()));
    
    scene.think(&mut gameloop);
    
    // Both the Foo and Foobar systems were added. Both should have run.
    
    let store = scene.component_control().store::<FooComponent>();
    assert!(store.get_entity(&entity1).is_none());
    assert_eq!(store.get_entity(&entity2).unwrap().runs, 2);
    assert!(store.get_entity(&entity3).is_none());
    assert_eq!(store.get_entity(&entity4).unwrap().runs, 12);
    let store = scene.component_control().store::<BarComponent>();
    assert!(store.get_entity(&entity1).is_none());
    assert!(store.get_entity(&entity2).is_none());
    assert_eq!(store.get_entity(&entity3).unwrap().runs, 0);
    assert_eq!(store.get_entity(&entity4).unwrap().runs, 10);
}

#[test]
fn scene_remove_entity_component()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();
    
    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();
    
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity2, FooComponent::new()), true);
    
    scene.add_thinker_system(Box::new(FooSystem::new()));
    
    scene.think(&mut gameloop);
    
    let store = scene.component_control().store::<FooComponent>();
    assert_eq!(store.get_entity(&entity1).unwrap().runs, 1);
    assert_eq!(store.get_entity(&entity2).unwrap().runs, 1);
    
    std::mem::drop(store);
    
    // Prevent adding component where one already exists
    
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), false);
    assert_eq!(scene.remove_component::<FooComponent>(&entity1), true);
    
    // Prevent duplicate removal
    
    assert_eq!(scene.remove_component::<FooComponent>(&entity1), false);
    
    let store = scene.component_control().store::<FooComponent>();
    assert!(store.get_entity(&entity1).is_none());
    
    std::mem::drop(store);
    
    scene.think(&mut gameloop);
    
    let store = scene.component_control().store::<FooComponent>();
    assert_eq!(store.get_entity(&entity2).unwrap().runs, 2);
    
    std::mem::drop(store);
    
    assert_eq!(scene.remove_entity(&entity2), true);
    assert_eq!(scene.remove_entity(&entity2), false);
    
    scene.think(&mut gameloop);
    
    let store = scene.component_control().store::<FooComponent>();
    assert!(store.get_entity(&entity2).is_none());
}

#[test]
fn scene_remove_entity_component_ordered()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();

    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();

    assert_eq!(scene.add_component(&entity1, FooComponentOrdered::new()), true);
    assert_eq!(scene.add_component(&entity2, FooComponentOrdered::new()), true);

    scene.add_thinker_system(Box::new(FooSystemOrdered::new()));

    scene.think(&mut gameloop);

    let store = scene.component_control().store::<FooComponentOrdered>();
    assert_eq!(store.get_entity(&entity1).unwrap().runs, 1);
    assert_eq!(store.get_entity(&entity2).unwrap().runs, 1);

    std::mem::drop(store);

    // Prevent adding component where one already exists

    assert_eq!(scene.add_component(&entity1, FooComponentOrdered::new()), false);
    assert_eq!(scene.remove_component::<FooComponentOrdered>(&entity1), true);

    // Prevent duplicate removal

    assert_eq!(scene.remove_component::<FooComponentOrdered>(&entity1), false);

    let store = scene.component_control().store::<FooComponentOrdered>();
    assert!(store.get_entity(&entity1).is_none());

    std::mem::drop(store);

    scene.think(&mut gameloop);

    let store = scene.component_control().store::<FooComponentOrdered>();
    assert_eq!(store.get_entity(&entity2).unwrap().runs, 2);

    std::mem::drop(store);

    assert_eq!(scene.remove_entity(&entity2), true);
    assert_eq!(scene.remove_entity(&entity2), false);

    scene.think(&mut gameloop);

    let store = scene.component_control().store::<FooComponentOrdered>();
    assert!(store.get_entity(&entity2).is_none());
}

#[test]
fn scene_remove_many()
{
    let mut scene = SimpleScene::new();
    
    for gen in 1..11
    {
        for _ in 0..1000
        {
            let new_entity = scene.add_entity();
            
            assert_eq!(scene.add_component(&new_entity, FooComponent::new()), true);
            assert_eq!(scene.add_component(&new_entity, BarComponent::new()), true);
        }
        
        for i in 0..1000
        {
            assert!(scene.remove_entity(&Entity::new(i, gen)));
        }
    }
}

#[test]
fn scene_remove_random()
{
    for _ in 0..100
    {
        let mut scene = SimpleScene::new();
        let mut entities = Vec::new();
        let rng = &mut rand::thread_rng();
        
        for _ in 0..10
        {
            for _ in 0..100
            {
                let new_entity = scene.add_entity();
                
                assert_eq!(scene.add_component(&new_entity, FooComponent::new()), true);
                
                entities.push(new_entity);
            }
            
            for _ in 0..100
            {
                let new_entity = scene.add_entity();
                
                assert_eq!(scene.add_component(&new_entity, FooComponent::new()), true);
                assert_eq!(scene.add_component(&new_entity, BarComponent::new()), true);
                
                entities.push(new_entity);
            }
            
            entities.shuffle(rng);
            
            for _ in 0..100
            {
                let entity = entities.pop().unwrap();
                assert!(scene.remove_entity(&entity));
            }
            
            let store = scene.component_control().store::<FooComponent>();
            
            for entity in &entities
            {
                assert!(scene.has_entity(entity));
                assert!(store.contains(entity));
            }
        }
        
        while !entities.is_empty()
        {
            let entity = entities.pop().unwrap();
            assert!(scene.remove_entity(&entity));
        }
    }
}

#[test]
fn scene_remove_random_ordered()
{
    for _ in 0..100
    {
        let mut scene = SimpleScene::new();
        let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();
        let mut entities = Vec::new();
        let rng = &mut rand::thread_rng();
        
        scene.add_thinker_system(Box::new(FooBarSystemOrdered::new()));
        
        for _ in 0..10
        {
            for _ in 0..100
            {
                let new_entity = scene.add_entity();
                
                assert_eq!(scene.add_component(&new_entity, FooComponentOrdered::new()), true);
                
                entities.push(new_entity);
            }
            
            for _ in 0..100
            {
                let new_entity = scene.add_entity();
                
                assert_eq!(scene.add_component(&new_entity, FooComponentOrdered::new()), true);
                assert_eq!(scene.add_component(&new_entity, BarComponentOrdered::new()), true);
                
                entities.push(new_entity);
            }
            
            entities.shuffle(rng);
            
            for _ in 0..100
            {
                let entity = entities.pop().unwrap();
                assert!(scene.remove_entity(&entity));
            }
            
            let store = scene.component_control().store::<FooComponentOrdered>();
            
            for entity in &entities
            {
                assert!(scene.has_entity(entity));
                assert!(store.contains(entity));
            }
            
            std::mem::drop(store);
            
            // Check consistency after purging entities
            scene.think(&mut gameloop);
            
            let store = scene.component_control().store::<FooComponentOrdered>();
            
            for entity in &entities
            {
                assert!(scene.has_entity(entity));
                assert!(store.contains(entity));
            }
        }
        
        while !entities.is_empty()
        {
            let entity = entities.pop().unwrap();
            assert!(scene.remove_entity(&entity));
        }
    }
}

#[test]
fn scene_freeze_component()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();

    scene.add_thinker_system(Box::new(FooBarFreezeSystem::new()));

    let mut entities = Vec::new();

    for _ in 0..1000
    {
        let entity = scene.add_entity();
        scene.add_component(&entity, FooComponent::new());
        scene.add_component(&entity, BarComponent::new());
        scene.add_component(&entity, BazComponent::new());

        entities.push(entity);
    }

    scene.think(&mut gameloop);

    {
        let mut foo_store = scene.component_control().store_mut::<FooComponent>();
        let bar_store = scene.component_control().store::<BarComponent>();
        let baz_store = scene.component_control().store::<BazComponent>();

        for entity in &entities
        {
            assert_eq!(foo_store.get_entity(entity).unwrap().runs, 1);
            assert_eq!(bar_store.get_entity(entity).unwrap().runs, 1);
            assert_eq!(baz_store.get_entity(entity).unwrap().runs, 1);

            if entity.id() % 4 > 0
            {
                foo_store.set_frozen(entity, FreezeFlags::REASON_HIDDEN, true);
            }
        }
    }

    scene.think(&mut gameloop);

    {
        let mut foo_store = scene.component_control().store_mut::<FooComponent>();
        let mut bar_store = scene.component_control().store_mut::<BarComponent>();
        let mut baz_store = scene.component_control().store_mut::<BazComponent>();

        for entity in &entities
        {
            if entity.id() % 4 > 0
            {
                assert_eq!(foo_store.get_entity(entity).unwrap().runs, 1);
            }
            else
            {
                assert_eq!(foo_store.get_entity(entity).unwrap().runs, 2);
            }
            assert_eq!(bar_store.get_entity(entity).unwrap().runs, 2);
            assert_eq!(baz_store.get_entity(entity).unwrap().runs, 2);

            if entity.id() % 4 > 0
            {
                foo_store.set_frozen(entity, FreezeFlags::REASON_HIDDEN, false);
                bar_store.set_frozen(entity, FreezeFlags::REASON_HIDDEN, true);
            }
            if entity.id() % 2 == 1
            {
                baz_store.set_frozen(entity, FreezeFlags::REASON_HIDDEN, true);
            }
            if entity.id() % 4 > 1
            {
                baz_store.set_frozen(entity, FreezeFlags::REASON_EFFECT, true);
            }
        }
    }

    scene.think(&mut gameloop);

    {
        let foo_store = scene.component_control().store::<FooComponent>();
        let bar_store = scene.component_control().store::<BarComponent>();
        let baz_store = scene.component_control().store::<BazComponent>();

        for entity in &entities
        {
            let expected_baz = match entity.id() % 4
            {
                0 => {3} // none
                1 => {2} // REASON_HIDDEN
                2 => {3} // REASON_EFFECT
                3 => {2} // REASON_EFFECT and REASON_HIDDEN
                _ => unreachable!()
            };

            if entity.id() % 4 > 0
            {
                assert_eq!(foo_store.get_entity(entity).unwrap().runs, 2);
            }
            else
            {
                assert_eq!(foo_store.get_entity(entity).unwrap().runs, 3);
                assert_eq!(baz_store.get_entity(entity).unwrap().runs, 3);
            }
            assert_eq!(bar_store.get_entity(entity).unwrap().runs, 3);
            assert_eq!(baz_store.get_entity(entity).unwrap().runs, expected_baz);
        }
    }
}

#[test]
fn scene_component_registration()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();

    scene.add_thinker_system(Box::new(FooSystem::new()));
    scene.add_thinker_system(Box::new(BarAdderSystem::new()));
    scene.add_thinker_system(Box::new(FooBarSystem::new()));
    scene.add_drawer_system(Box::new(DrawSystemGood::new()));

    scene.think(&mut gameloop);

    assert_eq!(scene.component_control().store::<BarComponent>().count(), 1);

    scene.think(&mut gameloop);

    let entity1 = scene.add_entity();
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), true);

    scene.think(&mut gameloop);

    let entity2 = scene.add_entity();
    assert_eq!(scene.add_component(&entity2, BarComponent::new()), true);

    scene.think(&mut gameloop);
}

#[test]
fn scene_immutable_draw()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();
    
    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();
    
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity2, FooComponent::new()), true);
    
    scene.add_drawer_system(Box::new(DrawSystemGood::new()));

    // Rust borrow checker once again being unreasonable
    let mut res = DataMultistore::new(gameloop.source_manager().clone());
    std::mem::swap(&mut res, gameloop.res_mut());

    scene.draw(&res, gameloop.renderer_mut().control(), &DrawTransform::identity(), 0.0);

    std::mem::swap(&mut res, gameloop.res_mut());
}

#[test]
#[should_panic(expected = "ComponentStore for keeshond::tests::scene::FooComponent unavailable: Mutable access is not allowed while drawing")]
fn scene_mutable_draw_should_panic()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();
    
    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();
    
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity2, FooComponent::new()), true);
    
    scene.add_drawer_system(Box::new(DrawSystemBad::new()));
    
    scene.think(&mut gameloop);

    // Rust borrow checker once again being unreasonable
    let mut res = DataMultistore::new(gameloop.source_manager().clone());
    std::mem::swap(&mut res, gameloop.res_mut());

    scene.draw(&res, gameloop.renderer_mut().control(), &DrawTransform::identity(), 0.0);

    std::mem::swap(&mut res, gameloop.res_mut());
}

#[derive(Debug)]
struct DeferredSystem
{
    runs : u32,
    foo_entity : Entity,
    blank_entity : Entity,
    foobar_entity : Entity,
    foobar2_entity : Entity,
    ghost_entity : Entity,
    spaced_entity : Entity,
    frozen_entity : Entity
}

impl DeferredSystem
{
    fn new() -> DeferredSystem
    {
        DeferredSystem
        {
            runs : 0,
            foo_entity : Entity::null(),
            blank_entity : Entity::null(),
            foobar_entity : Entity::null(),
            foobar2_entity : Entity::null(),
            ghost_entity : Entity::null(),
            spaced_entity : Entity::null(),
            frozen_entity : Entity::null()
        }
    }
}

impl ThinkerSystem<NullSceneType> for DeferredSystem
{
    fn think(&mut self, args : ThinkerArgs<NullSceneType>)
    {
        match self.runs
        {
            0 =>
            {
                let foo_store = args.components.store_mut::<FooComponent>();
                let bar_store = args.components.store_mut::<BarComponent>();

                assert_eq!(foo_store.count(), 0);
                assert_eq!(bar_store.count(), 0);

                self.foo_entity = args.scene.add_entity_later();
                args.scene.add_component_later(&self.foo_entity, FooComponent::new());
                self.blank_entity = args.scene.add_entity_later(); // Don't do this in real programs :)
                self.foobar_entity = args.scene.add_entity_later();
                args.scene.add_component_later(&self.foobar_entity, FooComponent::new());
                args.scene.add_component_later(&self.foobar_entity, BarComponent { runs : 1 });
                // Should fail later
                args.scene.add_component_later(&self.foobar_entity, BarComponent { runs : 2 });
                self.foobar2_entity = args.scene.add_entity_later();
                args.scene.add_component_later(&self.foobar2_entity, FooComponent::new());
                args.scene.add_component_later(&self.foobar2_entity, BarComponent { runs : 3 });
                self.frozen_entity = args.scene.add_entity_later();
                args.scene.add_component_later(&self.frozen_entity, FooComponent::new());
                args.scene.add_component_later(&self.frozen_entity, BarComponent::new());
                args.scene.set_entity_frozen_later(&self.frozen_entity, FreezeFlags::REASON_HIDDEN, true);

                assert_eq!(foo_store.count(), 0);
                assert_eq!(bar_store.count(), 0);
                assert!(foo_store.get_entity(&self.foo_entity).is_none());
                assert!(foo_store.get_entity(&self.foobar_entity).is_none());
                assert!(bar_store.get_entity(&self.foobar_entity).is_none());
                assert!(foo_store.get_entity(&self.foobar2_entity).is_none());
                assert!(bar_store.get_entity(&self.foobar2_entity).is_none());
                assert!(foo_store.freeze_flags(&self.frozen_entity).is_none());
                assert!(bar_store.freeze_flags(&self.frozen_entity).is_none());
            }
            1 =>
            {
                let foo_store = args.components.store_mut::<FooComponent>();
                let bar_store = args.components.store_mut::<BarComponent>();

                assert_eq!(foo_store.count(), 4);
                assert_eq!(bar_store.count(), 3);
                assert!(foo_store.get_entity(&self.foo_entity).is_some());
                assert!(foo_store.get_entity(&self.foobar_entity).is_some());
                assert!(foo_store.get_entity(&self.foobar2_entity).is_some());
                assert!(bar_store.get_entity(&self.foo_entity).is_none());
                assert!(bar_store.get_entity(&self.foobar_entity).is_some());
                assert!(bar_store.get_entity(&self.foobar2_entity).is_some());
                assert!(foo_store.freeze_flags(&self.frozen_entity).is_some());
                assert!(bar_store.freeze_flags(&self.frozen_entity).is_some());
                assert_eq!(foo_store.freeze_flags(&self.frozen_entity).unwrap(), FreezeFlags::REASON_HIDDEN);
                assert_eq!(bar_store.freeze_flags(&self.frozen_entity).unwrap(), FreezeFlags::REASON_HIDDEN);

                let bar = bar_store.get_entity(&self.foobar_entity).unwrap();

                // Check that the first one, not the second one, was added
                assert_eq!(bar.runs, 1);

                args.scene.remove_component_later::<FooComponent>(&self.foobar_entity);
                args.scene.remove_entity_later(&self.foobar2_entity);
                args.scene.set_entity_frozen_later(&self.frozen_entity, FreezeFlags::REASON_HIDDEN, false);
                args.scene.set_entity_frozen_later(&self.frozen_entity, FreezeFlags::REASON_EFFECT, true);

                assert_eq!(foo_store.count(), 4);
                assert_eq!(bar_store.count(), 3);
                assert!(foo_store.get_entity(&self.foo_entity).is_some());
                assert!(foo_store.get_entity(&self.foobar_entity).is_some());
                assert!(foo_store.get_entity(&self.foobar2_entity).is_some());
                assert!(bar_store.get_entity(&self.foo_entity).is_none());
                assert!(bar_store.get_entity(&self.foobar_entity).is_some());
                assert!(bar_store.get_entity(&self.foobar2_entity).is_some());
                assert_eq!(foo_store.freeze_flags(&self.frozen_entity).unwrap(), FreezeFlags::REASON_HIDDEN);
                assert_eq!(bar_store.freeze_flags(&self.frozen_entity).unwrap(), FreezeFlags::REASON_HIDDEN);
            }
            2 =>
            {
                let foo_store = args.components.store_mut::<FooComponent>();
                let bar_store = args.components.store_mut::<BarComponent>();

                assert_eq!(foo_store.count(), 2);
                assert_eq!(bar_store.count(), 2);
                assert!(foo_store.get_entity(&self.foo_entity).is_some());
                assert!(foo_store.get_entity(&self.foobar_entity).is_none());
                assert!(foo_store.get_entity(&self.foobar2_entity).is_none());
                assert!(bar_store.get_entity(&self.foo_entity).is_none());
                assert!(bar_store.get_entity(&self.foobar_entity).is_some());
                assert!(bar_store.get_entity(&self.foobar2_entity).is_none());
                assert_eq!(foo_store.freeze_flags(&self.frozen_entity).unwrap(), FreezeFlags::REASON_EFFECT);
                assert_eq!(bar_store.freeze_flags(&self.frozen_entity).unwrap(), FreezeFlags::REASON_EFFECT);

                args.scene.add_component_later(&self.foobar_entity, FooComponent { runs : 4 });
                self.foobar2_entity = args.scene.add_entity_later();
                args.scene.add_component_later(&self.foobar2_entity, FooComponent::new());
                args.scene.add_component_later(&self.foobar2_entity, BarComponent { runs : 5 });
            }
            3 =>
            {
                let foo_store = args.components.store_mut::<FooComponent>();
                let bar_store = args.components.store_mut::<BarComponent>();

                assert_eq!(foo_store.count(), 4);
                assert_eq!(bar_store.count(), 3);
                assert!(foo_store.get_entity(&self.foo_entity).is_some());
                assert!(foo_store.get_entity(&self.foobar_entity).is_some());
                assert!(foo_store.get_entity(&self.foobar2_entity).is_some());
                assert!(bar_store.get_entity(&self.foo_entity).is_none());
                assert!(bar_store.get_entity(&self.foobar_entity).is_some());
                assert!(bar_store.get_entity(&self.foobar2_entity).is_some());

                let foo = foo_store.get_entity(&self.foobar_entity).unwrap();

                assert_eq!(foo.runs, 4);

                // Now we try some weird stuff and make sure it still has consistent results

                self.ghost_entity = args.scene.add_entity_later();
                args.scene.add_component_later(&self.ghost_entity, FooComponent::new());
                args.scene.remove_entity_later(&self.ghost_entity);
                args.scene.add_component_later(&self.ghost_entity, BarComponent::new());
            }
            4 =>
            {
                let foo_store = args.components.store_mut::<FooComponent>();
                let bar_store = args.components.store_mut::<BarComponent>();

                assert_eq!(foo_store.count(), 4);
                assert_eq!(bar_store.count(), 3);

                assert!(foo_store.get_entity(&self.ghost_entity).is_none());
                assert!(bar_store.get_entity(&self.ghost_entity).is_none());

                args.scene.remove_entity_later(&self.ghost_entity);
                self.ghost_entity = args.scene.add_entity_later();

                for i in 0..100
                {
                    args.scene.remove_entity_later(&self.spaced_entity);
                    self.spaced_entity = args.scene.add_entity_later();
                    args.scene.add_component_later(&self.spaced_entity, BarComponent { runs : i });
                }
            }
            5 =>
            {
                let foo_store = args.components.store_mut::<FooComponent>();
                let bar_store = args.components.store_mut::<BarComponent>();

                assert_eq!(foo_store.count(), 4);
                assert_eq!(bar_store.count(), 4);
                assert!(bar_store.get_entity(&self.spaced_entity).is_some());

                let bar = bar_store.get_entity(&self.spaced_entity).unwrap();

                assert_eq!(bar.runs, 99);
            }
            _ => {}
        }

        self.runs += 1;
    }
}

#[test]
fn scene_deferred_add_remove_freeze()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new_headless(&GameInfo::default()).unwrap();

    scene.add_thinker_system(Box::new(DeferredSystem::new()));

    assert_eq!(scene.entity_count(), 0);
    scene.think(&mut gameloop);
    assert_eq!(scene.entity_count(), 5);
    scene.think(&mut gameloop);
    assert_eq!(scene.entity_count(), 4);
    scene.think(&mut gameloop);
    assert_eq!(scene.entity_count(), 5);
    scene.think(&mut gameloop);
    assert_eq!(scene.entity_count(), 5);
    scene.think(&mut gameloop);
    assert_eq!(scene.entity_count(), 7);
    scene.think(&mut gameloop);
}

#version 140

uniform sampler2D t_texture;
in vec2 v_uv;
in vec4 v_rgba;

out vec4 TargetScreen;

void main()
{
    vec4 tex_color = texture(t_texture, v_uv);
    vec4 color = vec4(1.0 - tex_color.r, 1.0 - tex_color.g, 1.0 - tex_color.b, tex_color.a);

    color = color * v_rgba;

    if (color.a <= 0.0)
    {
        discard;
    }

    TargetScreen = color;
}


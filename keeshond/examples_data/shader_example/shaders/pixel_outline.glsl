#version 140

uniform sampler2D t_texture;
uniform vec2 u_tex_size;
in vec2 v_uv;
in vec4 v_tex_coords;
in vec4 v_rgba;

out vec4 TargetScreen;

vec2 slice_coords()
{
    return vec2((v_uv.x - v_tex_coords[0]) / v_tex_coords[2], (v_uv.y - v_tex_coords[1]) / v_tex_coords[3]);
}

vec2 pixel_slice_coords()
{
    return slice_coords() * u_tex_size * v_tex_coords.zw;
}

vec2 nudge_uv_by_pixel(vec2 xy)
{
    return vec2(clamp(v_uv.xy + xy / u_tex_size.xy, v_tex_coords.xy, v_tex_coords.xy + v_tex_coords.zw));
}

void main()
{
    vec4 color = texture(t_texture, v_uv);
    float outline = texture(t_texture, nudge_uv_by_pixel(vec2(1.0, 0.0))).a;
    outline += texture(t_texture, nudge_uv_by_pixel(vec2(-1.0, 0.0))).a;
    outline += texture(t_texture, nudge_uv_by_pixel(vec2(0.0, 1.0))).a;
    outline += texture(t_texture, nudge_uv_by_pixel(vec2(0.0, -1.0))).a;
    outline = max(min(outline, 1.0) - color.a, 0.0);

    color = vec4(mix(color.rgb, v_rgba.rgb, outline), color.a + outline);

    if (color.a <= 0.0)
    {
        discard;
    }

    TargetScreen = color;
}

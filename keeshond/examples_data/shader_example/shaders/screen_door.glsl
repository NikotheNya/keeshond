#version 140

uniform sampler2D t_texture;
uniform vec2 u_tex_size;
in vec2 v_uv;
in vec4 v_tex_coords;
in vec4 v_rgba;

out vec4 TargetScreen;

vec2 slice_coords()
{
    return vec2((v_uv.x - v_tex_coords[0]) / v_tex_coords[2], (v_uv.y - v_tex_coords[1]) / v_tex_coords[3]);
}

vec2 pixel_slice_coords()
{
    return slice_coords() * u_tex_size * v_tex_coords.zw;
}

void main()
{
    vec4 color = texture(t_texture, v_uv);
    vec2 pixel_xy = pixel_slice_coords();

    color.a = color.a * ((int(pixel_xy.x) + int(pixel_xy.y)) % 2);

    color = color * v_rgba;

    if (color.a <= 0.0)
    {
        discard;
    }

    TargetScreen = color;
}

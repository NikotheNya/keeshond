# 🐶 KEESHOND Game Engine 🐶

## About

Keeshond is a 2D game engine with a focus on simplicity, flexibility, and performance.

Also check out [Keeshond Treats](https://crates.io/crates/keeshond_treats) for extra goodies that will help you make your games faster!

Keeshond currently targets Windows, Linux, and the web via Emscripten. It should also work on macOS, but this is largely untested.


## Getting started

Check out The Keeshond Book:
https://cosmicchipsocket.com/keeshond_book


## Project goals

- Runs reasonably well on hardware from 2010
- Deterministic game logic across systems, important for speedrunners
- Implements features games need, without adding unneeded complexity
- First-class Linux support via SDL2


## Current features

- Gameloop with fixed timestep synchronization
- Custom-built ECS system designed for decoupled game/drawing logic
- Mouse, keyboard, and gamepad input system
- Up to 400k sprites at 60 FPS (dependent on hardware and other factors)
- Asset loading from the filesystem and from zip/pk3 files
- Audio system via OpenAL
- Resolution-independent viewport scaling with maintained aspect ratio
- Optional pixel upscaling with non-integral softening for neo-retro games
- Optional imgui integration


## Supplemental features with keeshond_treats

- Sprite animation system
- Sprite-based text
- Tilemaps
- Object-to-object collision with fast broadphase
- Level format and level editor


## Features to come

- Multiplayer gamepad input
- Audio streaming
- Tilemap collision


## Performance notes

During development, you should build with an opt-level of at least 1 or else the compiler might not generate code that
is fast enough to make your game playable:

```
[profile.dev]
opt-level = 1

[profile.release]
opt-level = 3
lto = true
```


## Running examples

In order to get the examples to load their resources, run them from within the `keeshond` directory.

```
cd keeshond
cargo run --example doggymark
```


## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.


## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
